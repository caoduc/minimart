<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'trangchu/index';
$route['trang-chu'] = 'trangchu/index';
$route['tin-tuc'] = 'tintuc';
$route['tin-tuc/(:num)'] = 'tintuc/index/$1';
$route['tin-tuc/(:any)'] = 'tintuc/detail';
$route['gioi-thieu'] = 'gioithieu';
$route['dich-vu'] = 'dichvu';
$route['lien-he'] = 'lienhe';
$route['khuyen-mai'] = 'khuyenmai';
$route['khuyen-mai/(:num)'] = 'khuyenmai/index/$1';
$route['khuyen-mai/(:any)'] = 'khuyenmai/detail';
$route['khach-hang-chia-se'] = 'khachhangchiase';
$route['tuyen-dung'] = 'tuyendung/index';
$route['tuyen-dung/(:num)'] = 'tuyendung/index/$1';
$route['tai-khoan'] = 'taikhoan';
$route['tim-kiem'] = 'timkiem';
$route['qua-tang'] = 'quatang';
$route['chinh-sach-van-chuyen'] = 'csvanchuyen';
$route['chinh-sach-bao-mat'] = 'csbaomat';
$route['doi-tra-hang'] = 'doitrahang';
$route['van-chuyen'] = 'vanchuyen';
$route['phi-van-chuyen'] = 'phivanchuyen'; 
$route['phuong-thuc-thanh-toan'] = 'thanhtoan';
$route['san-pham'] = 'sanpham/index';
$route['san-pham/(:num)'] = 'sanpham/index/$1';
$route['san-pham/(:any)'] = 'sanpham/category';
$route['san-pham/(:any)/(:num)'] = 'sanpham/category/$1';
$route['dang-nhap'] = 'dangnhap/dangnhap';
$route['dang-xuat'] = 'dangnhap/dangxuat';
$route['dang-ky'] = 'dangky/index';
$route['gio-hang'] = 'giohang/index';
$route['checkout'] = 'giohang/checkout';
$route['gio-hang/remove/(:num)'] = 'giohang/remove/(:num)';
$route['gio-hang/thankyou'] = 'giohang/thankyou';
$route['tai-khoan/don-hang/(:num)'] = 'taikhoan/order/(:num)';
$route['tai-khoan/cap-nhat'] = 'taikhoan/update';
$route['search?(:any)'] = 'search/index/$1';
$route['search?(:any)/(:num)'] = 'search/index/$1';

$route['admin'] = 'admin/dashboard';

$route['admin/product'] ='admin/product';
$route['admin/product/(:num)'] ='admin/product/index/$1';
$route['product/insert'] ='product/insert';
$route['product/status/(:num)'] ='product/status/$1';
$route['product/update/(:num)'] ='product/update/$1';
$route['product/recyclebin'] ='product/recyclebin';
$route['product/trash/(:num)'] ='product/trash/$1';
$route['product/recyclebin/(:num)'] ='product/recyclebin/$1';

$route['admin/category'] ='admin/category';
$route['admin/category/(:num)'] ='admin/category/index/$1';
$route['category/insert'] ='category/insert';
$route['category/update/(:num)'] ='category/update/$1';
$route['category/status/(:num)'] ='category/status/$1';
$route['category/recyclebin'] ='category/recyclebin';
$route['category/recyclebin/(:num)'] ='category/recyclebin/$1';
$route['category/trash/(:num)'] ='category/trash/$1';
$route['category/delete/(:num)'] ='category/delete/$1';
$route['category/restore/(:num)'] ='category/restore/$1';

$route['admin/topic'] ='admin/topic';
$route['admin/topic/(:num)'] ='admin/topic/index/$1';
$route['topic/update/(:num)'] ='topic/update/$1';
$route['topic/status/(:num)'] ='topic/status/$1';
$route['topic/insert'] ='topic/insert';
$route['topic/recyclebin'] ='topic/recyclebin';
$route['topic/recyclebin/(:num)'] ='topic/recyclebin/$1';
$route['topic/trash/(:num)'] ='topic/trash/$1';
$route['topic/delete/(:num)'] ='topic/delete/$1';
$route['topic/restore/(:num)'] ='topic/restore/$1';

$route['admin/content'] ='admin/content';
$route['admin/content/(:num)'] ='admin/content/index/$1';
$route['content/update/(:num)'] ='content/update/$1';
$route['content/status/(:num)'] ='content/status/$1';
$route['content/insert'] ='content/insert';
$route['content/recyclebin'] ='content/recyclebin';
$route['content/recyclebin/(:num)'] ='content/recyclebin/$1';
$route['content/trash/(:num)'] ='content/trash/$1';
$route['content/delete/(:num)'] ='content/delete/$1';
$route['content/restore/(:num)'] ='content/restore/$1';

$route['admin/sliders'] ='admin/sliders';
$route['admin/sliders/(:num)'] ='admin/sliders/index/$1';
$route['admin/sliders/insert'] ='admin/sliders/insert';
$route['sliders/update/(:num)'] ='sliders/update/$1';
$route['sliders/status/(:num)'] ='sliders/status/$1';
$route['sliders/recyclebin'] ='sliders/recyclebin';
$route['sliders/recyclebin/(:num)'] ='sliders/recyclebin/$1';
$route['sliders/trash/(:num)'] ='sliders/trash/$1';
$route['sliders/delete/(:num)'] ='sliders/delete/$1';
$route['sliders/restore/(:num)'] ='sliders/restore/$1';

$route['admin/customer'] ='admin/customer';
$route['admin/customer/(:num)'] ='admin/customer/index/$1';
$route['customer/update/(:num)'] ='customer/update/$1';
$route['customer/status/(:num)'] ='customer/status/$1';
$route['customer/recyclebin'] ='customer/recyclebin';
$route['customer/recyclebin/(:num)'] ='customer/recyclebin/$1';
$route['customer/trash/(:num)'] ='customer/trash/$1';
$route['customer/delete/(:num)'] ='customer/delete/$1';
$route['customer/restore/(:num)'] ='customer/restore/$1';

$route['admin/orders'] ='admin/orders';
$route['admin/orders/(:num)'] ='admin/orders/index/$1';
$route['404_override'] = 'Error404';
$route['(:any)'] = 'sanpham/detail/$1';


$route['translate_uri_dashes'] = FALSE;
