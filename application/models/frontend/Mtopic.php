<?php
class Mtopic extends CI_Model {

    public function __construct()
    {
            parent::__construct();
            $this->table = $this->db->dbprefix('topic');
    }
  
    public function topic_id($link)
    {
        $this->db->where('link', $link);
        $this->db->where('status', 1);
        $this->db->where('trash', 1);
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        $row=$query->row_array();
        return $row['id'];
    }

    public function topic_position($parentid,$position )
        {
            $this->db->where('parentid', $parentid);
            $this->db->where('position', $position);
            $this->db->where('status', 1);
            $this->db->where('trash', 1);
            $this->db->order_by('orders asc, modified desc');
            //$this->db->order_by('updated_at', 'desc');
            $query = $this->db->get($this->table);
            return $query->result_array();
        }
}