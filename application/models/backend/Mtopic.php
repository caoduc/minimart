<?php
class Mtopic extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = $this->db->dbprefix('topic');
    }

    public function topic_count_parentid($parentid)
    {
        $this->db->where('parentid', $parentid);
        $this->db->where('status', 1);
        $this->db->where('trash', 1);
        $query = $this->db->get($this->table);
        return count($query->result_array());

    }

    public function topic_all($limit,$first)
    {
        $this->db->where('trash',1);
        $this->db->order_by('created', 'desc');
        $query = $this->db->get($this->table,$limit,$first);
        return $query->result_array();
    }

    public function topic_trash($limit, $first)
    {
        $this->db->where('trash',0);
        $query = $this->db->get($this->table, $limit, $first);
        return $query->result_array();
    }
  
    public function topic_trash_count()
    {
        $this->db->where('trash', 0);
        $query = $this->db->get($this->table);
        return count($query->result_array());
    }

    public function topic_count()
    {
        $this->db->where('status', 1);
        $this->db->where('trash', 1);
        $query = $this->db->get($this->table);
        return count($query->result_array());
    }

    public function topic_parentid($id)
    {
        $this->db->where('trash',1);
        $this->db->where('status',1);
        $this->db->where('id',$id);
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        $row=$query->row_array();
        return $row['parentid'];
    }

    public function topic_name($id)
    {
        $this->db->where('trash',1);
        $this->db->where('status',1);
        $this->db->where('id',$id);
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        $row=$query->row_array();
        return $row['name'];
    }

    public function topic_position_id($id)
    {
        $this->db->where('trash',1);
        $this->db->where('status',1);
        $this->db->where('id',$id);
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        $row=$query->row_array();
        return $row['position'];
    }

    public function topic_detail($id)
    {
        $this->db->where('trash',1);
        $this->db->where('id',$id);
        $this->db->order_by('created', 'desc');
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function topic_list()
    {
        $this->db->where('trash',1);
        $this->db->order_by('created', 'desc');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function topic_position()
    {
        $this->db->distinct();
        $this->db->group_by('position');
        $this->db->where('trash',1);
        $this->db->order_by('created', 'desc');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function topic_restore($id)
    {
        $data=array('trash'=>1);
        $this->db->where('id',$id);
        $this->db->update($this->table, $data);
    }

    //Thêm
    public function topic_insert($mydata)
    {
        $this->db->insert($this->table, $mydata);
    }

    //Cập nhật
    public function topic_update($mydata, $id)
    {
        $this->db->where('id',$id);
        $this->db->update($this->table, $mydata);
    }

    //Xóa
    public function topic_delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete($this->table);
    }
}