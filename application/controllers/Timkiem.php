<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timkiem extends CI_Controller {

	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mtopic');
        $this->load->model("frontend/Mproduct");
        $this->data['com']='timkiem';
    }
    
	public function index()
	{
        $this->data['title']='Tìm kiếm - Mini Mark';  
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}
}