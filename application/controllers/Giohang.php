<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Giohang extends CI_Controller {
	
	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mtopic');
        $this->load->model("frontend/Mproduct");
        $this->load->model("frontend/Mprovince");
        $this->load->model("frontend/Mdistrict");
        $this->load->model("frontend/Mcustomer");
        $this->load->model("frontend/Morder");
        $this->load->model("frontend/Morderdetail");
        $this->data['com']='giohang';
        $this->data['customer']=$this->Mcustomer->customer_detail_id($this->session->userdata('id'));
    }

	public function index()
	{
		$this->data['title']='Giỏ hàng của bạn - Mini Mark';  
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}

	public function checkout()
	{
		$this->load->library('session');
		$this->load->library('text'); //Thư viện tự tạo
		$this->load->library('email');
		$d=getdate();
		$today=$d['year']."/".$d['mon']."/".$d['mday']." ".$d['hours'].":".$d['minutes'].":".$d['seconds'];
		$this->load->library('form_validation');
		$this->form_validation->set_rules('Name', 'Họ và tên', 'required');
		$this->form_validation->set_rules('Email', 'Địa chỉ email', 'required');
		if($this->form_validation->run() == TRUE)
		{
			if($this->session->userdata('cart'))//Tính tiền
			{
				$val = $this->session->userdata('cart');
				$total = 0; $ship = 40000;
				foreach ($val as $key => $value) 
				{
					$row = $this->Mproduct->product_detail_id($key);
					if($row['price_sale'] > 0)
                    {
                        $sum = $row['price_sale'] * $value;
                    }
                    else
                    {
                        $sum = $row['price'] * $value;
                    }
                    $total += $sum; 
				}
				$s = $total + $ship; 
			}
			//Xử lý giá trị tỉnh thành
			$province = $_POST['city'];
			$province_name = $this->Mprovince->province_name($province);
			//Xử lý giá trị quận/huyện
			$district = $_POST['DistrictId'];
			$district_name = $this->Mdistrict->district_name($district);
			//Xử lý chưa có tài khoản
			if($this->session->userdata('user'))
			{
				$id = $this->Mcustomer->customer_detail_id($this->session->userdata('id'));
				$customerid = $id['id'];
			}
			else
			{
				$name = strtolower($_POST['Name']);
				//Lấy họ tên người dùng làm tên đăng nhập
				$username = str_replace(' ','', $this->text->vn_str_filter($name));
				//Kiểm tra có tồn tại hay chưa, có tồn tại + randoom số
				$detail = $this->Mcustomer->customer_detail();
				foreach ($detail as $key) 
				{
					if($username == $key['username'])
					{
						$username = $username.rand(1, 10000);
					}
				}
				$data = array(
					'fullname' => $_POST['Name'],
					'username' => $username,
					'password' => md5(123456),
					'email' => $_POST['Email'],
					'phone' => $_POST['Phone'],
					'address' => $_POST['Address'],
					'trash' => 1,
					'access' => 1,
					'status' => 1
				);
				$password = md5(123456);
				$this->Mcustomer->customer_insert($data);
				if($this->Mcustomer->customer_login($username, $password)!=FALSE)
	            {
	                $row = $this->Mcustomer->customer_login($username, $password);
	                $this->session->set_userdata('user',$row['username']);
	                $this->session->set_userdata('fullname',$row['fullname']);
	                $this->session->set_userdata('id',$row['id']);
	                $this->session->set_userdata('access',$row['access']);
	            }
				$customerid = $this->Mcustomer->customer_id($username);
			}
			$mydata=array(
				'customerid' => $customerid,
				'orderdate' => $today,
				'requireddate' => $today,
				'shipperdate' => $today,
				'total' => $s,
				'address' => $_POST['Address'],
				'province' => $province_name,
				'district' => $district_name,
				'trash' => 1,
				'access' => 1,
				'status' => 0
			);
			$this->Morder->order_insert($mydata);
			$order_detail = $this->Morder->order_detail_customerid($customerid);
			foreach ($order_detail as $row) 
			{
				$orderid = $row['id'];
			}
			if($this->session->userdata('cart'))
			{
				$val = $this->session->userdata('cart');
				foreach ($val as $key => $value) 
				{
					$row = $this->Mproduct->product_detail_id($key);
					//Lưu theo giá gốc || giá khuyến mãi
					if($row['price_sale'] > 0)
					{
						$price = $row['price_sale'];
					}
					else
					{
						$price = $row['price'];
					}
					$data = array(
						'orderid' => $orderid,
						'productid' => $key,
						'price' => $price,
						'amount' => $value,
						'discount' => $row['sale'],
						'trash' => 1,
						'access' => 1,
						'status' => 1
					);
					$this->Morderdetail->orderdetail_insert($data);
				}
			}
			
			redirect('gio-hang/thankyou','refresh');
		}
		else
		{
			$this->data['title']='Đặt hàng - Mini Mark';  
			$this->data['view']='checkout';
			$this->load->view('frontend/layout',$this->data);
		}
	}

	public function district()
	{
		$this->load->library('session');
		$id=$_POST['provinceid'];
		$list = $this->Mdistrict->district_provinceid($id);
		$html="<option value =''>--- Chọn quận huyện ---</option>";
		foreach ($list as $row) 
		{
			$html.='<option value = '.$row["districtid"].'>'.$row["type"].' '.$row["name"].'</option>';
		}
		echo json_encode($html);
	}

	public function remove($id)
	{
		$this->load->library('session');
		$url = explode('/',uri_string());
		$num=$url[2];//Lấy số id
		if($this->session->userdata('cart'))
		{
			$val = $this->session->userdata('cart');
			if(array_key_exists ("$num", $val))
			{
				unset($val["$num"]);
			}
		}
		print_r($val);
		//$this->session->set_userdata('remove_id', $id);
	}

	public function thankyou()
	{
		$this->data['view']='thankyou';
		$this->load->view('frontend/layout',$this->data);
	}

}