<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csvanchuyen extends CI_Controller {
    function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->model('frontend/Mcontent');
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mtopic');
        $this->load->model("frontend/Mproduct");
        $this->data['com']='csvanchuyen';
    }
    
    public function index()
    {
        $aurl= explode('/',uri_string());
        $catlink=$aurl[0];
        $catid = $this->Mtopic->topic_id($catlink);
        $this->data['row']=$this->Mcontent->content_gioithieu($catid);  
        $this->data['title']='Chính sách vận chuyển - Mini Mark';  
        $this->data['view']='index';
        $this->load->view('frontend/layout',$this->data);
    }
}