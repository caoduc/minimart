<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Topic extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('backend/Mtopic');
		$this->load->model('backend/Mcontent');
		$this->load->model('backend/Muser');
		if(!$this->session->userdata('admin'))
		{
			redirect('admin/user/login','refresh');
		}
		$this->data['user']=$this->Muser->user_detail($this->session->userdata('id'));
		$this->data['com']='topic';
	}

	public function index()
	{
		$this->load->library('phantrang');
		$limit=10;
		$current=$this->phantrang->PageCurrent();
		$first=$this->phantrang->PageFirst($limit, $current);
		$total=$this->Mtopic->topic_count();
		$this->data['strphantrang']=$this->phantrang->PagePer($total, $current, $limit, $url='admin/topic');
		$this->data['list']=$this->Mtopic->topic_all($limit,$first);
		$this->data['view']='index';
		$this->data['title']='Chủ đề bài viết';
		$this->load->view('backend/layout', $this->data);
	}

	public function insert()
	{
		$d=getdate();
		$today=$d['year']."/".$d['mon']."/".$d['mday']." ".$d['hours'].":".$d['minutes'].":".$d['seconds'];
		$this->load->library('form_validation');
		$this->load->library('alias');
		$this->form_validation->set_rules('name', 'Tên chủ đề', 'required|is_unique[db_topic.name]');
		$this->form_validation->set_rules('position', 'Vị trí', 'required');
		if ($this->form_validation->run() == TRUE) 
		{
			$mydata= array(
				'name' =>$_POST['name'], 
				'link' =>$string=$this->alias->str_alias($_POST['name']),
				'status' =>$_POST['status'],
				'orders' =>'',
				'created' =>$today,
				'created_by' =>$this->session->userdata('fullname'),
				'modified' =>$today,
				'modified_by' =>$this->session->userdata('fullname'),
				'trash'=>1,'access'=>$_POST['access']
			);
			$pos=$_POST['position'];
			$position = $this->Mtopic->topic_position_id($pos);
			$mydata['position']=$position;
			$id=$_POST['parentid'];
			if($id==0)
			{
				$mydata['level']=1;
				$mydata['parentid']=$id;
			}
			else
			{
				$row=$this->Mtopic->topic_detail($id);//Xem thông tin
				$mydata['level']=$row['level']+1;
				$mydata['parentid']=$id;	
			}
			$this->Mtopic->topic_insert($mydata);
			$this->session->set_flashdata('success', 'Thêm chủ đề bài viết thành công');
			redirect('admin/topic','refresh');
		} 
		else 
		{
			$this->data['view']='insert';
			$this->data['title']='Thêm chủ đề bài viết';
			$this->load->view('backend/layout', $this->data);
		}
	}

	public function update($id)
	{
		$this->data['row']=$this->Mtopic->topic_detail($id);
		$d=getdate();
		$today=$d['year']."/".$d['mon']."/".$d['mday']." ".$d['hours'].":".$d['minutes'].":".$d['seconds'];
		$this->load->library('form_validation');
		$this->load->library('alias');
		$this->form_validation->set_rules('position', 'Vị trí', 'required');
		if ($this->form_validation->run() == TRUE) 
		{
			$mydata= array(
				'name' =>$_POST['name'], 
				'link' =>$string=$this->alias->str_alias($_POST['name']),
				'status' =>$_POST['status'],
				'orders' =>'',
				'modified' =>$today,
				'modified_by' =>$this->session->userdata('fullname'),
				'trash'=>1,'access'=>$_POST['access']
			);
			$pos=$_POST['position'];
			$position = $this->Mtopic->topic_position_id($pos);
			$mydata['position']=$position;
			$idtmp=$_POST['parentid'];
			if($idtmp==0)
			{
				$mydata['level']=1;
				$mydata['parentid']=$idtmp;
			}
			else
			{
				$row=$this->Mtopic->topic_detail($idtmp);//Xem thông tin
				$mydata['level']=$row['level']+1;
				$mydata['parentid']=$idtmp;	
			}
			$this->Mtopic->topic_update($mydata, $id);
			$this->session->set_flashdata('success', 'Cập nhật chủ đề bài viết thành công');
			redirect('admin/topic','refresh');
		} 
		$this->data['view']='update';
		$this->data['title']='Cập nhật chủ đề bài viết';
		$this->load->view('backend/layout', $this->data);
	}

	public function status($id)
	{
		$row=$this->Mtopic->topic_detail($id);
		$status=($row['status']==1)?0:1;
		$mydata= array('status' => $status);
		$this->Mtopic->topic_update($mydata, $id);
		$this->session->set_flashdata('success', 'Cập nhật chủ đề bài viết thành công');
		redirect('admin/topic','refresh');
	}

	public function trash($id)
	{
		$row = $this->Mtopic->topic_detail($id);
		$count_content = $this->Mcontent->content_count_parentid($row['id']);
		$count_topic = $this->Mtopic->topic_count_parentid($row['id']);
		if($count_content > 0)
		{
			$this->session->set_flashdata('error', 'Còn bài viết bên trong ! Hãy xóa bài viết trước !');
			redirect('admin/topic','refresh');
		}
		else
		{
			if($count_topic > 0)
			{
				$this->session->set_flashdata('error', 'Còn danh mục con bên trong ! Không thể thực hiện !');
				redirect('admin/topic','refresh');
			}
			else
			{
				$mydata= array('trash' => 0);
				$this->Mtopic->topic_update($mydata, $id);
				$this->session->set_flashdata('success', 'Xóa chủ đề bài viết vào thùng rác thành công');
				redirect('admin/topic','refresh');
			}
		}
	}

	public function restore($id)
	{
		$this->Mtopic->topic_restore($id);
		$this->session->set_flashdata('success', 'Khôi phục chủ đề bài viết thành công');
		redirect('admin/topic/recyclebin','refresh');
	}

	public function recyclebin()
	{
		$this->load->library('phantrang');
		$limit=10;
		$current=$this->phantrang->PageCurrent();
		$first=$this->phantrang->PageFirst($limit, $current);
		$total=$this->Mtopic->topic_trash_count();
		$this->data['strphantrang']=$this->phantrang->PagePer($total, $current, $limit, $url='admin/topic/recyclebin');
		$this->data['list']=$this->Mtopic->topic_trash($limit, $first);
		$this->data['view']='recyclebin';
		$this->data['title']='Thùng rác chủ đề bài viết';
		$this->load->view('backend/layout', $this->data);
	}

	public function delete($id)
	{
		$this->Mtopic->topic_delete($id);
		$this->session->set_flashdata('success', 'Xóa chủ đề bài viết thành công');
		redirect('admin/topic/recyclebin','refresh');
	}

}

/* End of file Topic.php */
/* Location: ./application/controllers/Topic.php */