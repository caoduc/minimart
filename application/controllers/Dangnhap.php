<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dangnhap extends CI_Controller {
	// Hàm khởi tạo
    function __construct() {
        parent::__construct();
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mcategory');
        $this->load->model('frontend/Mtopic');
        $this->load->model('frontend/Mcustomer');
        $this->load->model("frontend/Mproduct");
        $this->data['com']='dangnhap';
    }

    public function dangnhap()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Tên đăng nhập', 'required|min_length[6]|max_length[32]');
        $this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]|max_length[32]');
        if($this->form_validation->run() ==TRUE)
        {
            $username = $_POST['username'];
            $password = md5($_POST['password']);
            if($this->Mcustomer->customer_login($username, $password)!=FALSE)
            {
                $row = $this->Mcustomer->customer_login($username, $password);
                $this->session->set_userdata('user',$row['username']);
                $this->session->set_userdata('fullname',$row['fullname']);
                $this->session->set_userdata('id',$row['id']);
                $this->session->set_userdata('access',$row['access']);
                redirect('tai-khoan','refresh');
            }
            else
            {
                $this->data['error']='Tên đăng nhập hoặc mật khẩu không chính xác';
                $this->data['title']='Đăng nhập tài khoản';
                $this->data['view']='dangnhap';
                $this->load->view('frontend/layout',$this->data);
            }
        }
        else
        {
            $this->data['title']='Đăng nhập tài khoản - Mini Mart';
            $this->data['view']='dangnhap';
            $this->load->view('frontend/layout',$this->data);
        }     
    }

    public function dangxuat()
    {
        $array_items = array('user', 'fullname', 'id', 'access', 'cart');
        $this->session->unset_userdata($array_items);
        //$this->session->sess_destroy();
        redirect('trang-chu','refresh');
    }
}