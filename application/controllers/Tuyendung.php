<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tuyendung extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/Mmenu');
		$this->load->model("frontend/Mproduct");
		$this->load->model('frontend/Mtopic');
        $this->data['com']='tuyendung';
	}

	public function index()
	{
		$this->data['title']='Tuyển dụng - Mini Mark';
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}

}
