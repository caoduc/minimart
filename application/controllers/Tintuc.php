<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tintuc extends CI_Controller {
	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->model('frontend/Mcontent');
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mtopic');
        $this->load->model("frontend/Mproduct");
        $this->data['com']='tintuc';
    }
    
	public function index()
	{
		$aurl= explode('/',uri_string());
		$catlink=$aurl[0];
        $catid = $this->Mtopic->topic_id($catlink);
		$this->load->library('phantrang');
		$limit=4;
		$current=$this->phantrang->PageCurrent();
		$first=$this->phantrang->PageFirst($limit, $current);
		$total=$this->Mcontent->content_chude_count($catid);
		$this->data['strphantrang']=$this->phantrang->PagePer($total, $current, $limit, $url='tin-tuc');
        $this->data['list']=$this->Mcontent->content_chude($catid, $limit,$first);
        $this->data['title']='Tin tức - Mini Mark';  
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}

	public function detail()
	{
		$aurl = explode('/', uri_string());
		$link = $aurl[1];
		$row = $this->Mcontent->content_detail($link);
		$this->data['row']=$row;
		$this->data['title']=$row['title'];
		$this->data['view']='detail';
		$this->load->view('frontend/layout',$this->data);
	}
}