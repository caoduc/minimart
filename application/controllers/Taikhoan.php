<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taikhoan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/Mtopic');
		$this->load->model("frontend/Mproduct");
		$this->load->model("frontend/Mcustomer");
		$this->load->model("frontend/Morder");
		$this->load->model("frontend/Morderdetail");
        $this->data['com']='taikhoan';
        if(!$this->session->userdata('user'))
		{
			redirect('dang-nhap','refresh');
		}
		$this->data['customer']=$this->Mcustomer->customer_detail_id($this->session->userdata('id'));
	}

	public function index()
	{
		$this->data['title']='Tài khoản - Mini Mark';  
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}

	public function order($id)
	{
		$aurl= explode('/',uri_string());
		$id = $aurl[2];
		$this->data['orderid'] = $id;
		$this->data['row'] = $this->Morderdetail->orderdetail_order($id);
		$this->data['title']='Chi tiết đơn hàng - Mini Mark';  
		$this->data['view']='order_detail';
		$this->load->view('frontend/layout',$this->data);
	}

	public function update()
	{
		$d=getdate();
		$today=$d['year']."/".$d['mon']."/".$d['mday']." ".$d['hours'].":".$d['minutes'].":".$d['seconds'];
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->form_validation->set_rules('name', 'Họ và tên', 'required');
		$this->form_validation->set_rules('phone', 'Số điện thoại', 'required|min_length[6]|numeric|max_length[11]');
		$this->form_validation->set_rules('address', 'Địa chỉ', 'required');
		if($this->session->userdata('user'))
		{
			$id = $this->session->userdata('id');
			if($this->form_validation->run() == TRUE)
			{
				$mydata= array(
					'fullname' =>$_POST['name'],
					'phone'=>$_POST['phone'],
					'address'=>$_POST['address'],
					'gender' =>$_POST['gender'],
					'birthday' =>$_POST['birthday'],
					'trash'=>1,
					'status'=>1,
					'access'=>1
				);
				$this->Mcustomer->customer_update($mydata, $id);
				$this->session->set_flashdata('success', 'Cập nhật thông tin thành công');
				redirect('tai-khoan','refresh');
			}
		}
		$this->data['title']='Cập nhật thông tin - Mini Mark';  
		$this->data['view']='update';
		$this->load->view('frontend/layout',$this->data);
	}

}
