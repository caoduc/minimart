<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Khachhangchiase extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/Mmenu');
		$this->load->model("frontend/Mproduct");
        $this->data['com']='khachhangchiase';
	}

	public function index()
	{
		$this->data['title']='Khách hàng chia sẻ - Mini Mark';  
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}

}

/* End of file Khachhangchiase.php */
/* Location: ./application/controllers/Khachhangchiase.php */