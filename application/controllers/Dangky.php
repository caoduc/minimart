<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dangky extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mcategory');
        $this->load->model('frontend/Mtopic');
        $this->load->model('frontend/Mcustomer');
        $this->load->model("frontend/Mproduct");
        $this->data['com']='dangky';
    }

     public function index()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->form_validation->set_rules('username', 'Tên đăng nhập', 'required|min_length[6]|max_length[32]|callback_check_username');
        $this->form_validation->set_rules('name', 'Họ và tên', 'required|min_length[8]');
        $this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]|max_length[32]');
        $this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu', 'required|matches[password]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[db_user.email]');
        $this->form_validation->set_rules('phone', 'Số điện thoại', 'required|min_length[6]|numeric|is_unique[db_user.phone]|max_length[11]');
        
        if($this->form_validation->run() ==TRUE)
        {
            $data = array(
                'username'     => $this->input->post('username'),   
                'fullname'     => $this->input->post('name'),
                'email'    => $this->input->post('email'),
                'phone'    => $this->input->post('phone'),
                'password' => md5($this->input->post('password'))
            );
            $this->Mcustomer->customer_insert($data);
            $this->data['msg']='Đăng ký thành công !';
            redirect('dang-nhap','refresh');
        }  
        $this->data['title']='Đăng ký tài khoản - Mini Mart';   
        $this->data['view']='index';
        $this->load->view('frontend/layout',$this->data);  
    }

    function check_username()
    {
     
        $username = $this->input->post('username');
        if($this->Mcustomer->customer_check_username($username))
        {
            $this->form_validation->set_message(__FUNCTION__, 'Tên đăng nhập đã được sử dụng');
            return FALSE;
        }
        return TRUE;
    }

    function check_mail()
    {
     
        $mail = $this->input->post('mail');
        if($this->Mcustomer->customer_check_mail($mail))
        {
            $this->form_validation->set_message(__FUNCTION__, 'Email đã được sử dụng');
            return FALSE;
        }
        return TRUE;
    }

}

/* End of file Dangki.php */
/* Location: ./application/controllers/Dangki.php */