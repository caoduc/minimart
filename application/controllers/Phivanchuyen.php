<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phivanchuyen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/Mcontent');
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mtopic');
        $this->load->model("frontend/Mproduct");
        $this->data['com']='phivanchuyen';
	}

	public function index()
	{
		$aurl= explode('/',uri_string());
		$catlink=$aurl[0];
        $catid = $this->Mtopic->topic_id($catlink);
        $this->data['row']=$this->Mcontent->content_gioithieu($catid);  
        $this->data['title']='Phí vận chuyển - Mini Mark';  
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}

}