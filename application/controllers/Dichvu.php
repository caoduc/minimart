<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dichvu extends CI_Controller {
	
	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->model('frontend/Mmenu');
        $this->load->model('frontend/Mtopic');
        $this->load->model("frontend/Mproduct");
        $this->data['com']='dichvu';
    }

	public function index()
	{
		$this->data['title']='Dịch vụ - Mini Mark';  
		$this->data['view']='index';
		$this->load->view('frontend/layout',$this->data);
	}
}