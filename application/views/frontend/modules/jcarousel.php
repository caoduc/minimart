<div class="connected-carousels">
    <div class="stage">
        <div class="carousel carousel-stage" data-jcarousel="true">
            <ul>
                <?php 
                    $img = $row['img'];
                    $mang = explode('#', $img);
                    foreach ($mang as $value) :?>
                        <li><img src="public/images/products/<?php echo $value; ?>" width="600px" max-width="100%" min-height="400px" alt=""></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <a href="#" class="prev prev-stage" data-jcarouselcontrol="true"><span>‹</span></a>
        <a href="#" class="next next-stage inactive" data-jcarouselcontrol="true"><span>›</span></a>
    </div>
    <div class="navigation">
        <a href="#" class="prev prev-navigation" data-jcarouselcontrol="true">‹</a>
        <a href="#" class="next next-navigation inactive" data-jcarouselcontrol="true">›</a>
        <div class="carousel carousel-navigation" data-jcarousel="true">
            <ul style="left: -120px; top: 0px;">
                <?php 
                    $img = $row['img'];
                    $mang = explode('#', $img);
                    foreach ($mang as $value) :?>
                        <li data-jcarouselcontrol="true" class="">
                            <img src="public/images/products/<?php echo $value; ?>" width="50" height="50" alt="">
                        </li>
                <?php endforeach; ?>
                
            </ul>
        </div>
    </div>
</div>