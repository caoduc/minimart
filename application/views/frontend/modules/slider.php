<div id="carousel-id" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<?php  
		$list = $this->Mslider->list_img_banner('slideshow');
		foreach ($list as $row):?>
			<li data-target="#carousel-id" data-slide-to="<?php echo $row['options'] ?>" class="<?php if($row['active']==1){ echo 'active';} ?>"></li>
		<?php endforeach; ?>
	</ol>
	<div class="carousel-inner">
		<?php  
		$list = $this->Mslider->list_img_banner('slideshow');
		foreach ($list as $row):?>
		<div class="item <?php if($row['active']==1){ echo 'active';} ?>">
			<a href="<?php echo $row['link'] ?>"><img src="<?php echo base_url()?>public/images/banners/<?php echo $row['img'] ?>" style="width: 100%; height: 480px;">
			</a>
		</div>
		<?php endforeach; ?>
	</div>
	<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
