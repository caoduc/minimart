
<nav id="primary-menu">
	<ul class="main">
		<li class = "li-menu">
			<a href="san-pham/may-lanh">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_1.png">
					<div>Máy lạnh</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/tu-lanh">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_2.png">
					<div>Tủ lạnh</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/may-giat">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_3.png">
					<div>Máy giặt</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/quat-may">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_4.png">
					<div>Quạt máy</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/bep-gas">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_5.png">
					<div>Bếp gas</div>
				</div>
			</a>
		</li>
		<li class = "li-menu menu">
			<a href="san-pham/tivi">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_6.png">
					<div>Tivi</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/dien-thoai-di-dong">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_7.png">
					<div>Điện thoại</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/may-anh-du-lich">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_8.png">
					<div>Máy ảnh </div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/may-tinh-xach-tay">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_9.png">
					<div>Laptop</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/dien-thoai-di-dong">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_10.png">
					<div>Iphone</div>
				</div>
			</a>
		</li>
		<li class = "li-menu menu">
			<a href="san-pham/may-tinh-bang">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_11.png">
					<div>Tablet</div>
				</div>
			</a>
		</li>
		<li class = "li-menu">
			<a href="san-pham/may-in">
				<div class="menu-icon">
					<img src="public/images/templates/main_menu_icon_12.png">
					<div>Máy in</div>
				</div>
			</a>
		</li>		
	</ul>
</nav>