<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
	<div class="logo-footer">
		<a href="#"><img src="public/images/banners/footer_logo.png" alt="Mini Mark"></a>
		<span>Chất lượng là nhất không nói nhiều.</span>
	</div>
</div>
<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 first">
	<h4>Giới thiệu</h4>
	<?php  
		$list = $this->Mtopic->topic_position(0, 'footer-left');
			$html_menu='<ul>';
				foreach ($list as $menu) {
					$html_menu.='<li>';
						$html_menu.="<a href=' ".$menu['link']." ' title=' ".$menu['name']." '> ".$menu['name']."</a>";
					$html_menu.="</li>";
				}
			$html_menu.="</ul>";
		echo $html_menu;
	?>
</div>
<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
	<h4>Hỗ trợ khách hàng</h4>
	<?php  
		$list = $this->Mtopic->topic_position(0, 'footer-right');
			$html_menu='<ul>';
				foreach ($list as $menu) {
					$html_menu.='<li>';
						$html_menu.="<a href=' ".$menu['link']." ' title=' ".$menu['name']." '> ".$menu['name']."</a>";
					$html_menu.="</li>";
				}
			$html_menu.="</ul>";
		echo $html_menu;
	?>
</div>
<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 end">
	<h4>Liên hệ</h4>
	<p>Kênh mua sắm trực tuyến giá tốt hàng đầu Việt Nam</p>
	<address>
		<p><img src="public/images/templates/pre_footer_address_icon.png">Thủ Dầu Một, Bình Dương</p>
		<p><a href="tel:0367757389"><img src="public/images/templates/pre_footer_phone_icon.png">1900 008 099</a></p>
		<p><a href="mailto:duccao147@gmail.com"><img src="public/images/templates/pre_footer_email_icon.png">teamdevt@gmail.com</a></p>
	</address>
</div>