<div class="top-cart" id="cart" onclick="ShowCartContent()">
	<a href="gio-hang"><img src="public/images/templates/cart_bg.png" alt="Cart"></a>
	<span>
		<?php  
			if($this->session->userdata('cart'))
			{
				$val = $this->session->userdata('cart');
				$sl = count($val);
				echo $sl;
			}
			else
			{
				echo 0;
			}
		?>
	</span>
</div>
<div class="top-cart-content" id ="cart-content">
	<?php  
	if($this->session->userdata('cart')):
		$val = $this->session->userdata('cart');
		$sl = count($val);
		if($sl > 0):?>
			<div class="top-cart-title">
				<h4>Giỏ hàng</h4>
			</div>
			<div class="top-cart-items">
				<?php foreach ($val as $key => $value) : 
					$row = $this->Mproduct->product_detail_id($key);?>
					<div class="top-cart-item clearfix">
						<div class="top-cart-item-image">
							<a href="<?php echo $row['alias'] ?>">
								<img src="public/images/products/<?php echo $row['img']?>" alt="<?php echo $row['name']; ?>">
							</a>
						</div>
						<div class="top-cart-item-desc">
							<div class="top-cart-item-title">
								<a href="<?php echo $row['alias'] ?>"><?php echo $row['name'] ?></a>
							</div>
							<span class="top-cart-item-price">
								<?php 
									if($row['price_sale'] > 0) 
									{
										echo (number_format($row['price_sale'])).'₫';
									}
									else
									{
										echo (number_format($row['price'])).'₫';
									}
								?>
							</span>
							<span class="top-cart-item-quantity">x <?php echo $value; ?></span>
						</div>
					</div>
				<?php endforeach; ?>	
			</div>
			<div class="view-cart">
				<div class="btn-view">
					<a class="button" href="<?php echo base_url() ?>gio-hang">
					<span class="text">Xem giỏ hàng</span>
				</a>
				</div>
				<div class="cart-total">
					<span>
						<?php $total = 0; ?>
						<?php foreach ($val as $key => $value) : 
							$row = $this->Mproduct->product_detail_id($key);?>
							<?php
								if($row['price_sale'] > 0)
								{
									$sum = $row['price_sale'] * $value;
								}
								else
								{
									$sum = $row['price'] * $value;
								}
								$total += $sum;
							?>	
						<?php endforeach; ?>
						<?php echo (number_format($total)).'₫'; ?>
					</span>
				</div>
			</div>
		<?php endif;?>
	<?php else: ?>
		<div class="minicart-header">Chưa có sản phẩm nào trong giỏ!</div>
		<div class="minicart-footer">
			<a class="button" href="<?php echo base_url() ?>san-pham">
				<span class="text">VÀO CỬA HÀNG</span>
			</a>
		</div>
	<?php endif;?>
</div>