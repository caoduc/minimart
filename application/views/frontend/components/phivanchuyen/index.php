<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row content-ct">
	<div class="container">
		<div class = "col-xs-12 col-sm-12 col-md-12 col-sm-12">
			<?php foreach( $row as $val): ?>
				<div class="info">
					<h2><?php echo $val['title'] ?></h2>
					<?php echo $val['fulltext'] ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>