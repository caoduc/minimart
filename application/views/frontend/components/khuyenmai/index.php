<div class="row wraper">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row content-ct">
	<div class="container">
		<div class = "col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<?php foreach ($list as $item) :?>
				<div class="row news">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<a href="khuyen-mai/<?php echo $item['alias']; ?>"><img src="public/images/posts/<?php echo $item['img']; ?>" alt=""></a>
					</div>
					<div class="col-md-8 entry">
						<div class="entry-title">
							<a href="khuyen-mai/<?php echo $item['alias']; ?>"><h3><?php echo $item['title']; ?></h3></a>
						</div>
						<ul class="entry-meta clearfix">
							<li><i class="fa fa-calendar"></i><?php echo $item['created']; ?></li>
							<li><i class="fa fa-user"></i><?php echo $item['created_by']; ?></li>
						</ul>
						<div class="entry-content">
							<a href="tin-tuc/<?php echo $item['alias']; ?>" class="more-link">Xem tiếp</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<div class = "row text-center">
				<ul class ="pagination">
					<?php echo $strphantrang; ?>
				</ul>
			</div>
		</div>
		<div class="col-md-3 col-lg-3 hidden-xs hidden-sm">
			<div class="post_content">
				<?php 
					$this->load->view('frontend/modules/panel_right');
					$this->load->view('frontend/modules/news');
				?>
			</div>
		</div>
	</div>
</div>