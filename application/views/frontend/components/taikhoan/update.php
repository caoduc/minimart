<?php echo form_open_multipart('tai-khoan/cap-nhat') ?>
<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row account">
	<div class="container">
		<div class="customer-title">
			<h4>Cập nhật tài khoản</h4>
		</div>
		<div class="customer-detail">		
			<div class="content-wrapper" style="min-height: 454px;">
			    <form action="tai-khoan/cap-nhat" enctype="multipart/form-data" method="post" accept-charset="utf-8">
			        <section class="content">
			            <div class="row">
			                <div class="col-md-12">
			                    <div class="box" >
			                        <div class="box-body" style="max-width: 750px;">
			                            <div class="form-group" style="margin-top: 50px;">
			                                <label>Họ và tên:<span class = "maudo">(*)</span></label>
			                                <input type="text" name="name" placeholder="Họ và tên" class="form-control" value="<?php echo $customer['fullname']; ?>">
			                            </div>
			                            <div class="error" id="name_error" style="margin-bottom: 15px;"><?php echo form_error('name')?></div>
			                            <div class="form-group">
			                                <label>Số điện thoại<span class = "maudo">(*)</span></label>
			                                <input type="text" name="phone" placeholder="Số điện thoại" class="form-control" value="<?php echo $customer['phone']; ?>">
			                            </div>
			                            <div class="error" id="name_error" style="margin-bottom: 15px;"><?php echo form_error('phone')?></div>
			                            <div class="form-group">
			                                <label>Giới tính </label>
			                                <select name="gender" class="form-control" style="width:30%">
			                                    <option value="1">Nữ</option>
			                                    <option value="0">Nam</option>
			                                </select>
			                            </div>
			                            <div class="form-group" style="width:30%">
			                                <label>Ngày sinh</label>
			                                <input type="date" name="birthday" placeholder="Ngày sinh" class="form-control" value="<?php echo $customer['birthday']; ?>">
			                            </div>
			                            <div class="form-group">
			                                <label>Địa chỉ<span class = "maudo">(*)</span></label>
			                                <input type="text" name="address" placeholder="Vui lòng nhập đúng địa chỉ để tiện cho việc giao hàng" class="form-control" value="<?php echo $customer['address']; ?>">
			                            </div>
			                            <div class="error" id="name_error" style="margin-bottom: 15px;"><?php echo form_error('address')?></div>
			                        </div>
			                        <button name="update" type="submit" class="btn btn-primary btn-sm">Cập nhật</button>
			                        <a href="tai-khoan"><button name="update" type="submit" class="btn btn-primary btn-sm">Quay lại</button></a>
			                    </div><!-- /.box -->
			                </div><!-- /.col -->
			            </div><!-- /.row -->
			        </section><!-- /.content -->
			    </form>         
			</div>
		</div>
		<div class="margin" style="margin-top: 50px;"></div>
	</div>
</div>