<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row account">
	<div class="container">
	<?php $detail = $this->Morder->order_orderid($orderid); ?>
		<div class="customer-title">
			<h5 style="font-size: 1em;">Đơn hàng #<?php echo $orderid; ?>, đặt ngày: <?php echo($detail['orderdate']); ?></h5>
		</div>
		<div class="row">
			<div class="container customer-detail">
				<h4>Thông tin nhận hàng</h4>
				<p>Tình trạng thanh toán:
					<?php  
			  			if($detail['status'] == 1)
			  			{
			  				echo "Đã thanh toán";
			  			}
			  			else
			  			{
			  				echo "Chưa thanh toán";
			  			}
			  		?>
				</p>
				<div class="customer-detail">
					<p>Họ và tên: <?php echo($customer['fullname']); ?></p>
					<p>Địa chỉ: <?php echo $detail['district'].', '.$detail['province']; ?></p>
					<p>Số điện thoại: <?php echo $customer['phone']; ?></p>
					<br />
				</div>
			</div>
		</div>
		<div class="table-responsive account-table">		
			<table class="table" style="border: 1px solid #dcdcdc;">
				<thead>
					<tr class="active" style="color: #786c6c;">
						<th>Tên sản phẩm</th>
						<th>Giá bán</th>
						<th>Số lượng</th>
						<th>Thành tiền</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$ship = 40000; $total = 0;
						$list = $this->Morderdetail->orderdetail_order($orderid);
						foreach($list as $row):
							$product = $this->Mproduct->product_detail_id($row['productid']);
					?>
					<tr>
						<td style="border-right: 1px solid #dcdcdc;">
							<a href="<?php echo $product['alias']; ?>"><?php echo $product['name']; ?></a>
						</td>
						<td style="border-right: 1px solid #dcdcdc;">
							<?php echo number_format($row['price']); ?>₫
						</td>
						<td style="border-right: 1px solid #dcdcdc;"><?php echo $row['amount']; ?></td>
						<td style="color: #e90000;"><?php $tong = $row['price'] * $row['amount']; echo number_format($tong); $total += $tong;?>₫</td>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="3" style="color: #e90000; border-right: 1px solid #dcdcdc;">Chưa thuế:</td>
						<td style="color: #e90000;"><?php echo number_format($total);?>₫</td>
					</tr>
					<tr>
						<td colspan="3" style="color: #e90000; border-right: 1px solid #dcdcdc;">Phí vận chuyển:</td>
						<td style="color: #e90000;"><?php echo number_format($ship);?>₫</td>
					</tr>
					<tr>
						<td colspan="3" style="color: #e90000; border-right: 1px solid #dcdcdc;">Tổng cộng:</td>
						<td style="color: #e90000;"><?php echo number_format($detail['total']);?>₫</td>
					</tr>
				</tbody>
			</table>
		<div id="undo"><a href="tai-khoan" class = "fa fa-undo undo-order"> Quay lại tài khoản</a></div>
		</div>
		<div class="margin" style="margin-top: 50px;"></div>
	</div>
</div>