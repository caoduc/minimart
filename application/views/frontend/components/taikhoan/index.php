<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row account">
	<div class="container">
		<div class="customer-title">
			<h4>Thông tin tài khoản</h4>
		</div>
		<div class="customer-detail">
			<br />
			<p>Tên đăng nhập: <?php echo $customer['username']; ?></p>
			<p>Họ và tên: <?php echo $customer['fullname']; ?></p>
			<p>Email: <?php echo $customer['email']; ?></p>
			<p>Giới tính:
				<?php 
					if($customer['gender'] == 0)
					{
						echo 'Nam';
					}
					else
					{
						echo 'Nữ';
					}
				?>
			</p>
			<p><?php if($customer['birthday'] != 0){echo 'Ngày sinh: '.$customer['birthday'];}?></p>
			<p><?php if(!empty($customer['address'])){echo 'Địa chỉ: '.$customer['address'];}?></p>
			<p><?php if(!empty($customer['phone'])){echo 'Số điện thoại: '.$customer['phone'];}?></p>
			<?php 
				if(empty($customer['address']) || empty($customer['phone']) || $customer['birthday'] == '0000-00-0')
				{
					echo "<p><i>(Tài khoản của bạn vẫn chưa cập nhật đủ thông tin, nhấn vào <a href='tai-khoan/cap-nhat'>đây</a> để cập nhật !)</i></p>";
				}
				else
				{
					echo "<p><a href='tai-khoan/cap-nhat' style = 'text-decoration: none;'>Cập nhật tài khoản</a></p>";
				}
			?>		
		</div>
		<div class="margin" style="margin-top: 50px;"></div>
		<div class="customer-title">
			<h4>Đơn hàng đã đặt</h4>
		</div>
		<div class="table-responsive account-table">		
			<table class="table">
				<thead>
					<tr class="active" style="color: #786c6c;">
						<th>Mã đơn hàng</th>
						<th>Ngày đặt</th>
						<th>Trạng thái thanh toán</th>
						<th>Tổng tiền</th>
					</tr>
				</thead>
				<tbody>
				<?php  
					$order = $this->Morder->order_listorder_customerid($customer['id']);
					foreach ($order as $row) :?>
						<tr>
							<td>
								<div  data-toggle="tooltip" data-placement="top" title="Xem đơn hàng"><a href="tai-khoan/don-hang/<?php echo $row['id'] ?>">#<?php echo $row['id'] ?></a>
								</div>
							</td>
							<td><?php echo $row['orderdate']; ?></td>
							<td>
						  		<?php  
						  			if($row['status'] == 1)
						  			{
						  				echo "Đã thanh toán";
						  			}
						  			else
						  			{
						  				echo "Chưa thanh toán";
						  			}
						  		?>
						  	</td>
							<td><?php echo number_format($row['total']); ?>₫</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div class="margin" style="margin-top: 50px;"></div>
	</div>
</div>