<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>

<div class="row content-wrap">
	<div class="container">
		<div class="hidden-xs hidden-sm sidebar-widgets-wrap">
			<?php  
				$this->load->view('frontend/modules/widgets-links');
				$this->load->view('frontend/modules/widget');
			?>
		</div>
		<?php
			$html_list='<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">';
				$html_list.='<div class = " row sort-wrapper">';
					$html_list.='<div class = "browse-tags col-md-8 col-sm-12 col-xs-12 pull-right">';
						$html_list.='<div class = "col-md-9 col-sm-9 col-xs-7 text-right nopadding">';
							$html_list.='<span>Sắp xếp theo:</span>';
						$html_list.='</div>';
						$html_list.='<div class ="col-md-3 col-sm-3 col-xs-5 nopadding">';
							$html_list.='<span class = "custom-dropdown custom-dropdown--white pull-right">';
								$html_list.='<select id ="sortControl" class = "sort-by form-control input-sm" onchange="sortby(this.value)">';
									if($this->session->userdata('sortby-category'))
									{
										$data = $this->session->userdata('sortby-category');
										$sort = $data[0].'-'.$data[1];
										if($sort == 'number_buy-desc')
										{
											$html_list.='<option value="number_buy-desc" selected>Bán chạy nhất</option>';
										}
										else
										{
											$html_list.='<option value="number_buy-desc">Bán chạy nhất</option>';
										}
										if($sort == 'name-asc')
										{
											$html_list.='<option value="name-asc" selected>A → Z</option>';
										}
										else
										{
											$html_list.='<option value="name-asc" >A → Z</option>';
										}
										if($sort == 'name-desc')
										{
											$html_list.='<option value="name-desc" selected>Z → A</option>';
										}
										else
										{
											$html_list.='<option value="name-desc">Z → A</option>';
										}
										if($sort == 'price-asc')
										{
											$html_list.='<option value="price-asc" selected>Giá tăng dần</option>';
										}
										else
										{
											$html_list.='<option value="price-asc">Giá tăng dần</option>';
										}
										if($sort == 'price-desc')
										{
											$html_list.='<option value="price-desc" selected>Giá giảm dần</option>';
										}
										else
										{
											$html_list.='<option value="price-desc">Giá giảm dần</option>';
										}
										if($sort == 'created-desc')
										{
											$html_list.='<option value="created-desc" selected>Hàng mới nhất</option>';
										}
										else
										{
											$html_list.='<option value="created-desc">Hàng mới nhất</option>';
										}
										if($sort == 'created-asc')
										{
											$html_list.='<option value="created-asc" selected>Hàng cũ nhất</option>';
										}
										else
										{
											$html_list.='<option value="created-asc">Hàng cũ nhất</option>';
										}

									}
									else
									{
										$html_list.='<option>Thứ tự</option>';
										$html_list.='<option value="created-desc">Mặc định</option>';
										$html_list.='<option value="number_buy-desc">Bán chạy nhất</option>';
										$html_list.='<option value="name-asc">A → Z</option>';
										$html_list.='<option value="name-desc">Z → A</option>';
										$html_list.='<option value="price-asc">Giá tăng dần</option>';
										$html_list.='<option value="price-desc">Giá giảm dần</option>';
										$html_list.='<option value="created-desc">Hàng mới nhất</option>';
										$html_list.='<option value="created-desc">Hàng cũ nhất</option>';
									}
								$html_list.='</select>';
							$html_list.='</span>';
						$html_list.='</div>';
					$html_list.='</div>';
				$html_list.='</div>';
				$html_list.='<div class ="row index-product">';
					if(count($list))
					{
						foreach ($list as $val) 
						{	$html_list.='<div class = "product_category">';
								$html_list.='<div class="col-sm-3 col-xs-6">';
									$html_list.='<div class="info-product">';
										$html_list.='<div class="header-info">';
											$html_list.="<a href='".$val['alias']." ' title=' ".$val['name']." '>";
												if(str_word_count($val['name']) > 6)
												{
													$html_list.=substr($val['name'], 0, 27).'...';
												}
												else
												{
													$html_list.=$val['name'];
												}
											$html_list.='</a>';
										$html_list.='</div>';

										$mang = explode('#', $val['img']);
										$img = $mang[0];

										$html_list.='<div class="image-product">';
											$html_list.="<a href='".$val['alias']."'>";
												$html_list.="<img src='public/images/products/".$img."'>";
											$html_list.='</a>';
											if($val['sale'] > 0)
											{
												$html_list.='<div class="sale-flash">';
													$html_list.="-".$val['sale']."%";
												$html_list.='</div>';
											}
											$html_list.='<div class="product-overlay hidden-xs">';
												$html_list.="<a href='gio-hang' onclick='ham(".$val['id'].");' class='product_quick_add' data-toggle='tooltip' data-original-title='Thêm vào giỏ'>";
													$html_list.='<i class="fa fa-shopping-cart"></i>';
												$html_list.='</a>';
												$html_list.="<a href='".$val['alias']."' class='item-quick-view fancybox-fast-view ' data-toggle='tooltip' data-original-title='Xem nhanh'>";
													$html_list.='<i class="fa fa-eye"></i>';
												$html_list.='</a>';
											$html_list.='</div>';
										$html_list.='</div>';
										$html_list.='<div class="price-product">';
											if($val['sale'] > 0)
											{
												$html_list.="".number_format($val['price_sale'])."₫";
												$html_list.='<br .>';
											}
											else
											{
												$html_list.="".number_format($val['price'])."₫";
												$html_list.='<br .>';
											}
											if($val['sale'] > 0)
											{
												$html_list.='<div>';
												$html_list.="".number_format($val['price'])."₫";
												$html_list.='</div>';
											}
										$html_list.='</div>';
										$html_list.="<div style='clear: both;' class='clearfix'></div>";
										$html_list.="<div class='btn-product hidden-sm hidden-md hidden-lg'>";
											$html_list.="<a href='gio-hang' class='btn btn-primary' role='button' style='width: 100%;' onclick='ham(".$val['id'].");'><i class='fa fa-shopping-cart'></i> Thêm vào giỏ</a>";
										$html_list.="</div>";
									$html_list.='</div>';
								$html_list.='</div>';
							$html_list.='</div>';
						}
						$html_list.='</div>';
					}
					else
					{
						$html_list.= '<div class="content_title">Chưa có sản phẩm nào trong danh mục này.</div>';
					}
					$html_list.='<div class = "row text-center">';
						$html_list.='<ul class ="pagination">';
							$html_list.=$strphantrang;
						$html_list.='</ul>';
					$html_list.='</div>';
				$html_list.='</div>';
			$html_list.='</div>';
			echo $html_list;
		?>
	</div>
</div>
<script>
	function ham(id)
	{
		var strurl="<?php echo base_url();?>"+'/sanpham/addcart';
		jQuery.ajax({
		  url: strurl,
		  type: 'POST',
		  dataType: 'json',
		  data: {id: id},
		  success: function(data) {
		    //alert(data)
		  }
		});	
	}
	function sortby(option)
    {
        var strurl="<?php echo base_url();?>"+'/sanpham/category';
        jQuery.ajax({
            url: strurl,
            type: 'POST',
            dataType: 'json',
            data: {'sapxep-category': option},
            success: function(data) {
                $('#list-product').html(data);
            }
        });
    }
</script>