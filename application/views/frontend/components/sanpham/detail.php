<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>

<div class="row content-pd-wrap">
	<div class="container clearfix">
		<div class="post-content nobottommargin clearfix">
			<div class="product-detail">
				<div class="signle-product">
					<form action="" method="post" id="ProductDetailsForm">
						<?php if($row['alias']):?>
							<div class="pd_detail">
								<div class="pd_title">
									<h3><?php echo $row['name']; ?></h3>
								</div>
								<div style="margin-bottom: 25px; margin-top: 20px;"> 
									<div class="col-xs-12 col-sm-8 col-md-8">
										<div class="pd-img-wrap noleftpadding">
											<div class="img">
												<?php $this->load->view('frontend/modules/jcarousel');?>
											</div>
										</div>
										<?php if($row['sale'] > 0) :?>
											<div class="sale-flash">
												-<?php echo $row['sale']; ?>%
											</div>
										<?php endif; ?>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4">
										<div class="product-price col-xs-12 col-md-12 noleftpadding">
											<?php if($row['price_sale'] > $row['price']) :?>
												<p>Giá khuyến mãi: <ins><?php echo number_format($row['price_sale']) ?>₫</ins></p>
												<p>Giá gốc: <del><?php echo number_format($row['price']) ?>₫</del></p>
											<?php else: ?>
												<p>Giá bán: <ins><?php echo number_format($row['price']) ?>₫</ins></p>
											<?php endif; ?>
										</div>
										<div class="clear"></div>
										<div class="line"></div>
										<div class="quantity product-quantity clearfix col-sm-6 col-xs-6 col-md-6 noleftpadding">
											<input type="button" value="-" class="minus">
											<input type="text" id="product_quantity" readonly="" step="1" min="1" name="quantity" value="1" title="Qty" class="qty" size="4">
											<input type="button" value="+" class="plus">
										</div>
										<button type="submit" id="addtocart" class="add-to-cart button nomargin col-sm-6 col-xs-6 col-md-6 nopadding">Thêm vào giỏ</button>
										<button type="submit" id="buynow" class="button col-xs-12 nopadding">Mua ngay</button>
										<div class="line"></div>
										<div class="pd_policies">
											<ul class="nostyled">
												<li class="col-sm-6 nopadding">
													<a href="van-chuyen"><img src="public/images/templates/pd_policies_1.png"> Miễn phí vận chuyển </a>
												</li>
												<li class="col-sm-6 nopadding">
													<a href="qua-tang"><img src="public/images/templates/pd_policies_2.png"> Quà tặng </a>
												</li>
												<li class="col-sm-6 nopadding">
													<a href="doi-tra-hang"><img src="public/images/templates/pd_policies_3.png"> Đổi trả hàng </a>
												</li>
												<li class="col-sm-6 nopadding">
													<a href=""><img src="public/images/templates/pd_policies_4.png"> HOTLINE: 08 7300 5757 </a>
												</li>
											</ul>
										</div>
										<div class="clear"></div>
										<div class="line"></div>
										<div class="product-meta">
											<div class="">
												<?php if(!empty($row['tag'])) :?>
													<strong>Tags:</strong>
													<span class="tagged_as">
														<?php 
															$tags = $row['tag'];
															$tag = explode('|', $tags);
														?>
														<?php foreach ($tag as $value):?>
															<a href="">
																<i class="fa fa-caret-right"></i>
																<?php echo $value; ?>
															</a>
														<?php endforeach; ?>
													</span>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
								<div class="clear" style="margin-bottom: 25px;"></div>
								<div class="clearfix hidden-xs" style="margin-bottom: 50px;">
									<ul class="nav nav-tabs" role="tablist">
								  		<li class="active"><a href="#desc" role="tab" data-toggle="tab">Mô tả</a></li>
								  		<li><a href="#comments" role="tab" data-toggle="tab">Bình luận</a></li>
								  		<li><a href="#similar-products" role="tab" data-toggle="tab">Sản phẩm cùng loại</a></li>
									</ul>
									<div class="tab-content">
									  	<div class="tab-pane active" id="desc">
									  		<br />
									  		<?php echo $row['detail']; ?>
									  	</div>
									  	<div class="tab-pane" id="comments">
									  		<br />
									  		Hiện chưa có bình luận nào !
									  	</div>
										<div class="tab-pane" id="similar-products">
									  		<br />
									  		<div class="row similar-products index-main-product">
								  			<?php  
							  				$sm_products = $this->Mproduct->product_similar($row['catid'], $row['id'], 8);
							  				foreach($sm_products as $item) :?>
									  			<div class="col-sm-3 col-xs-6">
													<div class="info-product">
														<div class="header-info">
															<a href="<?php echo $item['alias'] ?>" title="<?php echo $item['name'] ?>">
																<?php echo $item['name'] ?>
															</a>
														</div>
														<?php
															$mang = explode('#', $item['img']);
															$img = $mang[0];
														?>
														<div class="image-product">
															<a href="<?php echo $item['alias'] ?>">
																<img src="<?php echo base_url() ?>public/images/products/<?php echo $img; ?>">
															</a>
															<?php if($item['sale'] > 0) :?>
																<div class="sale-flash">
																	-<?php echo $item['sale'] ?>%
																</div>
															<?php endif; ?>
															<div class="product-overlay hidden-xs">
																<a href='gio-hang' onclick="ham(<?php echo $item['id']; ?>)" class='product_quick_add' data-toggle="tooltip" data-placement="top" title="Thêm vào giỏ">
																	<i class="fa fa-shopping-cart"></i>
																</a>
																<a href="<?php echo $item['alias'] ?>" class='item-quick-view fancybox-fast-view 'data-toggle="tooltip" data-placement="top" title="Xem nhanh">
																	<i class="fa fa-eye">
																	</i>
																</a>
															</div>
														</div>
														<div class="price-product">
															<?php if($item['sale'] > 0) :?>
																<?php echo(number_format($item['price_sale'])); ?>₫
																<br />
																<div>
																	<?php echo(number_format($item['price']));?>₫
																</div>
															<?php else: ?>
																<?php echo(number_format($item['price']));?>₫
															<?php endif; ?>
														</div>
														<div style="clear: both;" class="clearfix"></div>
														<div class="btn-product hidden-sm hidden-md hidden-lg">
															<a href="gio-hang" class="btn btn-primary" role="button" style="width: 100%;" onclick="ham(<?php echo $item['id']; ?>)"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a>
														</div>
													</div>
												</div>
									  		<?php endforeach; ?>
									  		</div>

									  		<?php if(count($sm_products) > 0) :?>
									  			<span style="font-size: 0.85em; float: right;">Nhấn vào <a href="<?php $namecat = $this->Mcategory->category_link($item['catid']); echo $namecat; ?>">đây</a> để xem toàn bộ !</span>
									  		<?php endif; ?>
									  	</div>
									</div>
									<script>
									  	$(function () {
									   		$('#myTab a:last').tab('show')
									  	})
									</script>
								</div>
								<div class="panel-group hidden-lg hidden-sm hidden-md pd_description" id="accordion">
									<div class="panel panel-default ">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title" style="text-decoration: none;">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="width: 100%; display: block;">
													<i class="fa fa-align-justify"></i><span class="" style=" margin-left: 5px;">Mô tả</span>
												</a>
											</h4>
										</div>
										<div id="collapseOne" class="pd_description_content panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<h4 style="text-align: center;">
													<strong>
														<span style="color:#008AC2;"><?php echo $row['name']; ?></span>
													</strong>
												</h4>
												<div>
													<?php echo $row['introtext']; ?>
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default ">
										<div class="panel-heading" role="tab" id="headingTwo">
											<h4 class="panel-title" style="text-decoration: none;">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="width: 100%; display: block;">
													<i class="fa fa-comments"></i><span class="" style=" margin-left: 5px;">Bình luận</span>
												</a>
											</h4>
										</div>
										<div id="collapseTwo" class="pd_description_content panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
											<div class="panel-body">
												<h4 style="text-align: center;">
													<strong>
														<span style="color:#008AC2;"></span>
													</strong>
												</h4>
												<div>
													Hiện chưa có bình luận nào.
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default ">
										<div class="panel-heading" role="tab" id="headingThree">
											<h4 class="panel-title" style="text-decoration: none;">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="width: 100%; display: block;">
													<i class="fa fa-bars"></i><span class="" style=" margin-left: 5px;">Sản phẩm cùng loại</span>
												</a>
											</h4>
										</div>
										<div id="collapseThree" class="pd_description_content panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
											<div class="panel-body">
												<h4 style="text-align: center;">
													<strong>
														<span style="color:#008AC2;"></span>
													</strong>
												</h4>
												<div class="row similar-products index-main-product">
								  			<?php  
							  				$sm_products = $this->Mproduct->product_similar($row['catid'], $row['id'], 8);
							  				foreach($sm_products as $item) :?>
									  			<div class="col-sm-3 col-xs-6">
													<div class="info-product">
														<div class="header-info">
															<a href="<?php echo $item['alias'] ?>" title="<?php echo $item['name'] ?>">
																<?php echo $item['name'] ?>
															</a>
														</div>
														<?php
															$mang = explode('#', $item['img']);
															$img = $mang[0];
														?>
														<div class="image-product">
															<a href="<?php echo $item['alias'] ?>">
																<img src="<?php echo base_url() ?>public/images/products/<?php echo $img; ?>">
															</a>
															<?php if($item['sale'] > 0) :?>
																<div class="sale-flash">
																	-<?php echo $item['sale'] ?>%
																</div>
															<?php endif; ?>
															<div class="product-overlay hidden-xs">
																<a href='gio-hang' onclick="ham(<?php echo $item['id']; ?>)" class='product_quick_add' data-toggle="tooltip" data-placement="top" title="Thêm vào giỏ">
																	<i class="fa fa-shopping-cart"></i>
																</a>
																<a href="<?php echo $item['alias'] ?>" class='item-quick-view fancybox-fast-view 'data-toggle="tooltip" data-placement="top" title="Xem nhanh">
																	<i class="fa fa-eye">
																	</i>
																</a>
															</div>
														</div>
														<div class="price-product">
															<?php if($item['sale'] > 0) :?>
																<?php echo(number_format($item['price_sale'])); ?>₫
																<br />
																<div>
																	<?php echo(number_format($item['price']));?>₫
																</div>
															<?php else: ?>
																<?php echo(number_format($item['price']));?>₫
															<?php endif; ?>
														</div>
														<div style="clear: both;" class="clearfix"></div>
														<div class="btn-product hidden-sm hidden-md hidden-lg">
															<a href="gio-hang" class="btn btn-primary" role="button" style="width: 100%;" onclick="ham(<?php echo $item['id']; ?>)"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a>
														</div>
													</div>
												</div>
									  		<?php endforeach; ?>
									  		</div>
									  		<span style="font-size: 0.85em; float: right;">Nhấn vào <a href="<?php $namecat = $this->Mcategory->category_link($item['catid']); echo $namecat; ?>">đây</a> để xem toàn bộ !</span>
											</div>
										</div>
									</div>
								</div>
						<?php endif; ?>	
					</form>
				</div>
			</div>	
		</div>
	</div>
</div>

<script>
	var quantity = parseInt($('#ProductDetailsForm .product-quantity input.qty').val());

	$('#ProductDetailsForm .minus').click(function() {
		if (quantity > 0) {
			if (quantity == 1) {
				$('#addtocart').attr('disabled','disabled');
			}
			quantity -= 1;
		}
		else {
			quantity = 0;
		}
		$('#ProductDetailsForm .product-quantity input.qty').val(quantity);
	});
	$('#ProductDetailsForm  .plus').click(function() {
		$('#addtocart').removeAttr('disabled');
		if (quantity < 100) {
			quantity += 1;
		}
		else {
			quantity = 100;
		}
		$('#ProductDetailsForm .product-quantity input.qty').val(quantity)
	});
</script>