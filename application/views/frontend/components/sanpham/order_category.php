<?php
	$html_list='<div class = "sort-by">';
	foreach ($list as $val) 
	{	$html_list.='<div class = "product_category">';
			$html_list.='<div class="col-sm-3 col-xs-6">';
				$html_list.='<div class="info-product">';
					$html_list.='<div class="header-info">';
						$html_list.="<a href='".$val['alias']." ' title=' ".$val['name']." '>";
							$html_list.="".$val['name']."";
						$html_list.='</a>';
					$html_list.='</div>';

					$mang = explode('#', $val['img']);
					$img = $mang[0];

					$html_list.='<div class="image-product">';
						$html_list.="<a href='".$val['alias']."'>";
							$html_list.="<img src='public/images/products/".$img."'>";
						$html_list.='</a>';
						if($val['sale'] > 0)
						{
							$html_list.='<div class="sale-flash">';
								$html_list.="-".$val['sale']."%";
							$html_list.='</div>';
						}
						$html_list.='<div class="product-overlay hidden-xs">';
								$html_list.="<a href='gio-hang' onclick='ham(".$val['id'].");' class='product_quick_add' data-toggle='tooltip' data-original-title='Thêm vào giỏ'>";
									$html_list.='<i class="fa fa-shopping-cart"></i>';
								$html_list.='</a>';
								$html_list.="<a href='".$val['alias']."' class='item-quick-view fancybox-fast-view ' data-toggle='tooltip' data-original-title='Xem nhanh'>";
									$html_list.='<i class="fa fa-eye"></i>';
								$html_list.='</a>';
							$html_list.='</div>';
					$html_list.='</div>';
					$html_list.='<div class="price-product">';
						if($val['sale'] > 0)
						{
							$html_list.="".number_format($val['price_sale'])."₫";
							$html_list.='<br .>';
						}
						else
						{
							$html_list.="".number_format($val['price'])."₫";
							$html_list.='<br .>';
						}
						if($val['sale'] > 0)
						{
							$html_list.='<div>';
							$html_list.="".number_format($val['price'])."₫";
							$html_list.='</div>';
						}
					$html_list.='</div>';
				$html_list.='</div>';
			$html_list.='</div>';
		$html_list.='</div>';
	}
	$html_list.='</div>';
	$html_list.='<div class = "row text-center">';
		$html_list.='<ul class ="pagination">';
			$html_list.=$strphantrang;
		$html_list.='</ul>';
	$html_list.='</div>';
echo $html_list;
?>