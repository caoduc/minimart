<?php echo form_open('dang-nhap'); ?>
<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row content-ct">
	<div class="container">
		<div class="accordion accordion-lg divcenter nobottommargin clearfix" style="max-width: 550px;">
				<!-- login -->
			<div id="login">
				<div class="acctitle acctitlec"><i class="acc-closed fa fa-lock"></i>Đăng nhập</div>
				<div class="acc_content clearfix" style="display: block;">
					<form accept-charset="UTF-8" action="" id="customer_login" method="post">
						
						<input name="FormType" type="hidden" value="customer_login">
						<input name="utf8" type="hidden" value="true">
						<div class="col_full">
							<label for="login-form-username">Tên đăng nhập:<span class="require_symbol">* </span></label>
							<input type="text" id="login-form-username" name="username" value="" class="form-control">
							<div class="error" id="password_error"><?php echo form_error('username')?></div>
						</div>

						<div class="col_full">
							<label for="login-form-password">Mật khẩu:<span class="require_symbol">* </span></label>
							<input type="password" id="login-form-password" name="password" value="" class="form-control">
							<div class="error" id="password_error"><?php echo form_error('password')?></div>
						</div>
						
	                    <?php  if(isset($msg)):?>
		                        <div class="row">
	                                <?php echo $msg; ?>
		                        </div>
		                    <?php  endif;?>
						<div class="col_full nobottommargin">
							<button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" type="submit" value="login">Đăng nhập</button>
							<ul>
								<li><a href="#" class="fright">Quên mật khẩu?</a></li>
								<li><a href="<?php echo base_url() ?>dang-ky" class="fright">Người dùng mới? Đăng ký tài khoản</a></li>
							</ul>
						</div>
					</form>
				</div>
			</div>
		</div>   
	</div>
</div>