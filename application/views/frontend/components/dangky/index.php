<?php echo form_open('dang-ky'); ?>
<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row content-ct">
	<div class="container">
		<div class="accordion accordion-lg divcenter nobottommargin clearfix" style="max-width: 550px;">
			<div id="register">
				<div class="acctitle acctitlec"><i class="acc-closed fa fa-user"></i><i class="acc-open fa fa-user-plus"></i>Đăng ký tài khoản</div>
				<div class="acc_content clearfix" style="display: block;">
					<form accept-charset="UTF-8" action="" id="customer_register" method="post">
						<!---->
						<input name="FormType" type="hidden" value="customer_register">
						<input name="utf8" type="hidden" value="true"> 
						<div class="col_full">
							<label for="first_name">Tên đăng nhập:<span class="require_symbol">*</span></label>
							<input type="text" id="first_name" name="username" value="" class="form-control" placeholder="Tên đăng nhập">
							<div class="error" id="username_error"><?php echo form_error('username')?></div>
						</div> 
						<div class="col_full">
							<label for="register-form-password">Mật khẩu:<span class="require_symbol">*</span></label>
							<input type="password" id="register-form-password" name="password" placeholder="Mật khẩu" class="form-control">
							<div class="error" id="password_error"><?php echo form_error('password')?></div>
						</div>

						<div class="col_full">
							<label for="register-form-repassword">Nhập lại mật khẩu:<span class="require_symbol">* </span></label>
							<input type="password" id="register-form-repassword" name="re_password" value="" class="form-control" placeholder="Nhập lại mật khẩu">
							<div class="error" id="re_password_error"><?php echo form_error('re_password')?></div>
						</div>
						<div class="col_full">
							<label for="first_name">Họ tên:<span class="require_symbol">*</span></label>
							<input type="text" id="first_name" name="name" placeholder="Họ tên" class="form-control">
							<div class="error" id="name_error"><?php echo form_error('name')?></div>
						</div>              
						<div class="col_full">
							<label for="register-form-email">Email:<span class="require_symbol">*</span></label>
							<input type="text" id="register-form-email" name="email" value="" class="form-control" placeholder="Nhập email">
							<div class="error" id="email_error"><?php echo form_error('email')?></div>
						</div>
						<div class="col_full">
							<label for="first_name">Số điện thoại:<span class="require_symbol">*</span></label>
							<input type="text" id="first_name" name="phone" placeholder="Số điện thoại" class="form-control">
							<div class="error" id="name_error"><?php echo form_error('phone')?></div>
						</div>
						<div class="col_full nobottommargin">
							<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" type="submit">Đăng ký</button>
							<ul>
								<li class="right">Nếu đã có tài khoản, nhấp vào <a id = "login" href="<?php echo base_url() ?>dang-nhap">đây</a>để đăng nhập !</li>
							</ul>
						</div>
					</form>
				</div>
			</div>
		</div>   
	</div>
</div>