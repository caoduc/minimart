<div class="row wraper">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row content-ct">
	<div class="container">
		<div class = "col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<div class="entry-title">
				<h3><?php echo $row['title']; ?></h3>
			</div>
			<ul class="entry-meta clearfix">
				<li><i class="fa fa-calendar"></i><?php echo $row['created']; ?></li>
				<li><i class="fa fa-user"></i><?php echo $row['created_by']; ?></li>
			</ul>
			<div class="entry-content">
				<img src="public/images/posts/<?php echo $row['img']; ?>">
				<p><?php echo $row['fulltext']; ?></p>
			</div>
		</div>
		<div class="col-md-3 col-lg-3 hidden-xs hidden-sm">
			<div class="post_content">
				<?php 
					$this->load->view('frontend/modules/panel_right');
					$this->load->view('frontend/modules/news');
				?>
			</div>
		</div>
	</div>
</div>