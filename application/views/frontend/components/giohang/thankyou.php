<?php  
	if($this->session->userdata('user'))
	{
		$val = $this->session->userdata('user');
		$detail_cus = $this->Mcustomer->customer_detail_username($val);
		$id = $this->Mcustomer->customer_id($val);
		$list = $this->Morder->order_detail_customerid($id);
	}
?>

<div class="content-wrap">
	<div class="container">
		<div class="row thankyou-infos" style="margin-top: 50px; border-top: 1px solid #dcdcdc;">
            <div class="col-md-7 col-sm-12 thankyou-message" style="margin-bottom: 10px;">
                <div class="thankyou-message-icon unprint" style="width: 80px; float: left; margin-top: 10px;">
                    <div class="icon icon--order-success svg">
                        <svg xmlns="http://www.w3.org/2000/svg" width="72px" height="72px">
                            <g fill="none" stroke="#8EC343" stroke-width="2">
                                <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="thankyou-message-text">
                    <h3>Cảm ơn bạn đã đặt hàng</h3>
                    <p><?php  if($this->session->flashdata('success')){echo $this->session->flashdata('success');}?></p>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 order-info" define="{order_expand: false}">
                <div class="order-summary order-summary--custom-background-color ">
                    <div class="order-summary-header summary-header--thin summary-header--border">
                        <h3>
                            <label class="control-label">Đơn hàng</label>
                            	<?php foreach($list as $r){echo '#'.$r['id'];}?>
                            <label class="control-label unprint">(1)</label>
                        </h3>
                    </div>
                    <div class="order-items mobile--is-collapsed" bind-class="{'mobile--is-collapsed': !order_expand}">
                        <div class="summary-body summary-section summary-product">
                            <div class="summary-product-list">
                                <ul class="product-list">
                                <?php  
                                    if($this->session->userdata('cart')):
	                                    $val = $this->session->userdata('cart');?>
	                                    <?php foreach ($val as $key => $value) : 
	                                        $row = $this->Mproduct->product_detail_id($key);?>
		                                    <li class="product product-has-image clearfix">
		                                        <div class="product-thumbnail pull-left">
		                                            <div class="product-thumbnail__wrapper">
		                                                <img src="public/images/products/<?php echo $row['img']; ?>" alt="<?php echo $row['name']; ?>" class="product-thumbnail__image">
		                                            </div>
		                                            <span class="product-thumbnail__quantity unprint" aria-hidden="true"><?php echo $value; ?></span>
		                                        </div>
		                                        <div class="product-info pull-left">
		                                            <span class="product-info-name">
		                                                <strong><?php echo $row['name']; ?></strong>
		                                                <label class="print">x<?php echo $value; ?></label>
		                                            </span>
		                                        </div>
		                                        <strong class="product-price pull-right">
		                                            <?php 
		                                            	if($row['price_sale'] > 0) 
		                                            	{
		                                            		echo (number_format($row['price_sale'])).'₫';
		                                            	}
		                                            	else
		                                            	{
		                                            		echo (number_format($row['price'])).'₫';
		                                            	}
		                                            ?> 
		                                        </strong>
		                                    </li> 
		                                <?php endforeach; ?> 
		                            <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php if($this->session->userdata('cart')){
                        $val = $this->session->userdata('cart');
                        $total = 0;
                        foreach($val as $key => $value)
                        {
                            $row = $this->Mproduct->product_detail_id($key);
                                if($row['price_sale'] > 0)
                                {
                                    $sum = $row['price_sale'] * $value;
                                }
                                else
                                {
                                    $sum = $row['price'] * $value;
                                }
                            $total += $sum;
                        }
                    }?>
                    <div class="summary-section  border-top-none--mobile ">
                        <div class="total-line total-line-subtotal clearfix">
                            <span class="total-line-name pull-left">
                                Tạm tính
                            </span>
                            <span class="total-line-subprice pull-right">
                                <?php echo (number_format($total)).'₫'; ?> 
                            </span>
                        </div>
                        <div class="total-line total-line-shipping clearfix">
                            <span class="total-line-name pull-left">
                                Phí vận chuyển
                            </span>
                            <span class="pull-right"><?php $ship = 40000; echo (number_format($ship)).'₫'; ?> </span>
                        </div>   
                    </div>
                    <div class="summary-section">
                        <div class="total-line total-line-total clearfix">
                            <span class="total-line-name total-line-name--bold pull-left">
                                Tổng cộng
                            </span>
                            <span class="total-line-price pull-right">
                                <?php echo (number_format($total + $ship)).'₫'; ?> 
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-sm-12 customer-info">
                <div class="shipping-info">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="order-summary order-summary--white no-border no-padding-top">
                                <div class="summary-header">
                                    <h3>
                                        <label class="control-label">Thông tin nhận hàng</label>
                                    </h3>
                                </div>
                                <div class="summary-section no-border no-padding-top">
                                    <p class="address-name">
                                        <?php echo $detail_cus['fullname']; ?>
                                    </p>
                                    <p class="address-address">
                                        <?php echo $detail_cus['address']; ?>
                                    </p>
                                    <p class="address-district">
                                        <?php foreach($list as $r){echo $r['district'];}?>
                                    </p>
                                    <p class="address-province">
                                        <?php foreach($list as $r){echo $r['province'];}?>
                                    </p>
                                    <p class="address-phone">
                                        <?php echo $detail_cus['phone']; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="order-summary order-summary--white no-border">
                                <div class="summary-header">
                                    <h3>
                                        <label class="control-label">Thông tin thanh toán</label>
                                    </h3>
                                </div>
                                <div class="summary-section no-border no-padding-top">
                                    <p class="address-name">
                                        <?php echo $detail_cus['fullname']; ?>
                                    </p>
                                    <p class="address-address">
                                        <?php echo $detail_cus['address']; ?>
                                    </p>
                                    <p class="address-district">
                                        <?php foreach($list as $r){echo $r['district'];}?>
                                    </p>
                                    <p class="address-province">
                                        <?php foreach($list as $r){echo $r['province'];}?>
                                    </p>
                                    <p class="address-phone">
                                        <?php echo $detail_cus['phone']; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="order-summary order-summary--white no-border">
                                <div class="summary-header">
                                    <h3>
                                        <label class="control-label">Hình thức thanh toán</label>
                                    </h3>
                                </div>
                                <div class="summary-section no-border no-padding-top">
                                    <span>Thanh toán khi giao hàng (COD)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="order-summary order-summary--white no-border">
                                <div class="summary-header">
                                    <h3>
                                        <label class="control-label">Hình thức vận chuyển</label>
                                    </h3>
                                </div>
                                <div class="summary-section no-border no-padding-top">  
                                    <span>Giao hàng tận nơi - 40.000₫</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="order-success unprint" style="margin-top: 20px; float: right; margin-bottom: 50px;">
                    <a href="san-pham" class="btn btn-primary" style="height: 50px; line-height: 40px; font-size: 1em;">
                        Tiếp tục mua hàng
                    </a>
                    <a onclick="window.print()" class="nounderline print-link btn btn-primary" style="height: 50px; line-height: 40px; font-size: 1em;"" href="javascript:void(0)">
                        In
                    </a>
                </div>
            </div>
        </div>
	</div>
</div>
