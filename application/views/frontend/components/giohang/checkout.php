<?php echo form_open('checkout'); ?>
<?php  
    if(!$this->session->userdata('cart'))
    {
        redirect('gio-hang');
    }
?>
<div class="content-wrapper" style="min-height: 454px;">
    <form action="<?php echo base_url() ?>admin/sliders/insert.html" enctype="multipart/form-data" method="post" accept-charset="utf-8">
        <section class="content">
          <!-- Info boxes -->
            <div context="checkout" class="container checkout">
            <div class="main">
                <div class="wrap clearfix" style="margin-top: 50px;">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 order-info">
                            <div class="order-summary order-summary--custom-background-color ">
                                <div class="order-summary-header summary-header--thin summary-header--border">
                                    <h3>
                                        <label class="control-label">Đơn hàng</label>
                                        <label class="control-label">
                                            <?php  
                                                if($this->session->userdata('cart'))
                                                {
                                                    $val = $this->session->userdata('cart');
                                                    $sl = count($val);
                                                    echo '('.$sl.')';
                                                }
                                                else
                                                {
                                                    echo '(0)';
                                                }
                                            ?>
                                        </label>
                                    </h3>
                                </div>
                                <div class="order-items mobile--is-collapsed">
                                    <div class="summary-body summary-section summary-product">
                                        <div class="summary-product-list">
                                            <ul class="product-list">
                                                <?php  
                                                    if($this->session->userdata('cart')):
                                                    $val = $this->session->userdata('cart');?>
                                                    <?php foreach ($val as $key => $value) : 
                                                        $row = $this->Mproduct->product_detail_id($key);?>
                                                        <li class="product product-has-image clearfix">
                                                            <div class="product-thumbnail pull-left">
                                                                <div class="product-thumbnail__wrapper">
                                                                    <img src="public/images/products/<?php echo $row['img'] ?>" alt="<?php echo $row['name'] ?>" class="product-thumbnail__image">
                                                                </div>
                                                                <span class="product-thumbnail__quantity" aria-hidden="true"><?php echo $value; ?></span>
                                                            </div>
                                                            <div class="product-info pull-left">
                                                                <span class="product-info-name">
                                                                    <strong><?php echo $row['name'] ?></strong>
                                                                </span>  
                                                            </div>
                                                        </li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="summary-section border-top-none--mobile">
                                <?php if($this->session->userdata('cart')):
                                    $val = $this->session->userdata('cart');
                                    $total = 0;?>
                                    <div class="total-line total-line-subtotal clearfix">
                                        <span class="total-line-name pull-left">
                                            Tạm tính
                                        </span>
                                        <span class="total-line-subprice pull-right">
                                            <?php foreach ($val as $key => $value) : 
                                                $row = $this->Mproduct->product_detail_id($key);?>
                                                <?php
                                                    if($row['price_sale'] > 0)
                                                    {
                                                        $sum = $row['price_sale'] * $value;
                                                    }
                                                    else
                                                    {
                                                        $sum = $row['price'] * $value;
                                                    }
                                                ?> 
                                                <?php $total += $sum; ?>
                                            <?php endforeach; ?>
                                            <?php echo (number_format($total)).'₫'; ?> 
                                        </span>
                                    </div>
                                    <div class="total-line total-line-shipping clearfix">
                                        <span class="total-line-name pull-left">
                                            Phí vận chuyển
                                        </span>
                                        <span class="total-line-shipping pull-right"><?php $ship = 40000; echo (number_format($ship)).'₫';?></span>
                                    </div>
                                    <div class="total-line total-line-total clearfix">
                                        <span class="total-line-name pull-left">
                                            Tổng cộng
                                        </span>
                                        <span class="total-line-price pull-right" id ='total'><?php $s = $total + $ship; echo (number_format($s)).'₫'; ?></span>
                                    </div>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="breadcrumb" style="background: #fff;padding:0; margin-top: 20px; height: 50px;">
                                <button name="dathang" type="submit" class="btn btn-primary btn-sm" style="width:100%; height: auto; float: left; font-size: 1em; text-transform: uppercase;">Đặt hàng</button>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 customer-info" style="padding-left: 0px;">
                            <div class="form-group m0">
                                <h3>
                                    <label class="control-label">Thông tin mua hàng</label>
                                </h3>
                            </div>
                            <?php if(!$this->session->userdata('user')): ?>
                                <div class="form-group">
                                    <a href="dang-ky">Đăng ký tài khoản mua hàng</a>
                                    <span style="padding: 0 5px;">/</span>
                                    <a href="dang-nhap">Đăng nhập </a>
                                </div>
                                <div class="form-group" >
                                    <input data-error="Vui lòng nhập email đúng định dạng" name="Email" value="" type="email" class="form-control" placeholder="Email" required>
                                    <div class="error"><?php echo form_error('Email')?></div>
                                </div>
                                <div class="billing">
                                    <div class="form-group">
                                        <a class="underline-none open">
                                            Thông tin thanh toán và nhận hàng
                                        </a>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <input data-error="Vui lòng nhập họ tên" name="Name" class="form-control" placeholder="Họ và tên" value="" required>
                                            <div class="error"><?php echo form_error('Name')?></div>
                                        </div>
                                        <div class="form-group">
                                            <input name="Phone" class="form-control" placeholder="Số điện thoại" data-error="Vui lòng nhập số điện thoại">
                                        </div>
                                        <div class="form-group">
                                            <input name="Address" class="form-control" placeholder="Địa chỉ">
                                        </div>
                                        <div class="form-group">
                                            <div class="next-select__wrapper">
                                                <select name="city" id="province" onchange="ham()" class="form-control next-select">
                                                    <option value="">--- Chọn tỉnh thành ---</option>
                                                    <?php $list = $this->Mprovince->province_all();
                                                    foreach($list as $row):?>
                                                        <option value="<?php echo $row['provinceid']; ?>"><?php echo $row['type'].' '; ?><?php echo $row['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="next-icon next-icon--size-12">
                                                    <img src="public/images/templates/angle-down.png" class="img-responsive">
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="next-select__wrapper">
                                                <select name="DistrictId" id="district" class="form-control next-select">
                                                    <option value="">--- Chọn quận huyện ---</option>
                                                </select>
                                                <span class="next-icon next-icon--size-12">
                                                    <img src="public/images/templates/angle-down.png" class="img-responsive">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="form-group">
                                    <input data-error="Vui lòng nhập email đúng định dạng" required name="Email" value="<?php echo $customer['email'] ?>" type="email" class="form-control" placeholder="Email" readonly>
                                </div>
                                <div class="billing">
                                    <div class="form-group">
                                        <a class="underline-none open">
                                            Thông tin thanh toán và nhận hàng
                                        </a>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <input data-error="Vui lòng nhập họ tên" required="" name="Name" class="form-control" placeholder="Họ và tên" value="<?php echo $customer['fullname']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input name="Phone" class="form-control" placeholder="Số điện thoại" data-error="Vui lòng nhập số điện thoại" value="<?php echo $customer['phone']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <input name="Address" class="form-control" placeholder="Địa chỉ" value="<?php echo $customer['address']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <div class="next-select__wrapper">
                                                <select name="city" id="province" onchange="ham()" class="form-control next-select">
                                                    <option value="">--- Chọn tỉnh thành ---</option>
                                                    <?php $list = $this->Mprovince->province_all();
                                                    foreach($list as $row):?>
                                                        <option value="<?php echo $row['provinceid']; ?>"><?php echo $row['type'].' '; ?><?php echo $row['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="next-icon next-icon--size-12">
                                                    <img src="public/images/templates/angle-down.png" class="img-responsive">
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="next-select__wrapper">
                                                <select name="DistrictId" id="district" class="form-control next-select">
                                                    <option value="">--- Chọn quận huyện ---</option>
                                                </select>
                                                <span class="next-icon next-icon--size-12">
                                                    <img src="public/images/templates/angle-down.png" class="img-responsive">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <textarea name="note" value="" class="form-control" placeholder="Ghi chú"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" style="padding-left: 0px;">
                            <div class="shipping-method">
                                <div class="form-group">
                                    <h3>
                                        <label class="control-label">Vận chuyển</label>
                                    </h3>
                                    <div class="next-select__wrapper">
                                        <select name="ShippingMethod" bind="shippingMethod" class="form-control next-select">
                                            <option value="1">Giao hàng tận nơi - 40.000₫</option>
                                        </select>
                                        <span class="next-icon next-icon--size-12">
                                            <img src="public/images/templates/angle-down.png" class="img-responsive">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section><!-- /.content -->
    </form>         
</div>
<script>
    function ham()
    {
        var provinceid=$("#province").val();
        var strurl="<?php echo base_url();?>"+'/giohang/district';
        jQuery.ajax({
            url: strurl,
            type: 'POST',
            dataType: 'json',
            data: {'provinceid': provinceid},
            success: function(data) {
                $('#district').html(data);
            }
        });
    }

</script>