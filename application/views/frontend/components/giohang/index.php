<div class="row wraper">
	<div class="container">
		<div class="col-md-3 col-lg-3">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
			?>
		</div>
	</div>
</div>
<div class="row content-ct">
	<div class="container">
		<?php if($this->session->userdata('cart')):
			/*if($this->session->userdata('remove_id'))
			{
				$id = $this->session->userdata('remove_id');
				print_r($id);
			}
			else
			{
				echo 'Lỗi';
			}*/
			$val = $this->session->userdata('cart');?>
			<form action="/cart" method="post" id="cartformpage">
				<div class="cart">
					<div class="thead row hidden-xs text-center">
							<div class="col-sm-1 td cart-product-thumbnail">Sản phầm</div>
							<div class="col-sm-4 td cart-product-name">Mô tả</div>
							<div class="col-sm-2 td cart-product-price">Đơn giá</div>
							<div class="col-sm-2 td cart-product-quantity">Số lượng</div>
							<div class="col-sm-2 td cart-product-subtotal">Thành tiền</div>
							<div class="col-sm-1 td cart-product-remove">Xoá</div>
					</div>
					<div class="tbody text-center">
						
							<?php foreach ($val as $key => $value) : 
								$row = $this->Mproduct->product_detail_id($key);?>
								<div class="cart_item row">
									<div class="col-sm-1 td cart-product-thumbnail hidden-xs">
										<a href="<?php echo $row['alias'] ?>"><img src="public/images/products/<?php echo $row['img'] ?>" alt="<?php echo $row['name'] ?>"></a>
									</div>
									<div class="col-sm-4 col-xs-9 td cart-product-name">
										<a href="<?php echo $row['alias'] ?>"><?php echo $row['name'] ?></a>
									</div>
									<div class="col-sm-2 col-xs-3 td cart-product-price">
										<span class="amount">
											<?php 
												if($row['price_sale'] > 0) 
												{
													echo (number_format($row['price_sale'])).'₫';
												}
												else
												{
													echo (number_format($row['price'])).'₫';
												}
											?>
										</span>
									</div>
									<div class="col-sm-2 col-xs-3 td cart-product-quantity">
										<div class="quantity clearfix">
											<input name="quantity" class="form-control" type="number" value="<?php echo $value ?>" min="1" max="<?php echo $row['number'] ?>">
										</div>
									</div>
									<div class="col-sm-2 col-xs-3 td cart-product-subtotal">
										<span class="amount">
											<?php 
												if($row['price_sale'] > 0) 
												{
													echo (number_format($row['price_sale'])).'₫';
												}
												else
												{
													echo (number_format($row['price'])).'₫';
												}
											?>
										</span>
									</div>
									
									<div class="col-sm-1 col-xs-3 td cart-product-remove">
										<a href="gio-hang/remove/<?php echo $row['id'] ?>" class="remove" title="Xóa"><i class="fa fa-trash"></i></a>
									</div>
								</div>
							<?php endforeach; ?>
							<?php $total = 0; ?>
							<?php foreach ($val as $key => $value) : 
								$row = $this->Mproduct->product_detail_id($key);?>
								<?php
									if($row['price_sale'] > 0)
									{
										$sum = $row['price_sale'] * $value;
									}
									else
									{
										$sum = $row['price'] * $value;
									}
									$total += $sum;
								?>	
							<?php endforeach; ?>
							<div class="row total_price">
								<div class="col-sm-2 col-xs-6 td col-sm-push-7"><h4 class="nomargin">Tổng tiền: </h4></div>
								<div class="col-md-3 col-xs-6 td col-sm-push-7"><span class="amount color lead"><strong><?php echo (number_format($total)).'₫'; ?></strong></span></div>
							</div>
						
					</div>
				</div>
				<div class="row clearfix btn-submit">
					<div class="col-md-6 col-sm-6 col-xs-12 nopadding col-sm-push-6">
						<button type="submit" name="update" class="button button-3d nomargin fright">Cập nhật</button>
						<button type="button" onclick="window.location.href='checkout'" class="button button-3d notopmargin fright">Đặt hàng</button>
					</div>
				</div>
			</form>
		<?php else: ?>
			<p id = "cart-info">Không có sản phẩm nào trong giỏ hàng !</p>
			<a href="san-pham" class="fa fa-undo undo">Tiếp tục mua hàng</a>
		<?php endif; ?>
	</div>
</div>