<div class="row slide-wrapper">
	<div class="container">
		<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
			<div class="panel-left-header"><!--Start Header-->
				<div class="title-header">
					<a class="title" href="san-pham/">Danh mục sản phẩm</a>
					<a href="">
						<div class="fa fa-angle-down" id = "arrow"></div>
					</a>
				</div>
			</div>
			<?php 
				$this->load->view('frontend/modules/panel_left');
			?>
		</div>
		<div class="col-md-9 col-lg-9 hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/primary_menu');
				$this->load->view('frontend/modules/slider');
			?>
		</div>
	</div>
</div>
<?php  if(isset($msg)):?>
    <div class="row">
        <div class="alert alert-success">
            <?php echo $msg; ?>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
    </div>
<?php  endif;?>
<div class="row content">
	<div class="container">
		<?php
		$category=$this->Mcategory->category_list(0,'all'); 
		foreach ($category as $value):	
			$sub_category=$this->Mcategory->category_list($value['id'],6);
			$catid=$this->Mcategory->category_id($value['link']);
			$listcat=$this->Mcategory->category_listcat($catid);
			$list=$this->Mproduct->product_home_limit($listcat,8);
			if(count($sub_category) && (count($list) > 3)):?>	
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 index_col_group_left">
					<div class="index-title">
						<a href="san-pham/<?php echo $value['link'] ?>"  title="<?php echo $value['name'] ?>"><?php echo $value['name'] ?>
						</a>
					</div>
					<ul class = "hidden-xs hidden-sm">
						<?php foreach ($sub_category as $val) :?>
							<li>
								<a href="san-pham/<?php echo $val['link'] ?>" ' title="<?php echo $val['name'] ?>">
									<?php echo $val['name'] ?>
								</a>
							</li>
						<?php endforeach; ?>
					<a href="san-pham/<?php echo $value['link'] ?>"  title="<?php echo $value['name'] ?>">Xem tất cả
					</ul>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 index-main-product">
					<?php foreach ($list as $item) :?>
						<div class="col-sm-3 col-xs-6">
							<div class="info-product">
								<div class="header-info">
									<a href="<?php echo $item['alias'] ?>" title="<?php echo $item['name'] ?>">
										<?php  
											if(str_word_count($item['name']) > 6)
											{
												echo substr($item['name'], 0, 27).'...';
											}
											else
											{
												echo $item['name'];
											}
										?>
									</a>
								</div>
								<?php
									$mang = explode('#', $item['img']);
									$img = $mang[0];
								?>
								<div class="image-product">
									<a href="<?php echo $item['alias'] ?>">
										<img src="<?php echo base_url() ?>public/images/products/<?php echo $img; ?>">
									</a>
									<?php if($item['sale'] > 0) :?>
										<div class="sale-flash">
											-<?php echo $item['sale'] ?>%
										</div>
									<?php endif; ?>
									<div class="product-overlay hidden-xs">
										<a href='gio-hang' onclick="ham(<?php echo $item['id']; ?>)" class='product_quick_add' data-toggle="tooltip" data-placement="top" title="Thêm vào giỏ">
											<i class="fa fa-shopping-cart"></i>
										</a>
										<a href="<?php echo $item['alias'] ?>" class='item-quick-view fancybox-fast-view 'data-toggle="tooltip" data-placement="top" title="Xem nhanh">
											<i class="fa fa-eye">
											</i>
										</a>
									</div>
								</div>
								<div class="price-product">
									<?php if($item['sale'] > 0) :?>
										<?php echo(number_format($item['price_sale'])); ?>₫
										<br />
										<div>
											<?php echo(number_format($item['price']));?>₫
										</div>
									<?php else: ?>
										<?php echo(number_format($item['price']));?>₫
									<?php endif; ?>
								</div>
								<div style="clear: both;" class="clearfix"></div>
								<div class="btn-product hidden-sm hidden-md hidden-lg">
									<a href="gio-hang" class="btn btn-primary" role="button" style="width: 100%;" onclick="ham(<?php echo $item['id']; ?>)"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;?>
		<?php endforeach;?>
	</div>
</div>
<div class='ads-left hidden-md hidden-xs hidden-sm'>
	<div style='margin:0 0 5px 0; padding:0;width:72px;position:fixed; left:0; top:15%;'>
		<a href='<?php echo base_url() ?>san-pham/dien-tu'><img border='0' src='public/images/banners/banner-1.png' width='72'/></a>
	</div>
</div>
<div class='ads-right hidden-md hidden-xs hidden-sm'>
	<div style='margin:0 0 5px 0; padding:0;width:72px;position:fixed; right:0; top:15%;'>
		<a href='<?php echo base_url() ?>san-pham/dien-lanh'><img border='0' src='public/images/banners/banner-2.png' width='72'/></a>
	</div>
</div>
<script>
	function ham(id)
	{
		var strurl="<?php echo base_url();?>"+'/sanpham/addcart';
		jQuery.ajax({
		  url: strurl,
		  type: 'POST',
		  dataType: 'json',
		  data: {id: id},
		  success: function(data) {
		    //alert(data)
		  }
		});	
	}
</script>