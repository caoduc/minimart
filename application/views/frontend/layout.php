<!DOCTYPE>
<html>
	<head>
	    <base href="<?php echo base_url(); ?>"/>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>
			<?php 
				if(isset($title))
				{ 
					echo $title;
				} 
				else
				{
					echo "Website bán hàng online minimart";
				}
			?>
		</title>
		<meta name="keyworks" content =""/>
	  	<link rel="shortcut icon" href="public/images/templates/favicon.png" />
		<link rel="stylesheet" href="public/css/bootstrap.css">
		<link rel="stylesheet" href="public/css/font-awesome.min.css">
		<link rel="stylesheet" href="public/css/style.css">
		<link rel="stylesheet" href="public/css/style-jc.css">
		<link rel="stylesheet" href="public/css/checkout.css">
		<script>
			function ShowCartContent()
			{
				if(document.getElementById('cart-content').style.display != 'none')
				{
					document.getElementById('cart-content').style.display = 'none';
				}
				else
				{
					document.getElementById('cart-content').style.display = 'block';
				}
			}
		</script>
		<script src="public/js/jquery-2.2.3.min.js"></script>
	</head>
	<body>

		<!-- Top-bar -->
		<div class="row top-bar hidden-xs hidden-sm">
			<?php 
				$this->load->view('frontend/modules/top_bar');
			?>
		</div>
		<!-- End Top-Bar-->

		<!-- Nav-Header -->
		<div class="row nav-header">
			<div class="container"> 
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<div class="logo">
						<a href="#"><img src="public/images/banners/logo.png" alt="Mini Mark"></a>
					</div>
				</div>
				<div class="col-md-5 hidden-xs hidden-sm">
					<div class="form-search">
						<form action="search" method= "get">
							<input type="text" placeholder="Tìm kiếm..." name="search" id='autocomplete' required>
							<button type="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-1 col-lg-1" id = "top_cart">
					<?php $this->load->view('frontend/modules/minicart'); ?>
				</div>
				<div class="col-md-3 hot-line hidden-xs hidden-sm">
					<img src="public/images/banners/top_hotline.png" alt="Hotline">
				</div>
			</div>
		</div>
		<!--NỘI DUNG SẢN PHẨM-->
		<div>
			<?php 
				if(isset($com,$view))
				{
					$this->load->view('frontend/components/'.$com.'/'.$view);
				}
				else
				{
					$this->load->view('frontend/components/Error404/index');
				}
			?>
		</div>
		<div class="row footer">
			<div class="container">
				<?php  
					$this->load->view('frontend/modules/footer');
				?>
			</div>
		</div>
		<div class="row payment">
			<div class="container">
				<?php  
					$this->load->view('frontend/modules/payment');
				?>
			</div>
		</div>
		<div class="row copyright">
			<div class="container">
				<?php  
					$this->load->view('frontend/modules/copyright');
				?>
			</div>
		</div>
		<a class="btn-top" href="javascript:void(0);" title="Top" style="display: inline;"></a>
		
		<script src="public/js/bootstrap.js"></script>
		<script src="public/js/jquery.jcarousel.js"></script>
		<script src="public/js/jcarousel.connected-carousels.js"></script>
		<script src="public/js/gotop.js"></script>
		<!--Start of Zendesk Chat Script-->
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		$.src="https://v2.zopim.com/?4NJ5Mbd90wiOzdLWNl1pDHLWR2FgPCXa";z.t=+new Date;$.
		type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
<!--End of Zendesk Chat Script-->
	</body>
</html>