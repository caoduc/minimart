<?php echo form_open('admin/topic/update/'.$row['id']); ?>
<?php  
	$list=$this->Mtopic->topic_list();
	$option_parentid="";
	foreach ($list as $r) {
		if($r['id']==$row['parentid'])
		{
			$option_parentid.="<option selected value='".$r['id']."'>".$r['name']."</option>";
		}
		else
		{
			$option_parentid.="<option value='".$r['id']."'>".$r['name']."</option>";
		}
	}
	$list=$this->Mtopic->topic_position();
	$option="";
	foreach ($list as $r) {
		if($r['position']==$row['position'])
		{
			$option.="<option selected value='".$r['id']."'>".$r['position']."</option>";
		}
		else
		{
			$option.="<option value='".$r['id']."'>".$r['position']."</option>";
		}
	}
?>
<div class="content-wrapper">
	<form action="<?php echo base_url() ?>admin/topic/update.html" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
		<section class="content-header">
			<h1><i class="glyphicon glyphicon-cd"></i> Cập nhật chủ đề bài viết</h1>
			<div class="breadcrumb">
				<button type = "submit" class="btn btn-primary btn-sm">
					<span class="glyphicon glyphicon-floppy-save"></span>
					Lưu[Cập nhật]
				</button>
				<a class="btn btn-primary btn-sm" href="admin/topic" role="button">
					<span class="glyphicon glyphicon-remove do_nos"></span> Thoát
				</a>
			</div>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box" id="view">
						<div class="box-body">
						<?php //echo validation_errors(); ?>
								<div class="form-group">
									<label>Tên chủ đề <span class = "maudo">(*)</span></label>
									<input type="text" class="form-control" name="name" style="width:100%" placeholder="Tên chủ đề" value="<?php echo $row['name'] ?>">
								</div>
								<div class="form-group">
									<label>Chủ đề cha</label>
									<select name="parentid" class="form-control" style="width:300px">
										<option value = "0">[--Chọn chủ đề--]</option>
											<?php  
												echo $option_parentid;
											?>
									</select>
								</div>
								<div class="form-group">
									<label>Chọn vị trí<span class = "maudo">(*)</span></label>
									<select name="position" class="form-control" style="width:300px">
										<option value = "">[--Chọn vị trí--]</option>
										<?php echo $option;?>
									</select>
									<div class="error" id="password_error"><?php echo form_error('position')?></div>
								</div>
								<div class="form-group">
									<label>Quyền truy cập</label>
									<select name="access" class="form-control" style="width:300px">
										<option value="1" <?php if($row['access'] == 1) {echo 'selected';}?> >Công khai</option>
										<option value="0" <?php if($row['access'] == 0) {echo 'selected';}?>>Hạn chế</option>
									</select>
								</div>
								<div class="form-group">
									<label>Trạng thái</label>
									<select name="status" class="form-control" style="width:300px">
										<option value="1" <?php if($row['status'] == 1) {echo 'selected';}?> >Xuất bản</option>
										<option value="0" <?php if($row['status'] == 0) {echo 'selected';}?>>Chưa xuất bản</option>
									</select>
								</div>
							</div>
						</div>
					</div><!-- /.box -->
				</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
	</form>
<!-- /.content -->
</div><!-- /.content-wrapper -->