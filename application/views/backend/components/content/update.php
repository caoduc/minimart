<?php echo form_open_multipart('admin/content/update/'.$row['id']); ?>
<?php  
	$list=$this->Mtopic->topic_list();
	$option_parentid="";
	foreach ($list as $r) {
		if($r['id']==$row['catid'])
		{
			$option_parentid.="<option selected value='".$r['id']."'>".$r['name']."</option>";
		}
		else
		{
			$option_parentid.="<option value='".$r['id']."'>".$r['name']."</option>";
		}
	}
?>
<div class="content-wrapper">
	<form action="<?php echo base_url() ?>admin/content/update.html" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
		<section class="content-header">
			<h1><i class="glyphicon glyphicon-text-background"></i> Cập nhật bài viết</h1>
			<div class="breadcrumb">
				<button type = "submit" class="btn btn-primary btn-sm">
					<span class="glyphicon glyphicon-floppy-save"></span>
					Lưu[Cập nhật]
				</button>
				<a class="btn btn-primary btn-sm" href="admin/content" role="button">
					<span class="glyphicon glyphicon-remove do_nos"></span> Thoát
				</a>
			</div>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box" id="view">
						<div class="box-body">
							<div class="col-md-9">
							<?php echo validation_errors(); ?>
								<div class="form-group">
									<label>Tên bài viết <span class = "maudo">(*)</span></label>
									<input type="text" class="form-control" name="name" style="width:100%" placeholder="Tên sản phẩm" value="<?php echo $row['title'] ?>">
									<div class="error" id="password_error"><?php echo form_error('name')?></div>
								</div>
								<div class="form-group">
									<label>Chủ đề bài viết</label>
									<select name="catid" class="form-control" style="width:300px">
										<option value = "0">[--Chọn chủ đề--]</option>
											<?php  
												echo $option_parentid;
											?>
									</select>
								</div>
								<div class="form-group">
									<label>Chi tiết bài viết<span class = "maudo">(*)</span></label>
									<textarea name="fulltext" id="fulltext" class="form-control"><?php echo $row['fulltext'] ?></textarea>
      								<script>CKEDITOR.replace('fulltext');</script>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Hình đại diện</label>
									<input class="hinh" name="img" type="file" value="<?php echo $row['img'] ?>">
									<br />
									<img src="public/upload/images/select_image.png" class="img-responsive" alt="Hình đại diện">
								</div>
								<div class="form-group">
									<label>Quyền truy vập</label>
									<select name="access" class="form-control" style="width:250px">
										<option value="1" <?php if($row['access'] == 1) {echo 'selected';}?> >Công khai</option>
										<option value="0" <?php if($row['access'] == 0) {echo 'selected';}?>>Hạn chế</option>
									</select>
								</div>
								<div class="form-group">
									<label>Trạng thái</label>
									<select name="status" class="form-control" style="width:250px">
										<option value="1" <?php if($row['status'] == 1) {echo 'selected';}?> >Xuất bản</option>
										<option value="0" <?php if($row['status'] == 0) {echo 'selected';}?>>Chưa xuất bản</option>
									</select>
								</div>
							</div>
						</div>
					</div><!-- /.box -->
				</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
	</form>
<!-- /.content -->
</div><!-- /.content-wrapper -->