<?php  
	$list=$this->Mcategory->category_list();
	$option_parentid="";
	$option_orders="";
	foreach ($list as $r) {
		$option_parentid.="<option value='".$r['id']."'>".$r['name']."</option>";
		$option_orders.="<option value='".($r['orders']+1)."'>Sau - ".$r['name']."</option>";
	}
?>

<?php echo form_open_multipart('admin/category/insert'); ?>
<div class="content-wrapper">
	<form action="<?php echo base_url() ?>admin/category/insert.html" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
		<section class="content-header">
			<h1><i class="glyphicon glyphicon-cd"></i> Thêm loại sản phẩm mới</h1>
			<div class="breadcrumb">
				<button type = "submit" class="btn btn-primary btn-sm">
					<span class="glyphicon glyphicon-floppy-save"></span>
					Lưu[Thêm]
				</button>
				<a class="btn btn-primary btn-sm" href="admin/category" role="button">
					<span class="glyphicon glyphicon-remove do_nos"></span> Thoát
				</a>
			</div>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box" id="view">
						<div class="box-body">
							<div class="col-md-7">
								<?php //echo validation_errors(); ?>
								<div class="form-group">
									<label>Tên loại sản phẩm <span class = "maudo">(*)</span></label>
									<input type="text" class="form-control" name="name" style="width:100%" placeholder="Tên loại sản phẩm">
									<div class="error" id="password_error"><?php echo form_error('name')?></div>
								</div>
								<div class="form-group">
									<label>Chủ đề cha</label>
									<select name="parentid" class="form-control" style="width:300px">
										<option value = "0">[--Chọn chủ đề cha--]</option>
										<?php echo $option_parentid;?>
									</select>
								</div>
								<div class="form-group">
									<label>Sắp xếp</label>
									<select name="orders" class="form-control" style="width:300px">
										<option value = "">[--Chọn vị trí--]</option>
										<option value="0">Đứng đầu</option>
										<?php  
											echo $option_orders;
										?>
									</select>
								</div>
								<div class="form-group">
									<label>Quyền truy cập</label>
									<select name="access" class="form-control" style="width:300px">
										<option value = "">[--Chọn quyền truy cập--]</option>
										<option value="1">Công khai</option>
										<option value="0">Hạn chế</option>
									</select>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label>Hình đại diện:</label>
									<input class="hinh" name="img" type="file"><br />
									<img src="public/upload/images/select_image.png" class="img-responsive" alt="Hình đại diện">
								</div>
								<div class="form-group">
									<label>Trạng thái</label>
									<select name="status" class="form-control" style="width:300px">
										<option value = "">[--Chọn trạng thái--]</option>
										<option value="1">Xuất bản</option>
										<option value="0">Chưa xuất bản</option>
									</select>
								</div>
							</div>
						</div>
					</div><!-- /.box -->
				</div>
			<!-- /.col -->
		  <!-- /.row -->
		</section>
	</form>
<!-- /.content -->
</div><!-- /.content-wrapper -->