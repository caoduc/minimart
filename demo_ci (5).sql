-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2016 at 03:36 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `db_category`
--

CREATE TABLE IF NOT EXISTS `db_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  `parentid` int(11) NOT NULL,
  `orders` varchar(5) DEFAULT NULL,
  `img` varchar(255) NOT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(255) NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `metakey` varchar(155) NOT NULL,
  `metadesc` varchar(155) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_category`
--

INSERT INTO `db_category` (`id`, `name`, `link`, `level`, `parentid`, `orders`, `img`, `banner`, `created_at`, `created_by`, `updated_at`, `updated_by`, `trash`, `access`, `status`, `metakey`, `metadesc`) VALUES
(1, 'Điện tử', 'dien-tu', 1, 0, '0', 'main_menu_icon_611.png', 'index_col_main_img_2.jpg', '2016-10-03 12:38:40', '2', '2016-10-03 12:38:40', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(2, 'Điện lạnh', 'dien-lanh', 1, 0, '1', 'leftmenu_icon_16.png', 'index_col_main_img_1.jpg', '2016-10-03 12:35:52', '2', '2016-10-03 12:35:52', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(3, 'Nhà bếp', 'nha-bep', 1, 0, '2', 'leftmenu_icon_38.png', 'index_col_main_img_4.jpg', '2016-10-02 22:14:34', '2', '2016-10-02 22:14:34', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(4, 'Gia dụng', 'gia-dung', 1, 0, '3', 'leftmenu_icon_22.png', 'index_col_main_img_3.jpg', '2016-10-02 19:48:47', '2', '2016-10-02 19:48:47', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(5, 'Viễn thông', 'vien-thong', 1, 0, '4', 'default.png', 'index_col_main_img_6.jpg', '2016-10-02 19:49:26', '2', '2016-10-02 19:49:26', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(6, 'Tin học', 'tin-hoc', 1, 0, '5', 'leftmenu_icon_61.png', '', '2016-10-02 19:50:00', '0', '2016-10-02 19:50:00', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(7, 'Kỹ thuật số', 'ky-thuat-so', 1, 0, '6', 'leftmenu_icon_71.png', 'index_col_main_img_5.jpg', '2016-10-02 19:50:48', '0', '2016-10-02 19:50:48', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(8, 'Thiết bị văn phòng', 'thiet-bi-van-phong', 1, 0, '7', 'leftmenu_icon_81.png', '', '2016-10-02 19:51:14', '0', '2016-10-02 19:51:14', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(9, 'Điện cơ', 'dien-co', 1, 0, '9', 'leftmenu_icon_95.png', '', '2016-10-03 13:21:12', '0', '2016-10-03 13:21:12', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(11, 'Sức khỏe làm đẹp', 'suc-khoe-lam-dep', 1, 0, '8', 'leftmenu_icon_1131.png', '', '2016-10-02 20:18:16', '0', '2016-10-02 20:18:16', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(12, 'Bách hóa', 'bach-hoa', 1, 0, '9', 'leftmenu_icon_1253.png', '', '2016-10-03 13:21:00', '0', '2016-10-03 13:21:00', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(13, 'Tivi Sony', 'tivi-sony', 2, 1, '0', 'default.png', '', '2016-10-03 13:19:10', '0', '2016-10-13 16:18:23', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(14, 'Tivi Samsung', 'tivi-samsung', 2, 1, '1', 'default.png', '', '2016-10-03 13:19:36', '0', '2016-10-13 16:18:46', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(15, 'Tivi TCL', 'tivi-tcl', 2, 1, '3', '', '', '2016-10-03 13:20:04', '0', '2016-10-13 16:20:13', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(16, 'Tivi LG', 'tivi-lg', 2, 1, '2', '', '', '2016-10-03 13:21:58', '0', '2016-10-13 16:19:14', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(17, 'Tivi Panasonic', 'tivi-panasonic', 2, 1, '5', '', '', '2016-10-01 10:45:19', '0', '2016-10-13 16:21:52', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(18, 'Tivi Toshiba', 'tivi-toshiba', 2, 1, '4', '', '', '2016-10-01 10:45:26', '0', '2016-10-13 16:21:14', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(19, 'Máy giặt', 'may-giat', 2, 2, '', '', '', '2016-10-01 10:45:31', '0', '2016-10-01 10:45:31', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(20, 'Máy lạnh', 'may-lanh', 2, 2, '', '', '', '2016-10-01 10:45:40', '0', '2016-10-01 10:45:40', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(21, 'Tủ lạnh', 'tu-lanh', 2, 2, '', '', '', '2016-10-01 10:45:53', '0', '2016-10-01 10:45:53', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(22, 'Máy nước nóng', 'may-nuoc-nong', 2, 2, '', '', '', '2016-10-01 10:46:01', '0', '2016-10-01 10:46:01', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(23, 'Máy sấy quần áo', 'may-say-quan-ao', 2, 2, '', '', '', '2016-10-01 10:46:09', '0', '2016-10-01 10:46:09', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(24, 'Máy nước nóng lạnh', 'may-nuoc-nong-lanh', 2, 2, '', '', '', '2016-10-01 10:46:27', '0', '2016-10-01 10:46:27', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(25, 'Tủ đông', 'tu-dong', 2, 2, '', '', '', '2016-10-01 10:46:33', '0', '2016-10-01 10:46:33', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(26, 'Tủ mát - Tủ giữ lạnh', 'tu-mat-tu-giu-lanh', 2, 2, '', '', '', '2016-10-01 10:46:38', '0', '2016-10-01 10:46:38', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(27, 'Máy lọc nước', 'may-loc-nuoc', 2, 2, '', '', '', '2016-10-01 10:46:43', '0', '2016-10-01 10:46:43', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(28, 'Điều hòa', 'dieu-hoa', 2, 2, '0', '', '', '2016-10-01 10:46:49', '0', '2016-12-03 16:47:47', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(29, 'Nồi cơm điện', 'noi-com-dien', 2, 3, '', '', '', '2016-10-01 10:46:59', '0', '2016-10-01 10:46:59', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(30, 'Bếp gas', 'bep-gas', 2, 3, '', '', '', '2016-10-01 10:47:05', '0', '2016-10-01 10:47:05', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(31, 'Nồi đa năng', 'noi-da-nang', 2, 3, '', '', '', '2016-10-01 10:47:10', '0', '2016-10-01 10:47:10', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(32, 'Lò vi sóng', 'lo-vi-song', 2, 3, '', '', '', '2016-10-01 10:47:15', '0', '2016-10-01 10:47:15', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(33, 'Bộ nồi nấu ăn', 'bo-noi-nau-an', 2, 3, '', '', '', '2016-10-01 10:47:20', '0', '2016-10-01 10:47:20', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(34, 'Nồi áp suất', 'noi-ap-suat', 2, 3, '', '', '', '2016-10-01 10:47:25', '0', '2016-10-01 10:47:25', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(35, 'Chảo chống dính', 'chao-chong-dinh', 2, 3, '', '', '', '2016-10-01 10:47:32', '0', '2016-10-01 10:47:32', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(36, 'Quạt điện - Quạt máy', 'quat-dien-quat-may', 2, 4, '', '', '', '2016-10-01 10:47:37', '0', '2016-10-01 10:47:37', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(37, 'Máy hút bụi', 'may-hut-bui', 2, 4, '', '', '', '2016-10-01 10:47:42', '0', '2016-10-01 10:47:42', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(38, 'Bàn ủi - Bàn là', 'ban-ui-ban-la', 2, 4, '', '', '', '2016-10-01 10:48:01', '0', '2016-10-01 10:48:01', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(39, 'Máy hút ẩm', 'may-hut-am', 2, 4, '', '', '', '2016-10-01 10:48:08', '0', '2016-10-01 10:48:08', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(40, 'Quạt phun sương - Quạt hơi nước', 'quat-phun-suong-quat-hoi-nuoc', 2, 4, '', '', '', '2016-10-01 10:48:13', '0', '2016-10-01 10:48:13', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(41, 'Bình lọc nước', 'binh-loc-nuoc', 2, 4, '', '', '', '2016-10-01 10:48:18', '0', '2016-10-01 10:48:18', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(42, 'Máy pha cà phê', 'may-pha-ca-phe', 2, 4, '', '', '', '2016-10-01 10:48:22', '0', '2016-10-01 10:48:22', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(43, 'Điện thoại di động', 'dien-thoai-di-dong', 2, 5, '', '', '', '2016-10-01 10:48:27', '0', '2016-10-01 10:48:27', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(44, 'Máy tính bảng', 'may-tinh-bang', 2, 5, '', '', '', '2016-10-01 10:48:33', '0', '2016-10-01 10:48:33', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(45, 'Máy nghe nhạc MP3/MP4', 'may-nghe-nhac-mp3-mp4', 2, 5, '', '', '', '2016-10-01 10:48:37', '0', '2016-10-01 10:48:37', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(46, 'Sim - Thẻ cào', 'sim-the-cao', 2, 5, '', '', '', '2016-10-01 10:48:43', '0', '2016-10-01 10:48:43', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(47, 'Phụ kiện điện thoại', 'phu-kien-dien-thoai', 2, 5, '', '', '', '2016-10-01 10:48:50', '0', '2016-10-01 10:48:50', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(48, 'Máy tính xách tay', 'may-tinh-xach-tay', 2, 6, '', '', '', '2016-10-01 10:48:55', '0', '2016-10-01 10:48:55', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(49, 'Máy tính để bàn', 'may-tinh-de-ban', 2, 6, '', '', '', '2016-10-01 10:49:01', '0', '2016-10-01 10:49:01', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(50, 'Màn hình vi tính LCD', 'man-hinh-vi-tinh-lcd', 2, 6, '', '', '', '2016-10-01 10:49:11', '0', '2016-10-01 10:49:11', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(51, 'Loa vi tính', 'loa-vi-tinh', 2, 6, '', '', '', '2016-10-01 10:49:19', '0', '2016-10-01 10:49:19', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(52, 'Phụ kiện tin học', 'phu-kien-tin-hoc', 2, 6, '', '', '', '2016-10-01 10:52:27', '0', '2016-10-01 10:52:27', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(53, 'Máy tính để bàn', 'may-tinh-de-ban', 2, 6, '', '', '', '2016-10-01 10:53:02', '0', '2016-10-01 10:53:02', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(54, 'Máy ảnh ống kính rời', 'may-anh-ong-kinh-roi', 2, 7, '', '', '', '2016-10-01 10:53:13', '0', '2016-10-01 10:53:13', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(55, 'Máy ảnh du lịch', 'may-anh-du-lich', 2, 7, '', '', '', '2016-10-01 10:53:19', '0', '2016-10-01 10:53:19', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(56, 'Máy quay phim', 'may-quay-phim', 2, 7, '', '', '', '2016-10-01 10:53:25', '0', '2016-10-01 10:53:25', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(57, 'Camera quan sát', 'camera-quan-sat', 2, 7, '', '', '', '2016-10-01 10:53:31', '0', '2016-10-01 10:53:31', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(58, 'Máy ghi âm', 'may-ghi-am', 2, 7, '', '', '', '2016-10-01 10:53:36', '0', '2016-10-01 10:53:36', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(59, 'Máy chụp ảnh lấy liên', 'may-chup-anh-lay-lien', 2, 7, '', '', '', '2016-10-01 10:53:41', '0', '2016-10-01 10:53:41', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(60, 'Máy nghe nhạc MP3/MP4', 'may-nghe-nhac-mp3-mp4', 2, 7, '', '', '', '2016-10-01 10:53:47', '0', '2016-10-01 10:53:47', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(61, 'Thiết bị giải trí', 'thiet-bi-giai-tri', 2, 7, '', '', '', '2016-10-01 10:53:53', '0', '2016-10-01 10:53:53', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(62, 'Phụ kiện kỹ thuật số', 'phu-kien-ky-thuat-so', 2, 7, '', '', '', '2016-10-01 10:53:58', '0', '2016-10-01 10:53:58', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(63, 'Máy in', 'may-in', 2, 8, '', '', '', '2016-10-01 10:54:05', '0', '2016-10-01 10:54:05', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(64, 'Máy scan', 'may-scan', 2, 8, '', '', '', '2016-10-01 10:54:12', '0', '2016-10-01 10:54:12', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(65, 'Máy chiếu', 'may-chieu', 2, 8, '', '', '', '2016-10-01 10:54:17', '0', '2016-10-01 10:54:17', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(66, 'Máy fax', 'may-fax', 2, 8, '', '', '', '2016-10-01 10:54:30', '0', '2016-10-01 10:54:30', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(67, 'Máy hủy giấy', 'may-huy-giay', 2, 8, '', '', '', '2016-10-01 10:54:39', '0', '2016-10-01 10:54:39', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(68, 'Máy đếm tiền', 'may-dem-tien', 2, 8, '', '', '', '2016-10-01 10:54:44', '0', '2016-10-01 10:54:44', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(69, 'Máy tính bỏ túi', 'may-tinh-bo-tui', 2, 8, '', '', '', '2016-10-01 10:54:50', '0', '2016-10-01 10:54:50', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(70, 'Máy in Bill', 'may-in-bill', 2, 8, '', '', '', '2016-10-01 10:54:59', '0', '2016-10-01 10:54:59', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(71, 'Máy quét Barcode', 'may-quet-barcode', 2, 8, '', '', '', '2016-10-01 10:55:08', '0', '2016-10-01 10:55:08', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(72, 'Phụ kiện văn phòng', 'phu-kien-van-phong', 2, 8, '', '', '', '2016-10-01 10:55:18', '0', '2016-10-01 10:55:18', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(73, 'Gối', 'goi', 2, 12, '', '', '', '2016-10-01 10:55:26', '0', '2016-10-01 10:55:26', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(74, 'Vali', 'vali', 2, 12, '', '', '', '2016-10-01 10:55:34', '0', '2016-10-01 10:55:34', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(75, 'Ba lô', 'ba-lo', 2, 12, '', '', '', '2016-10-01 10:55:49', '0', '2016-10-01 10:55:49', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(76, 'Mền', 'men', 2, 12, '', '', '', '2016-10-01 10:55:54', '0', '2016-10-01 10:55:54', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(77, 'Mỹ phẩm', 'my-pham', 2, 12, '', '', '', '2016-10-01 10:42:51', '0', '2016-10-01 10:42:51', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(78, 'Bộ Drap trải giường và áo gối', 'bo-drap-trai-giuong-va-ao-goi', 2, 12, '', '', '', '2016-10-01 10:55:58', '0', '2016-10-01 10:55:58', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(102, 'Phụ kiện', 'phu-kien', 1, 0, '9', 'leftmenu_icon_1013.png', '', '2016-10-03 13:21:20', '0', '2016-10-03 13:21:20', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(103, 'Dàn âm thanh', 'dan-am-thanh', 2, 1, '6', 'default.png', '', '2016-10-13 20:21:30', 'Ngô Trung Phát', '2016-10-13 20:21:30', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(104, 'Dàn Karaoke', 'dan-karaoke', 2, 1, '7', 'default.png', '', '2016-10-13 20:22:06', 'Ngô Trung Phát', '2016-10-13 20:22:06', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(105, 'Loa Karaoke', 'loa-karaoke', 2, 1, '8', 'default.png', '', '2016-10-13 20:22:43', 'Ngô Trung Phát', '2016-10-13 20:22:43', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(106, 'Đầu Karaoke', 'dau-karaoke', 2, 1, '9', 'default.png', '', '2016-10-13 20:23:46', 'Ngô Trung Phát', '2016-10-13 20:23:46', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(107, 'Amply', 'amply', 2, 1, '10', 'default.png', '', '2016-10-13 20:24:12', 'Ngô Trung Phát', '2016-10-13 20:24:12', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(108, 'Đầu kỹ thuật số', 'dau-ky-thuat-so', 2, 1, '11', 'default.png', '', '2016-10-13 20:25:20', 'Ngô Trung Phát', '2016-10-13 20:25:20', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(109, 'Đầu Smart Box', 'dau-smart-box', 2, 1, '12', 'default.png', '', '2016-10-13 20:26:02', 'Ngô Trung Phát', '2016-10-13 20:26:02', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(110, 'Đàn điện tử', 'dan-dien-tu', 2, 1, '13', 'default.png', '', '2016-10-13 20:26:39', 'Ngô Trung Phát', '2016-10-14 19:05:02', 'Ngô Trung Phát', 1, 1, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `db_config`
--

CREATE TABLE IF NOT EXISTS `db_config` (
  `id` int(11) NOT NULL,
  `mail_smtp` varchar(64) NOT NULL,
  `mail_port` smallint(6) NOT NULL,
  `mail_info` varchar(64) NOT NULL,
  `mail_noreply` varchar(64) NOT NULL,
  `mail_password` varchar(64) NOT NULL,
  `googleid` varchar(50) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `copyright` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `keyword` text NOT NULL,
  `lang` varchar(5) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_config`
--

INSERT INTO `db_config` (`id`, `mail_smtp`, `mail_port`, `mail_info`, `mail_noreply`, `mail_password`, `googleid`, `contact`, `copyright`, `title`, `description`, `keyword`, `lang`, `status`, `access`) VALUES
(1, '', 29, 'hodienloi', 'hodienloi@', '', '', '', '', 'Web site bán hàng Demo', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `db_contact`
--

CREATE TABLE IF NOT EXISTS `db_contact` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_contact`
--

INSERT INTO `db_contact` (`id`, `fullname`, `email`, `phone`, `title`, `content`) VALUES
(1, 'Nguyễn Văn A', 'nguyenvan@yahoo.com', '0987654322', 'HHHH', 'DHkjkdsfds'),
(2, 'Nguyễn Văn B', 'nguyenvan@yahoo.com', '098765', 'hhhh', 'hhhhhh'),
(3, 'Nguyễn Văn B', 'nguyenvan@yahoo.com', '098765', 'hhhh', 'hhhhhh');

-- --------------------------------------------------------

--
-- Table structure for table `db_content`
--

CREATE TABLE IF NOT EXISTS `db_content` (
  `id` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `img` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'Super Admin',
  `modified` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT 'Super Admin',
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `metakey` varchar(155) NOT NULL,
  `metadesc` varchar(155) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_content`
--

INSERT INTO `db_content` (`id`, `catid`, `title`, `alias`, `introtext`, `fulltext`, `img`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`, `metakey`, `metadesc`) VALUES
(1, 2, 'Giới thiệu Mini Mart', 'gioi-thieu', 'Mini Mart là một trang mua sắm online uy tín chuyên cung cấp các sản phẩm về các lĩnh vực như Điện tử, điện lạnh, nhà bếp, ...được vận hành bởi các thương hiệu nổi tiếng. Các dòng sản phẩm trên website Mini Mart không ngừng đa dạng về kiểu dáng, mẫu mã vô cùng đẹp mắt mang chất lượng tốt nhất....', '<p>Mini Mart là một trang mua sắm online uy tín chuyên cung cấp các sản phẩm về các lĩnh vực như Điện tử, điện lạnh, nhà bếp, ...được vận hành bởi các thương hiệu nổi tiếng. Các dòng sản phẩm trên website Mini Mart không ngừng đa dạng về kiểu dáng, mẫu mã vô cùng đẹp mắt mang chất lượng tốt nhất. </p>\n<p>Thêm vào đó Mini Mart là nơi cập nhật thường xuyên những thông tin hữu ích về sản phẩm, những tin tức công nghệ mới, “hot” nhất, cũng như những mẹo vặt hết sức bổ ích. Với hệ thống giao diện, cấu trúc website hoàn toàn mới, tiện lợi, khi truy cập Mini Mart khách hàng có thể dễ dàng tìm thấy và đặt hàng sản phẩm mà mình yêu thích. Đồng thời, Mini Mart phát triển hệ thống giao nhận rộng khắp toàn quốc, sản phẩm sẽ được giao đến tận tay người tiêu dùng với thời gian nhanh nhất. </p>\n<p>Đó là sự tiện lợi mua sắm tối đa mà Mini Mart mang lại cho khách hàng. Hãy truy cập ngay Mini Mart để sở hữu những sản phẩm thời trang nam mới nhất. Chúng tôi luôn hân hoan chào đón và hân hạnh được phục vụ quý khách.</p>', 'default.png', '2015-02-15 00:00:00', 'Super Admin', '2015-02-15 16:04:35', 'Super Admin', 1, 1, 1, '', ''),
(68, 4, 'INTEL MUA SAFFRON, CÔNG TY DÙNG TRÍ TUỆ NHÂN TẠO ĐỂ DỰ ĐOÁN HƯ HỎNG MÁY BAY', 'intel-mua-saffron-cong-ty-dung-tri-tue-nhan-tao-de-du-doan-hu-hong-may-bay', '', '<p>Intel mới đ&acirc;y đ&atilde; mua lại một c&ocirc;ng ty khởi nghiệp t&ecirc;n Saffron. Họ đ&atilde; ph&aacute;t triển n&ecirc;n c&ocirc;ng nghệ tr&iacute; tuệ nh&acirc;n tạo c&oacute; thể gi&uacute;p &quot;ti&ecirc;u h&oacute;a&quot; dữ liệu từ nhiều nguồn ri&ecirc;ng biệt, kết nối ch&uacute;ng với nhau v&agrave; đưa ra những b&aacute;o c&aacute;o hỗ trợ việc ra quyết định của c&aacute;c doanh nghiệp. Nghe qua th&igrave; c&oacute; vẻ giống với c&aacute;c c&ocirc;ng cụ ph&acirc;n t&iacute;ch dữ liệu hiện nay, tuy nhi&ecirc;n Saffron n&oacute;i rằng &quot;nền tảng tr&iacute; tuệ tự nhi&ecirc;n&quot; của họ c&oacute; thể l&agrave;m điều đ&oacute; một c&aacute;ch hiệu quả hơn bằng việc tự học hỏi v&agrave; nhận biết, kh&ocirc;ng cần phải được lập tr&igrave;nh sẵn c&aacute;c m&ocirc; h&igrave;nh hay quy tắc t&igrave;m kiếm. C&ocirc;ng nghệ của Saffron c&oacute; thể được d&ugrave;ng để dự đo&aacute;n hư hỏng m&aacute;y m&oacute;c trong m&aacute;y bay, dự b&aacute;o t&agrave;i ch&iacute;nh, ph&aacute;t hiện những vụ lừa đảo bảo hiểm v&agrave; hơn thế nữa. Gi&aacute; trị của thương vụ kh&ocirc;ng được tiết lộ.</p>\r\n\r\n<p><img src="//bizweb.dktcdn.net/100/024/820/files/3526824-cv.jpg?v=1445939406474" /></p>\r\n\r\n<p>Intel n&oacute;i th&ecirc;m: &quot;Ch&uacute;ng t&ocirc;i thấy cơ hội để &aacute;p dụng điện to&aacute;n th&ocirc;ng minh kh&ocirc;ng chỉ trong những cỗ m&aacute;y server to lớn của c&aacute;c doanh nghiệp m&agrave; c&ograve;n trong những thiết bị ti&ecirc;u d&ugrave;ng, vốn đang c&oacute; nhu cầu thấy, cảm nhận v&agrave; hiểu th&ocirc;ng tin phức tạp theo thời gian thực. Saffron sẽ tiếp tục ph&aacute;t triển trong lĩnh vực của họ, cũng như cống hiến cho nỗ lực của Intel về những thiết bị mới, big data, an to&agrave;n th&ocirc;ng tin, sức khỏe v&agrave; Internet of Things&quot;. IBM hiện cũng đang x&agrave;i si&ecirc;u m&aacute;y t&iacute;nh Watson của m&igrave;nh để phục vụ cho việc ph&acirc;n t&iacute;ch dữ liệu bằng tr&iacute; tuệ nh&acirc;n tạo v&agrave; machine learning.</p>\r\n', '3526824-cv1.jpg', '2016-09-28 20:18:40', 'Ngô Trung Phát', '2016-09-28 20:18:40', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(86, 4, 'ỨNG DỤNG ĐI KÈM BLACKBERRY PRIV XUẤT HIỆN TRÊN PLAY STORE', 'ung-dung-di-kem-blackberry-priv-xuat-hien-tren-play-store', '', '<p>BlackBerry vừa cho đăng tải một loạt ứng dụng Android d&agrave;nh ri&ecirc;ng cho BlackBerry Priv l&ecirc;n Google Play Store. Ch&uacute;ng ta c&oacute; tất cả 6 ứng dụng đ&aacute;ng ch&uacute; &yacute; nhất, cụ thể l&agrave; BlackBerry Launcher, BlackBerry Keyboard, BlackBerry Hub, Password Keeper, BlackBerry Camera v&agrave; DTEK. Mặc d&ugrave; đ&atilde; được up l&ecirc;n Play Store nhưng những thiết bị chạy Android kh&aacute;c sẽ kh&ocirc;ng thể tải về v&agrave; c&agrave;i đặt những ứng dụng tr&ecirc;n. Động th&aacute;i n&agrave;y chỉ gi&uacute;p việc cập nhật từng ứng dụng ri&ecirc;ng lẽ sẽ trở n&ecirc;n thuận tiện hơn, người d&ugrave;ng c&oacute; thể tải bản cập nhật của mỗi ứng dụng ngay khi c&oacute; m&agrave; kh&ocirc;ng cần phải đợi đến khi c&oacute; bản cập nhật của to&agrave;n bộ hệ thống.</p>\r\n\r\n<p>Mặc d&ugrave; hiện tại ch&uacute;ng ta kh&ocirc;ng thể tải về để x&agrave;i cho bất kỳ thiết bị n&agrave;o kh&aacute;c, nhưng điều n&agrave;y cũng gi&uacute;p mang lại một c&aacute;i nh&igrave;n cụ thể hơn về giao diện cũng như chức năng của những ứng dụng quan trọng sắp hiện diện tr&ecirc;n BlackBerry Priv. Mời c&aacute;c bạn xem qua.</p>\r\n', '3526950-blackberry-app-cover1.jpg', '2016-09-28 20:20:08', 'Ngô Trung Phát', '2016-09-28 20:20:08', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(87, 4, 'TRÊN TAY MÁY QUAY CẦM TAY 4K OSMO TÍCH HỢP GIMBAL CỦA DJI', 'tren-tay-may-quay-cam-tay-4k-osmo-tich-hop-gimbal-cua-dji', '', '<p>DJI Osmo l&agrave; chiếc m&aacute;y quay 4K cầm tay nhỏ gọn mới nhất của DJI, n&oacute; vừa được ra mắt v&agrave;o đầu th&aacute;ng 10 n&agrave;y v&agrave; đ&atilde; được Camera Tinh Tế giới thiệu. Osmo l&agrave; một tay cầm điều khiển được gắn th&ecirc;m cụm gimbal Zenmuse X3 của chiếc drone Inspire 1, trong đ&oacute; cụm gimbal bao gồm một gimbal điện tử gắn c&ugrave;ng camera Zenmuse X3. Bạn c&oacute; thể quay video 4K, chụp h&igrave;nh với độ ph&acirc;n giải 12MP chất lượng cao một c&aacute;ch mượt m&agrave;, lại bỏ c&aacute;c rung động kh&ocirc;ng mong muốn một c&aacute;ch triệt để nhất do n&oacute; được t&iacute;ch hợp cụm gimbal qu&aacute; nổi tiếng của DJI_h&atilde;ng chuy&ecirc;n sản xuất drone v&agrave; c&aacute;c thiết bị ghi h&igrave;nh t&iacute;ch hợp gimbal. Gi&aacute; của sản phẩm n&agrave;y ở Mỹ v&agrave;o khoảng 649$.</p>\r\n\r\n<p>Ngo&agrave;i ra, b&ecirc;n h&ocirc;ng của Osmo c&ograve;n được gắn th&ecirc;m một gi&aacute; kẹp điện thoại c&oacute; thể xoay trở linh hoạt gi&uacute;p cố định điện thoại của bạn v&agrave;o để bạn Live View trực tiếp những gi&agrave; bạn đang quay v&agrave; chụp.</p>\r\n\r\n<p>Osmo được b&aacute;n ra với gi&aacute; 649 USD, đi k&egrave;m chung với camera Zenmuse X3, kh&ocirc;ng b&aacute;n rời. Tuy nhi&ecirc;n DJI vẫn cho ph&eacute;p bạn c&oacute; thể thay thế được cụm camera Zenmuse X3 với cụm camera cao cấp hơn Zenmuse X5 hoặc X5R để c&oacute; thể quay video 4K Raw cũng như tận dụng chất lượng của cảm biến M43 lớn hơn.</p>\r\n\r\n<p><img src="//bizweb.dktcdn.net/100/024/820/files/3526834-osmo-cover2.jpg?v=1445938731876" /></p>\r\n\r\n<p>Th&ocirc;ng số kỹ thuật của Osmo:</p>\r\n\r\n<p>a. Bộ phận tay cầm<br />\r\nK&iacute;ch thước (61.8 x 48.2 x 161.5 mm)<br />\r\nTrọng lượng (đ&atilde; bao gồm pin): 201 g<br />\r\nb. Bộ gimbal + camera&nbsp;<br />\r\nTrọng lượng bộ gimbal Zenmus X3: 221 g<br />\r\nNg&agrave;m: Gắn<br />\r\nBi&ecirc;n độ di chuyển c&oacute; thể điều khiển<br />\r\nXoay dọc: - 35&deg; to +135&deg;<br />\r\nXoay ngang: &plusmn;320&deg;<br />\r\nXoay tr&ograve;n: &plusmn;30&deg;<br />\r\nBi&ecirc;n độ dao động tối đa<br />\r\nXoay dọc (Tilt): - 90&deg; to +150&deg;<br />\r\nXoay ngang (pan): &plusmn;330&deg;<br />\r\nXoay tr&ograve;n: - 50&deg; to +90&deg;<br />\r\nTốc độ xoay 120&deg;/s<br />\r\nc. M&aacute;y ảnh<br />\r\nLoại: X3/FC350H<br />\r\nCảm biến<br />\r\nSony Exmor R CMOS; 1/2.3&rdquo;<br />\r\nĐộ ph&acirc;n giải 12.40M<br />\r\nThấu k&iacute;nh<br />\r\nTi&ecirc;u cự: 20mm<br />\r\nG&oacute;c nh&igrave;n 94&deg;<br />\r\nKhẩu độ tối đa: f/2.8<br />\r\nISO: 100-3200 (video);100-1600 (photo)<br />\r\nTốc độ m&agrave;n trập: 8s － 1/8000s (tối đa 30s ở chế độ M)<br />\r\nC&aacute;c chế độ chụp ảnh<br />\r\nSingle Shot<br />\r\nChup li&ecirc;n tiếp: 3/5/7 shots<br />\r\nAE Bracketing (AEB): 3/5 khung h&igrave;nh +- 0.7EV<br />\r\nInterval<br />\r\nTimelapse<br />\r\nAuto Panorama<br />\r\nSelfie Panorama<br />\r\nVideo<br />\r\nDCI: 4K (4096 x 2160) 24/25p<br />\r\nUHD 4K (3840 x 2160) 24/25/30p<br />\r\n2.7K (2704 x 1520) 24/25/30p<br />\r\nFHD: 1920 x 1080 24/25/30/48/50/60/120p<br />\r\nHD: 1280 x 720 24/25/30/48/50/60p<br />\r\nC&aacute;c chế độ quay video: Auto; Slow Motion<br />\r\nVideo Bitrate: 60 Mbps<br />\r\nThẻ nhớ hỗ trợ Micro SD tối đa 64 GB; Class 10 hoặc UHS-1<br />\r\nd. Pin<br />\r\nLoại: Pin sạc SOY015A-1260120 980mAh 10.8Wh<br />\r\nNguồn v&agrave;o: 100-240 V, 50/60 Hz<br />\r\nNguồn ra: 12.6 V, 1.2 A</p>\r\n', 'default.png', '2016-09-28 20:20:50', 'Ngô Trung Phát', '2016-09-28 20:20:50', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(88, 4, 'GOPRO TUYÊN BỐ THƯỞNG 5 TRIỆU USD', 'gopro-tuyen-bo-thuong-5-trieu-usd', '', '<p><strong>GoPro&nbsp;</strong>vừa c&ocirc;ng bố giải thưởng thường ni&ecirc;n cho những sản phẩm đẹp nhất được ghi lại từ thiết bị của họ với tổng gi&aacute; trị l&ecirc;n đến 5 triệu USD. Bạn c&oacute; thể nộp ảnh, một bộ phim ho&agrave;n thiện hoặc 1 đoạn Clip kh&ocirc;ng chỉnh sửa cũng được, GoPro sẽ tự đ&aacute;nh gi&aacute; v&agrave; nếu c&ocirc;ng ty đ&aacute;nh gi&aacute; sản phẩm của bạn l&agrave; tốt nhất th&igrave; bạn sẽ nhận được một khoảng tiền mặt lớn. Cụ thể, bức ảnh đẹp nhất sẽ được thưởng $500, xấp xỉ 11 triệu đồng, đoạn video RAW (kh&ocirc;ng chỉnh sửa) đẹp nhất sẽ được thưởng $1000, xấp xỉ 22 triệu đồng v&agrave; sản phẩm video ho&agrave;n chỉnh sẽ được thưởng $5000, xấp xỉ 110 triệu đồng. Đ&acirc;y thực sự l&agrave; một cơ hội tốt kh&ocirc;ng chỉ để trổ t&agrave;i m&agrave; c&ograve;n để săn giải thưởng bởi GoPro kh&ocirc;ng giới hạn số lượng sản phẩm dự thi cho mỗi người. Tuy nhi&ecirc;n, bạn n&ecirc;n ch&uacute; &yacute; l&agrave; một khi đ&atilde; gởi Clip cho GoPro, bạn đ&atilde; chấp nhận chuyển giao bản quyền sở hữu cho GoPro. Họ c&oacute; thể sử dụng Clip của bạn l&agrave;m g&igrave; tuỳ &yacute;, để quảng c&aacute;o, để chia sẻ tr&ecirc;n fan page,....</p>\r\n\r\n<p><img src="//bizweb.dktcdn.net/100/024/820/files/3527042-cv.jpg?v=1445937231907" /></p>\r\n\r\n<p>Hiện GoPro chỉ chấp nhận nội dung xoay quanh c&aacute;c chủ đề sau:</p>\r\n\r\n<ol>\r\n	<li>H&agrave;nh động</li>\r\n	<li>Phi&ecirc;u lưu Adventure</li>\r\n	<li>Động vật Animals</li>\r\n	<li>Gia đ&igrave;nh Family</li>\r\n	<li>Xe Motor thể thao ( Motorsports )</li>\r\n	<li>&Acirc;m nhạc</li>\r\n	<li>Thể thao</li>\r\n	<li>Khoa học</li>\r\n	<li>Du lịch</li>\r\n</ol>\r\n\r\n<p>C&aacute;c bước bạn cần l&agrave;m l&agrave;:<br />\r\nGhi lại đoạn video đẹp nhất, &yacute; nghĩa nhất. C&oacute; thể l&agrave; con của bạn đang vui chơi trong vườn, cảnh bạn đang trượt tuyết, mặt trời mọc, Milky way bay tr&ecirc;n đời,.... Những nội dung c&agrave;ng độc đ&aacute;o, cần đẹp, c&agrave;ng ấn tượng c&agrave;ng tốt<br />\r\nGởi video đỉnh nhất của bạn cho GoPro v&agrave; c&ocirc;ng ty sẽ xem, đ&aacute;nh gi&aacute; clip của bạn (chấm k&iacute;n trong c&ocirc;ng ty).<br />\r\nNếu bạn may mắn trung giải, bạn sẽ nhận được tiền mặt từ GoPro.<br />\r\nNộp b&agrave;i thi tại đ&acirc;y: https://gopro.com/awards<br />\r\n​<br />\r\nHiện tại chương tr&igrave;nh chỉ vừa mới khởi động nhưng đ&atilde; c&oacute; 2 đoạn video tr&uacute;ng giải đặc biệt $5000. Mời c&aacute;c bạn xem qua 2 video được đ&aacute;nh gi&aacute; cao bởi GoPro b&ecirc;n dưới. Vẫn c&ograve;n 4,995,000 triệu USD đang chờ bạn. Nhanh tay l&ecirc;n n&agrave;o!</p>\r\n', '3527042-cv1.jpg', '2016-09-28 20:21:52', 'Ngô Trung Phát', '2016-09-28 20:21:52', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(111, 86, 'Chính sách giao hàng và hoàn trả', 'chinh-sach-van-chuyen', 'Cửa hàng sẽ vận chuyển hàng trong khoảng thời gian thỏa thuận khi quý khách thanh toán. Tuy nhiên, chúng tôi không thể đảm bảo tất cả các đơn hàng sẽ được giao trong khoảng thời gian đã thống nhất.....', '<h4>Giao hàng</h4>\n<p>Cửa hàng sẽ vận chuyển hàng trong khoảng thời gian thỏa thuận khi quý khách thanh toán. Tuy nhiên, chúng tôi không thể đảm bảo tất cả các đơn hàng sẽ được giao trong khoảng thời gian đã thống nhất.</p>\n<p>Ngay sau khi đơn hàng của bạn được gửi đi từ kho của chúng tôi, quý khách sẽ nhận được thư xác nhận rằng hàng hóa của bạn đang trên đường vận chuyển. Nếu chúng tôi không thể giao hàng đúng ngày dự đoán, chúng tôi không chịu trách nhiệm về bất kỳ tổn thất, chí phí, thiệt hại hay chi phí phát sinh từ việc giao hàng trễ. Nếu quý khách vắng mặt ở nơi giao hàng, chúng tôi sẽ gửi cho bạn một thẻ hướng dẫn để nhận hàng lại hoặc lấy hàng từ hãng vận chuyển.</p>\n<p>Để kiểm tra đơn hàng của quý khách, hãy sử dụng mã số theo dõi đã được gửi cho khách hàng trong thư xác nhận. Sau khi hàng được chuyển đến quý khách, trước khi ký xác nhận, quý khách vui lòng kiểm tra nếu sản phẩm có bất kỳ lỗi hay khiếm khuyết nào. Để đề phòng, xin vui lòng giữ lại biên lại vận chuyển.</p>\n<p>Tuy hiếm xảy ra nhưng nếu quý khách phát hiện sản phảm bị khiếm khuyết hay thiếu sản phẩm trong đơn hàng, vui lòng thực hiện các bước hướng dẫn trong mục “Các câu hỏi thường gặp” hay liên lạc với Bộ phận Chăm Sóc Khách Hàng qua <a href = "mailto:customer@theshop.vn">customer@theshop.vn.</a></p>\n<h4>Hoàn trả hàng</h4>\n<p>Trong trường hợp khách hàng muốn trả lại hàng đã mua, quý khách có 30 ngày từ ngày mua hàng để hoàn trả đơn hàng. Vui lòng liên lạc với Bộ phận Chăm Sóc Khách Hàng của Cửa hàng để bắt đầu hoàn trả.</p> \n<p>Vui lòng đảm bảo mảnh giấy hoàn trả đã được đính kèm với bưu kiện, và sản phẩm chưa được mặc và còn trong tình trạng ban đầu. Sản phẩm cần phải được chuyển đến địa chỉ hoàn trả của chúng tôi trong vòng 30 ngày từ ngày mua hàng.</p>\n<p>Cửa hàng</p>\n<p>61/2 Quang Trung, P.10</p> \n<p>Q. Gò Vấp, TP. HCM</p>\n<p>Khi phòng Quản lý Chất lượng đã kiểm tra hàng được trả về, chúng tôi sẽ tiến hàng quá trình hoàn trả của bạn trong vòng 5 ngày làm việc</p>\n<p>Qúy khách sẽ chịu trách nhiệm cho tất cả hàng hóa cho đến khi chúng được gửi đến nhà kho của công ty và đồng ý trả chi phí liên quan đến việc trả hàng của quý vị. Vui lòng đảm bảo đóng gói hàng hóa cẩn thận để đề phòng thiệt hại. Chúng tôi khuyên bạn nên sử dụng hãng vận chuyển uy tín có dịch vụ theo dõi bưu phẩm.</p>\n<p>Nếu quý khách có câu hỏi hay yêu cầu khác, vui lòng liên hệ Bộ phận Chăm Sóc Khách Hàng qua email <a href = "mailto:customer@theshop.vn">customer@theshop.vn.</a></p>', 'default.png', '2015-02-15 00:00:00', 'Super Admin', '2015-02-15 16:04:35', 'Super Admin', 1, 1, 1, '', ''),
(112, 87, 'Chính sách bảo mật', 'chinh-sach-bao-mat', '<p>Riêng tư của khách hàng vô cùng quan trọng với Mini Mart, vì thế chúng tôi chỉ sử dụng thông tin cá nhân của quý khách vào những trường hợp nêu ra sau đây. </p>\r\n		<p>Bảo vệ dữ liệu là vấn đề của sự tin tưởng và bảo mật danh tính của quý khách vô cùng quan trọng với chúng tôi. Vì thế, chúng tôi chỉ sẽ sử dụng tên và một số thông tin khác của quý khách theo cách được đề ra trong Chính sách Bảo mật này. Chúng tôi chỉ sẽ thu thập những thông tin cần thiết và có liên quan đến giao dịch giữa chúng tôi và quý khách.....</p>', '<p>Riêng tư của khách hàng vô cùng quan trọng với Mini Mart, vì thế chúng tôi chỉ sử dụng thông tin cá nhân của quý khách vào những trường hợp nêu ra sau đây. </p>\n		<p>Bảo vệ dữ liệu là vấn đề của sự tin tưởng và bảo mật danh tính của quý khách vô cùng quan trọng với chúng tôi. Vì thế, chúng tôi chỉ sẽ sử dụng tên và một số thông tin khác của quý khách theo cách được đề ra trong Chính sách Bảo mật này. Chúng tôi chỉ sẽ thu thập những thông tin cần thiết và có liên quan đến giao dịch giữa chúng tôi và quý khách.</p>\n		<p>Chúng tôi chỉ giữ thông tin của quý khách trong thời gian luật pháp yêu cầu hoặc cho mục đích mà thông tin đó được thu thập.</p>\n		<p>Quý khách có thể ghé thăm trang web mà không cần phải cung cấp bất kỳ thông tin cá nhân nào. Khi viếng thăm trang web, quý khách sẽ luôn ở trong tình trạng vô danh trừ khi quý khách có tài khoản trên trang web và đăng nhập vào bằng tên và mật khẩu của mình.</p>\n		<p>Nếu quý khách có ý kiến hay đóng góp gì, xin vui lòng gửi đến địa chỉ bưu điện hoặc gửi email tới <a href="mailto:customer@gmail.com">customer@gmail.com</a>. Chúng tôi luôn sẳn sàng lắng nghe nhận xét của quý khách.</p>\n		<p>Quy định Bảo mật của chúng tôi hoàn toàn tuân theo Đạo luật Bảo Mật 1988 và là phương châm tốt nhất của nền công nghiệp. </p>\n		Thu thập thông tin cá nhân<br />\n		Cập nhật thông tin cá nhân<br />\n		Bảo mật thông tin cá nhân<br />\n		Tiết lộ thông tin cá nhân<br />\n		Thu thập dữ liệu máy tính<br />\n		Thay đổi của Chính sách Bảo Mật<br />\n		Thu thập thông tin cá nhân<br />\n		<p>Mini Mart không bán, chia sẻ hay trao đổi thông tin cá nhân của khách hàng thu thập trên trang web cho một bên thứ ba nào khác.</p>\n		<p>Thông tin cá nhân thu thập được sẽ chỉ được sử dụng trong nội bộ công ty. </p>\n		<p>Khi quý khách đăng ký tài khoản Mini Mart, thông tin cá nhân mà chúng tôi thu thập bao gồm:</p>\n		<ul>\n			<li>Tên</li>\n			<li>Địa chỉ giao hàng</li>\n			<li>Địa chỉ Email</li>\n			<li>Số điện thoại cố định</li>\n			<li>Số điện thoại di động</li>\n			<li>Ngày sinh</li>\n			<li>Giới tính</li>\n		</ul>\n		<p>Những thông tin trên sẽ được sử dụng cho một hoặc tất cả các mục đích sau đây:</p>\n		<ul>\n			<li>Giao hàng quý khách đã mua tại Mini Mart</li>\n			<li>Thông báo về việc giao hàng và hỗ trợ khách hàng</li>\n			<li>Cung cấp thông tin liên quan đến sản phẩm</li>\n			<li>Xử lý đơn đặt hàng và cung cấp dịch vụ và thông tin qua trang web của chúng tôi theo yêu cầu của quý khách</li>\n		</ul>\n		<p>Ngoài ra, chúng tôi sẽ sử dụng thông tin quý khách cung cấp để hỗ trợ quản lý tài khoản khách hàng; xác nhận và thực hiện các giao dịch tài chính liên quan đến các khoản thanh toán trực tuyến của quý khách; kiểm tra dữ liệu tải từ trang web của chúng tôi; cải thiện giao diện và/hoặc nội dung của các trang mục trên trang web và tùy chỉnh để dễ dàng hơn khi sử dụng; nhận diện khách đến thăm trang web; nghiên cứu về nhân khẩu học của người sử dụng; gửi đến quý khách thông tin mà chúng tôi nghĩ sẽ có ích hoặc do quý khách yêu cầu, bao gồm thông tin về sản phẩm và dịch vụ, với điều kiện quý khách đồng ý không phản đối việc được liên lạc cho các mục đích trên.</p>\n		<p>Chúng tôi có thể chia sẻ tên và địa chỉ của quý khách cho dịch vụ chuyển phát nhanh hoặc nhà cung cấp của chúng tôi để có thể giao hàng cho quý khách.</p>\n		Khi quý khách đăng ký làm thành viên trên trang web Mini Mart, chúng tôi cũng sẽ sử dụng thông tin cá nhân của quý khách để gửi các thông tin khuyến mãi/tiếp thị. Quý khách có thể hủy nhận các thông tin đó bất kỳ lúc nào bằng cách sử dụng chức năng hủy đăng ký trong các thông báo quảng cáo.\n		Các khoản thanh toán mà quý khách thực hiện qua trang web sẽ được xử lý bởi công ty của chúng tôi, CÔNG TY TNHH BÁN LẺ VÀ GIAO NHẬN RECESS. Quý khách phải cung cấp cho chúng tôi hoặc đại lý của chúng tôi hoặc trang web những thông tin chính xác và phải luôn cập nhật thông tin và báo cho chúng tôi biết nếu có thay đổi.<br />\n		Chi tiết đơn hàng của quý khách sẽ được chúng tôi lưu trữ nhưng vì lý do bảo mật, quý khách không thể yêu cầu thông tin đó từ chúng tôi. Tuy nhiên, quý khách có thể kiểm tra thông tin đó bằng cách đăng nhập vào tài khoản riêng của mình trên trang web. Tại đó, quý khách có thể theo dõi đầy đủ chi tiết của các đơn hàng đã hoàn tất, những đơn hàng mở và những đơn hàng sắp được giao cũng như quản lý thông tin về địa chỉ, thông tin về ngân hàng và những bản tin mà quý khách đã đăng ký nhận. Quý khách cần bảo đảm là thông tin được truy cập một cách bí mật và không làm lộ cho một bên thứ ba không có quyền. Chúng tôi sẽ không chịu trách nhiệm đối với việc sử dụng sai mật khẩu trừ khi đó là lỗi của chúng tôi.<br />\n		Cập nhật thông tin cá nhân<br />\n		Quý khách có thể cập nhật thông tin cá nhân của mình bất kỳ lúc nào bằng cách đăng nhập vào trang web Mini Mart.<br />\n		Bảo mật thông tin cá nhân<br />\n		<p>Mini Mart đảm bảo rằng mọi thông tin thu thập được sẽ được lưu giữ an toàn. Chúng tôi bảo vệ thông tin cá nhân của quý khách bằng cách:</p> \n		<ul>\n			<li>Giới hạn truy cập thông tin cá nhân</li>\n			<li>Sử dụng sản phẩm công nghệ để ngăn chặn truy cập máy tính trái phép</li>\n			<li>Xóa thông tin cá nhân của quý khách khi nó không còn cần thiết cho mục đích lưu trữ hồ sơ của chúng tôi</li>\n		</ul>\n		Mini Mart sử dụng công nghệ mã hóa theo giao thức 128-bit SSL (secure sockets layer) khi xử lý thông tin tài chính của quý khách. Mã hóa 128-bit SSL phải mất xấp xỉ một nghìn tỉ năm mới có thể phá vỡ được và là giao thức tiêu chuẩn của mã hóa.<br />\n		Tiết lộ thông tin cá nhân<br />\n		Chúng tôi sẽ không chia sẻ thông tin của quý khách cho bất kỳ một công ty nào khác ngoại trừ những công ty và các bên thứ ba có liên quan trực tiếp đến việc giao hàng mà quý khách đã mua tại Mini Mart. Trong một vài trường hợp đặc biệt, Mini Mart có thể bị yêu cầu phải tiết lộ thông tin cá nhân, ví dụ như khi có căn cứ cho việc tiết lộ thông tin là cần thiết để ngăn chặn các mối đe dọa về tính mạng và sức khỏe, hay cho mục đích thực thi pháp luật. Mini Mart cam kết tuân thủ Đạo luật Bảo Mật và các Nguyên tắc Bảo mật Quốc gia.<br />\n		Nếu quý khách tin rằng bảo mật của quý khách bị Mini Mart xâm phạm, xin vui lòng liên hệ với chúng tôi tại địa chỉ customer@Mini Mart để được giải quyết vấn đề.<br />\n		Thu thập dữ liệu máy tính<br />\n		Khi quý khách đến thăm Mini Mart, máy chủ của công ty chúng tôi sẽ tự động lưu trữ thông tin mà trình duyệt của quý khách gửi đến. Những thông tin này bao gồm: <br />\n		<ul>\n			<li>Địa chỉ IP của quý khách</li>\n			<li>Loại trình duyệt</li>\n			<li>Các trang web mà quý khách đã ghé thăm trước khi đến trang web của chúng tôi</li>\n			<li>Những trang mục trong Mini Mart mà quý khách ghé thăm</li>\n			<li>Khoảng thời gian quý khách giành ra đã xem những trang mục đó, sản phẩm, tìm kiếm thông tin trên trang web, thời gian và ngày tháng truy cập, và các số liệu thống kê khác.</li>\n		</ul>\n		Các thông tin đó được thu thập cho mục đích phân tích và đánh giá để góp phần cải thiện trang web, dịch vụ, và sản phẩm mà chúng tôi cung cấp. Những dữ liệu đó sẽ không có liên quan gì đến các thông tin cá nhân khác.<br />\n		Như đã đề cập ở phần trên, Mini Mart sẽ sử dụng tính năng của Google Analytics dựa trên những dữ liệu về quảng cáo bằng hình ảnh (Display advertising) bao gồm những phần sau: báo cáo về Re-marketing, báo cáo về số lượt hiện quảng cáo hình ảnh trên mạng lưới trang web của Google, báo cáo về Double Click, thống kê hành vi &amp; sở thích khách hàng trên Google Analytics. Hãy chọn mục thiết lập quảng cáo của Google (https://www.google.com/settings/ads ), để bạn có thể tắt phần hiển thị quảng cáo từ Google Analytics và tùy chỉnh quảng cáo trên hệ thống Google Display Network.<br />\n		Ngoài ra, Mini Mart còn sử dụng chức năng Re-marketing của Google Analytic để quảng cáo trực tuyến; các bên thứ ba (bao gồm Google) sẽ có thể hiển thị quảng cáo của Mini Mart trên các website liên kết. Mini Mart và bên cung cấp thứ 3, bao gồm cả Google, sẽ sử dụng cookies của bên thứ nhất (như Google Analytics cookies và cookie) của bên thứ 3 (như DoubleClick) để thông báo, tối ưu hóa và trình chiếu các mẫu quảng cáo dựa trên những lần khách hàng truy cập website trước đó, đồng thời cho biết kết quả phản ứng của khách hàng đối với mẫu quảng cáo, những cách sử dụng khác của quảng cáo và độ tương tác của những mẫu quảng cáo này và dịch vụ quảng cáo đến số lượng truy cập vào trang Mini Mart.<br />\n		Thay đổi của Chính sách Bảo mật <br />\n		Mini Mart có quyền thay đổi và chỉnh sửa Quy định Bảo mật vào bất kỳ lúc nào. Bất cứ thay đổi nào về chính sách này đều được đăng trên trang web của chúng tôi.<br />\n		Nếu quý khách không hài lòng với việc chúng tôi xử lý thắc mắc hay khiếu nại của quý khách, xin vui lòng liên hệ với chúng tôi tại customer@Mini Mart.<br />', 'default.png', '2015-02-15 00:00:00', 'Super Admin', '2015-02-15 16:04:35', 'Super Admin', 1, 1, 1, '', ''),
(113, 88, 'Điều kiện đổi trả sản phẩm', 'dieu-kien-doi-tra-san-pham', '', '<p>Nhằm mang đến những trải nghiệm mua sắm tốt nhất cho bạn, The Luxury fashion hỗ trợ dịch vụ đổi trả h&agrave;ng v&ocirc; c&ugrave;ng thuận tiện v&agrave; ho&agrave;n to&agrave;n MIỄN PH&Iacute;, trong v&ograve;ng 30 ng&agrave;y kể từ ng&agrave;y nhận h&agrave;ng. Vui l&ograve;ng đọc kỹ Điều kiện đổi v&agrave; trả h&agrave;ng. Sản phẩm gửi trả sẽ kh&ocirc;ng được The Luxury fashion chấp nhận nếu kh&ocirc;ng đ&aacute;p ứng một trong những điều kiện b&ecirc;n dưới. Bạn sẽ phải thanh to&aacute;n số tiền cho 2 lần vận chuyển (gửi h&agrave;ng về cho The Luxury fashion, v&agrave; The Luxury fashion gửi trả lại h&agrave;ng cho bạn)</p>\r\n\r\n<p><br />\r\nĐIỀU KIỆN ĐỔI TRẢ SẢN PHẨM:</p>\r\n\r\n<p>H&oacute;a đơn VAT c&ograve;n nguy&ecirc;n vẹn, kh&ocirc;ng chấp v&aacute; hoặc tẩy x&oacute;a.<br />\r\nTrong v&ograve;ng 30 ng&agrave;y kể từ ng&agrave;y nhận h&agrave;ng.<br />\r\nC&ograve;n nguy&ecirc;n tem, nh&atilde;n m&aacute;c, hộp v&agrave; bao b&igrave; c&oacute; d&aacute;n m&atilde; sản phẩm của TheShop.<br />\r\nChưa sử dụng, giặt ủi, kh&ocirc;ng bị dơ bẩn. (V&igrave; vậy khi thử c&aacute;c sản phẩm gi&agrave;y d&eacute;p, bạn n&ecirc;n mang vớ v&agrave; thử gi&agrave;y tr&ecirc;n bề mặt sạch)<br />\r\nCh&iacute;nh s&aacute;ch đổi trả h&agrave;ng kh&ocirc;ng &aacute;p dụng cho: đồ l&oacute;t, đồ ngủ, đồ bơi, đồ thể dục thẩm mỹ v&agrave; yoga, sản phẩm l&agrave;m đẹp, b&ocirc;ng tai, vớ, quần legging, văn ph&ograve;ng phẩm, v&agrave; c&aacute;c sản phẩm trong chương tr&igrave;nh giảm gi&aacute;, khuyến m&atilde;i kh&ocirc;ng &aacute;p dụng ch&iacute;nh s&aacute;ch đổi trả h&agrave;ng (nếu c&oacute;).<br />\r\nLưu &yacute;: Dịch vụ đổi h&agrave;ng chỉ &aacute;p dụng cho c&ugrave;ng sản phẩm nhưng kh&aacute;c m&agrave;u kh&aacute;c size, KH&Ocirc;NG &aacute;p dụng cho sản phẩm đặt tại TheShop Singapore v&agrave; sản phẩm được cung cấp bởi đối t&aacute;c của TheShop.</p>\r\n\r\n<p>C&Aacute;C BƯỚC HO&Agrave;N TRẢ SẢN PHẨM</p>\r\n\r\n<p><br />\r\n1. Điền v&agrave;o &quot;Phiếu Ho&agrave;n Trả Sản Phẩm&quot;:</p>\r\n\r\n<p>Chọn 1 trong 8 l&yacute; do cho mỗi sản phẩm ho&agrave;n trả: 1- Kh&ocirc;ng th&iacute;ch kiểu d&aacute;ng, 2- Kh&ocirc;ng th&iacute;ch m&agrave;u sắc, 3- Kh&ocirc;ng th&iacute;ch chất liệu, 4- Sản phẩm kh&aacute;c website, 5- Sản phẩm bị lỗi, 6- K&iacute;ch cỡ qu&aacute; nhỏ, 7- K&iacute;ch cỡ qu&aacute; to, 8- L&yacute; do kh&aacute;c.</p>\r\n\r\n<p>2. Đ&oacute;ng g&oacute;i sản phẩm cần ho&agrave;n trả:</p>\r\n\r\n<p>Đ&oacute;ng g&oacute;i sản phẩm k&egrave;m đầy đủ h&oacute;a đơn VAT v&agrave; &quot;Phiếu Ho&agrave;n Trả Sản Phẩm&quot; để gửi về TheShop.</p>\r\n\r\n<p>3. Gửi kiện h&agrave;ng đ&atilde; đ&oacute;ng g&oacute;i về TheShop:</p>\r\n\r\n<p>Nếu bạn ở nội th&agrave;nh H&agrave; Nội hoặc Th&agrave;nh phố Hồ Ch&iacute; Minh:</p>\r\n\r\n<p>Lựa chọn 1: Y&ecirc;u cầu TheShop đến nhận h&agrave;ng ho&agrave;n trả tận nơi, vui l&ograve;ng đăng k&yacute; th&ocirc;ng tin gửi trả tại đ&acirc;y.<br />\r\nLựa chọn 2: Sau khi đ&oacute;ng g&oacute;i, d&aacute;n &quot;Tem chuyển ph&aacute;t nhanh miễn ph&iacute;&quot; (đ&atilde; in sẵn &quot;M&atilde; vận chuyển&quot; v&agrave; th&ocirc;ng tin cần thiết) b&ecirc;n ngo&agrave;i kiện h&agrave;ng v&agrave; đem ra bưu điện gần nhất. Mọi thủ tục sẽ được Bưu điện tiến h&agrave;nh tự động v&agrave; ho&agrave;n to&agrave;n miễn ph&iacute;.</p>\r\n\r\n<p>Nếu bạn ở c&aacute;c tỉnh v&agrave; th&agrave;nh phố kh&aacute;c:</p>\r\n\r\n<p>Sau khi đ&oacute;ng g&oacute;i, d&aacute;n &quot;Tem chuyển ph&aacute;t nhanh miễn ph&iacute;&quot; (đ&atilde; in sẵn &quot;M&atilde; vận chuyển&quot; v&agrave; th&ocirc;ng tin cần thiết) b&ecirc;n ngo&agrave;i kiện h&agrave;ng v&agrave; đem ra bưu điện gần nhất.<br />\r\nMọi thủ tục sẽ được Bưu điện tiến h&agrave;nh tự động v&agrave; ho&agrave;n to&agrave;n miễn ph&iacute;.<br />\r\n4. Nhận tiền ho&agrave;n trả v&agrave;o V&iacute; Điện Tử:</p>\r\n\r\n<p>Để qu&aacute; tr&igrave;nh ho&agrave;n tiền được tiến h&agrave;nh nhanh ch&oacute;ng nhất c&oacute; thể, TheShop sẽ chủ động ho&agrave;n tiền v&agrave;o V&iacute; Điện Tử của bạn trong v&ograve;ng 24 giờ ngay sau khi nhận được sản phẩm ho&agrave;n trả.<br />\r\nTiền trong V&iacute; Điện Tử c&oacute; gi&aacute; trị 1 năm v&agrave; bằng với số tiền bạn đ&atilde; thanh to&aacute;n (kh&ocirc;ng bao gồm ph&iacute; vận chuyển v&agrave; m&atilde; giảm gi&aacute;), sẽ được khấu trừ trong qu&aacute; tr&igrave;nh đặt h&agrave;ng tiếp theo v&agrave; &aacute;p dụng đồng thời với c&aacute;c m&atilde; giảm gi&aacute; kh&aacute;c.<br />\r\nĐể thay đổi h&igrave;nh thức ho&agrave;n tiền kh&aacute;c, vui l&ograve;ng li&ecirc;n hệ bộ phận CSKH</p>\r\n\r\n<p>C&Aacute;C BƯỚC ĐỔI SẢN PHẨM</p>\r\n\r\n<p><br />\r\nVui l&ograve;ng li&ecirc;n hệ Bộ phận Chăm s&oacute;c Kh&aacute;ch h&agrave;ng hoặc đăng nhập v&agrave;o t&agrave;i khoản c&aacute; nh&acirc;n để đăng k&yacute; đổi h&agrave;ng (c&ugrave;ng sản phẩm, kh&aacute;c m&agrave;u, kh&aacute;c size). Dịch vụ đổi h&agrave;ng tại TheShop ho&agrave;n to&agrave;n miễn ph&iacute; tr&ecirc;n to&agrave;n quốc</p>\r\n\r\n<p>Trong v&ograve;ng 30 ng&agrave;y kể từ khi nhận h&agrave;ng, nếu bạn muốn đổi sản phẩm, t&ugrave;y v&agrave;o khu vực giao h&agrave;ng, chỉ cần thực hiện theo c&aacute;c bước sau:</p>\r\n\r\n<p>Nếu bạn ở nội th&agrave;nh H&agrave; Nội hoặc Th&agrave;nh phố Hồ Ch&iacute; Minh:</p>\r\n\r\n<p>Li&ecirc;n hệ Bộ phận Chăm s&oacute;c Kh&aacute;ch h&agrave;ng th&ocirc;ng qua Hotline để được hỗ trợ đổi sản phẩm tận nơi: Nh&acirc;n vi&ecirc;n giao h&agrave;ng sẽ đem sản phẩm bạn muốn đổi đến tận nh&agrave;, đồng thời nhận lại sản phẩm ban đầu.<br />\r\nNếu bạn ở khu vực kh&aacute;c:</p>\r\n\r\n<p>Y&ecirc;u cầu đổi h&agrave;ng bằng c&aacute;ch li&ecirc;n hệ TheShop để gi&uacute;p giữ sản phẩm bạn muốn đổi trong v&ograve;ng 10 ng&agrave;y.<br />\r\nTiến h&agrave;nh đ&oacute;ng g&oacute;i để ho&agrave;n trả sản phẩm ban đầu v&agrave; gửi về cho TheShop qua bưu điện (giống như Bước 3 trong &quot;C&aacute;c Bước Ho&agrave;n Trả Sản Phẩm&quot;, xem phần tr&ecirc;n)<br />\r\nNgay khi nhận được sản phẩm ho&agrave;n trả, TheShop sẽ tiến h&agrave;nh gửi sản phẩm bạn muốn đổi đến cho bạn.<br />\r\nLưu &yacute;:<br />\r\nTrong 10 ng&agrave;y: TheShop chỉ giữ sản phẩm bạn muốn đổi.<br />\r\nSau 10 ng&agrave;y: Nếu vẫn chưa nhận được sản phẩm bạn gửi trả, y&ecirc;u cầu đổi h&agrave;ng vẫn c&ograve;n hiệu lực, nhưng trong trường hợp sản phẩm bạn muốn đổi đ&atilde; hết h&agrave;ng, TheShop sẽ tiến h&agrave;nh ho&agrave;n tiền cho bạn (l&uacute;c n&agrave;y xem như đơn h&agrave;ng ho&agrave;n trả).<br />\r\nSau 15 ng&agrave;y: Y&ecirc;u cầu đổi h&agrave;ng hết hiệu lực.</p>\r\n\r\n<p>LI&Ecirc;N HỆ CHĂM S&Oacute;C KH&Aacute;CH H&Agrave;NG<br />\r\nEmail: customer@vn.TheShop.com<br />\r\nPhone: 123456789<br />\r\nGiờ l&agrave;m việc:<br />\r\nThứ 2 - Chủ nhật: 8h - 18h<br />\r\nNg&agrave;y Lễ: 9h - 18h</p>\r\n', 'default.png', '2015-02-15 00:00:00', 'Super Admin', '2016-10-20 23:48:47', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(114, 89, 'Vận chuyển', 'van-chuyen', '', '<p>Giao h&agrave;ng<br />\r\nMini Mart sẽ vận chuyển h&agrave;ng trong khoảng thời gian thỏa thuận khi qu&yacute; kh&aacute;ch thanh to&aacute;n. Tuy nhi&ecirc;n, ch&uacute;ng t&ocirc;i kh&ocirc;ng thể đảm bảo tất cả c&aacute;c đơn h&agrave;ng sẽ được giao trong khoảng thời gian đ&atilde; thống nhất.<br />\r\nNgay sau khi đơn h&agrave;ng của bạn được gửi đi từ kho của ch&uacute;ng t&ocirc;i, qu&yacute; kh&aacute;ch sẽ nhận được thư x&aacute;c nhận rằng h&agrave;ng h&oacute;a của bạn đang tr&ecirc;n đường vận chuyển. Nếu ch&uacute;ng t&ocirc;i kh&ocirc;ng thể giao h&agrave;ng đ&uacute;ng ng&agrave;y dự đo&aacute;n, ch&uacute;ng t&ocirc;i kh&ocirc;ng chịu tr&aacute;ch nhiệm về bất kỳ tổn thất, ch&iacute; ph&iacute;, thiệt hại hay chi ph&iacute; ph&aacute;t sinh từ việc giao h&agrave;ng trễ. Nếu qu&yacute; kh&aacute;ch vắng mặt ở nơi giao h&agrave;ng, ch&uacute;ng t&ocirc;i sẽ gửi cho bạn một thẻ hướng dẫn để nhận h&agrave;ng lại hoặc lấy h&agrave;ng từ h&atilde;ng vận chuyển.<br />\r\nĐể kiểm tra đơn h&agrave;ng của qu&yacute; kh&aacute;ch, h&atilde;y sử dụng m&atilde; số theo d&otilde;i đ&atilde; được gửi cho kh&aacute;ch h&agrave;ng trong thư x&aacute;c nhận. Sau khi h&agrave;ng được chuyển đến qu&yacute; kh&aacute;ch, trước khi k&yacute; x&aacute;c nhận, qu&yacute; kh&aacute;ch vui l&ograve;ng kiểm tra nếu sản phẩm c&oacute; bất kỳ lỗi hay khiếm khuyết n&agrave;o. Để đề ph&ograve;ng, xin vui l&ograve;ng giữ lại bi&ecirc;n lại vận chuyển.<br />\r\nTuy hiếm xảy ra nhưng nếu qu&yacute; kh&aacute;ch ph&aacute;t hiện sản phảm bị khiếm khuyết hay thiếu sản phẩm trong đơn h&agrave;ng, vui l&ograve;ng thực hiện c&aacute;c bước hướng dẫn trong mục &ldquo;C&aacute;c c&acirc;u hỏi thường gặp&rdquo; hay li&ecirc;n lạc với Bộ phận Chăm S&oacute;c Kh&aacute;ch H&agrave;ng qua customer@The-Luxury fashion.vn.<br />\r\nHo&agrave;n trả h&agrave;ng<br />\r\nTrong trường hợp kh&aacute;ch h&agrave;ng muốn trả lại h&agrave;ng đ&atilde; mua, qu&yacute; kh&aacute;ch c&oacute; 30 ng&agrave;y từ ng&agrave;y mua h&agrave;ng để ho&agrave;n trả đơn h&agrave;ng. Vui l&ograve;ng li&ecirc;n lạc với Bộ phận Chăm S&oacute;c Kh&aacute;ch H&agrave;ng của Mini Mart để bắt đầu ho&agrave;n trả.<br />\r\nVui l&ograve;ng đảm bảo mảnh giấy ho&agrave;n trả đ&atilde; được đ&iacute;nh k&egrave;m với bưu kiện, v&agrave; sản phẩm chưa được mặc v&agrave; c&ograve;n trong t&igrave;nh trạng ban đầu. Sản phẩm cần phải được chuyển đến địa chỉ ho&agrave;n trả của ch&uacute;ng t&ocirc;i trong v&ograve;ng 30 ng&agrave;y từ ng&agrave;y mua h&agrave;ng.<br />\r\nMini Mart<br />\r\n61/2 Quang Trung, P.10<br />\r\nQ. G&ograve; Vấp, TP. HCM<br />\r\nKhi ph&ograve;ng Quản l&yacute; Chất lượng đ&atilde; kiểm tra h&agrave;ng được trả về, ch&uacute;ng t&ocirc;i sẽ tiến h&agrave;ng qu&aacute; tr&igrave;nh ho&agrave;n trả của bạn trong v&ograve;ng 5 ng&agrave;y l&agrave;m việc<br />\r\nQ&uacute;y kh&aacute;ch sẽ chịu tr&aacute;ch nhiệm cho tất cả h&agrave;ng h&oacute;a cho đến khi ch&uacute;ng được gửi đến nh&agrave; kho của c&ocirc;ng ty v&agrave; đồng &yacute; trả chi ph&iacute; li&ecirc;n quan đến việc trả h&agrave;ng của qu&yacute; vị. Vui l&ograve;ng đảm bảo đ&oacute;ng g&oacute;i h&agrave;ng h&oacute;a cẩn thận để đề ph&ograve;ng thiệt hại. Ch&uacute;ng t&ocirc;i khuy&ecirc;n bạn n&ecirc;n sử dụng h&atilde;ng vận chuyển uy t&iacute;n c&oacute; dịch vụ theo d&otilde;i bưu phẩm.<br />\r\nNếu qu&yacute; kh&aacute;ch c&oacute; c&acirc;u hỏi hay y&ecirc;u cầu kh&aacute;c, vui l&ograve;ng li&ecirc;n hệ Bộ phận Chăm S&oacute;c Kh&aacute;ch H&agrave;ng qua email <a href="mailto:customer@gmail.com">customer@gmail.com</a>.</p>\r\n', 'default.png', '2016-09-28 18:53:46', 'Ngô Trung Phát', '2016-09-28 18:53:46', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(115, 90, 'Phí vận chuyển', 'phi-van-chuyen', '', '<p>Do sản phẩm của c&aacute;c nh&agrave; cung cấp được đặt tại nhiều kho kh&aacute;c nhau, Mini Mart sẽ ủy quyền vận chuyển sản phẩm cho mỗi nh&agrave; cung cấp tương ứng trong giỏ h&agrave;ng. V&igrave; vậy:<br />\r\nTừ ng&agrave;y 22/09/2015, ph&iacute; vận chuyển được &aacute;p dụng ri&ecirc;ng biệt cho từng NH&Agrave; CUNG CẤP*:<br />\r\nGi&aacute; trị mua h&agrave;ng từ c&ugrave;ng 1 NH&Agrave; CUNG CẤP &ge; 449,000 VNĐ: MIỄN PH&Iacute; ho&agrave;n to&agrave;n ph&iacute; vận chuyển tr&ecirc;n to&agrave;n quốc<br />\r\nGi&aacute; trị mua h&agrave;ng từ c&ugrave;ng 1 NH&Agrave; CUNG CẤP &lt; 449,000 VNĐ: Vui l&ograve;ng tham khảo biểu ph&iacute; giao h&agrave;ng cho từng khu vực tại đ&acirc;y.<br />\r\n* Để x&aacute;c định NH&Agrave; CUNG CẤP, bạn vui l&ograve;ng xem ghi ch&uacute; &ldquo;Cung cấp bởi ...&rdquo; tại 2 vị tr&iacute; sau:<br />\r\n1. Trang Cataloge:<br />\r\n2. Trang sản phẩm:<br />\r\nNếu kh&ocirc;ng c&oacute; ghi ch&uacute; tr&ecirc;n, th&igrave; sản phẩm thuộc NH&Agrave; CUNG CẤP Mini Mart.<br />\r\nVới sự đổi mới n&agrave;y, Mini Mart cam kết:<br />\r\n1. Giao sản phẩm đến bạn sớm nhất c&oacute; thể<br />\r\n2. Mang đến nhiều sản phẩm chất lượng từ nhiều nh&agrave; cung cấp hơn với mức gi&aacute; hấp dẫn hơn, tạo n&ecirc;n sự phong ph&uacute; v&agrave; h&agrave;i l&ograve;ng khi mua sắm cho bạn.</p>\r\n', 'default.png', '2016-09-28 00:00:00', 'Ngô Trung Phát', '2016-09-28 17:27:52', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(116, 91, 'Phương thức thanh toán', 'phuong-thuc-thanh-toan', '', '<p>Nhằm mang đến qu&yacute; kh&aacute;ch những trải nghiệm mua sắm trực tuyến tuyệt vời nhất, tại đ&acirc;y, ch&uacute;ng t&ocirc;i đưa ra nhiều phương thức thanh to&aacute;n để qu&yacute; kh&aacute;ch dễ d&agrave;ng lựa chọn:</p>\r\n\r\n<ul>\r\n	<li>Thanh to&aacute;n khi nhận h&agrave;ng (COD)</li>\r\n	<li>Thanh to&aacute;n qua thẻ t&iacute;n dụng/thẻ ghi nợ</li>\r\n	<li>Thanh to&aacute;n qua thẻ ATM nội địa</li>\r\n	<li>Thanh to&aacute;n trả g&oacute;p qua thẻ t&iacute;n dụng</li>\r\n</ul>\r\n', 'default.png', '2016-09-28 00:00:00', 'Ngô Trung Phát', '2016-09-28 17:29:38', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(143, 92, 'MUA PHỤ KIỆN THEO COMBO GIẢM ĐẾN 30%', 'mua-phu-kien-theo-combo-giam-den-30', '', '<p>TheShop.com mang đến cho người d&ugrave;ng g&oacute;i ưu đ&atilde;i cực lớn khi mua Combo phụ kiện đầy tiện &iacute;ch cho thiết bị điện tử của m&igrave;nh, xem chi tiết chương tr&igrave;nh dưới đ&acirc;y v&agrave; nhanh ch&acirc;n sắm cho m&igrave;nh c&ugrave;ng bạn b&egrave;, người th&acirc;n những m&oacute;n qu&agrave; phụ kiện n&agrave;y nh&eacute;.</p>\r\n\r\n<p>Thể lệ chương tr&igrave;nh.</p>\r\n\r\n<p>- Mua 1 sản phẩm: kh&ocirc;ng &aacute;p dụng mức giảm.</p>\r\n\r\n<p>- Mua 2 sản phẩm: Giảm 20% m&oacute;n thứ 2.</p>\r\n\r\n<p>- Mua từ 3, 4, 5 sản phẩm: Giảm 30% m&oacute;n thứ 3, 4, 5.</p>\r\n\r\n<p>- &Aacute;p dụng chung 1 đơn h&agrave;ng.</p>\r\n', 'tgdd-le-hoi-pk-800-30011.jpg', '2016-09-28 20:11:40', 'Ngô Trung Phát', '2016-09-28 20:11:40', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(144, 92, 'GIẢM 100.000Đ KHI ĐẶT MUA ONLINE TAI NGHE BLUETOOTH PLANTRONICS ML15', 'giam-100-000d-khi-dat-mua-online-tai-nghe-bluetooth-plantronics-ml15', '', '<p>Thiết kế nhỏ gọn đơn giản c&ugrave;ng nhiều tiện &iacute;ch l&agrave; những g&igrave; tai nghe bluetooth Plantronics ML15 mang đến cho bạn, ngay từ b&acirc;y giờ h&atilde;y nhanh ch&oacute;ng đặt h&agrave;ng online ngay để được giảm gi&aacute; trực tiếp l&ecirc;n đến 100.000đ.</p>\r\n\r\n<p>Giảm ngay 100.000 khi đặt mua Online tai nghe Bluetooth</p>\r\n\r\n<p>Những tiện &iacute;ch của tai nghe:<br />\r\n- C&oacute; thể kết nối 2 thiết bị c&ugrave;ng l&uacute;c.</p>\r\n\r\n<p>- Khoảng c&aacute;ch truyền t&iacute;n hiệu l&ecirc;n đến 10 m.</p>\r\n\r\n<p>- Kết nối nhanh ch&oacute;ng với thiết bị.</p>\r\n\r\n<p>- Thời gian thoại l&ecirc;n đến 6 giờ.</p>\r\n\r\n<p>- Lưu &yacute;: chỉ hỗ trợ nghe gọi, kh&ocirc;ng hỗ trợ nghe nhạc.</p>\r\n\r\n<p>Thời gian diễn ra chương tr&igrave;nh: từ 1/2/2016 đến 15/2/2016<br />\r\nGiảm gi&aacute; ưu đ&atilde;i đ&oacute;n tết:<br />\r\n- Khi bạn đặt mua online tai nghe bluetooth Plantronics ML15 sẽ được giảm ngay 100.000đ, gi&aacute; c&ograve;n lại l&agrave; 590.000đ.</p>\r\n\r\n<p>- Đặt mua bằng hai c&aacute;ch, gọi đến tổng đ&agrave;i 1800.1060 (miễn ph&iacute;), hoặc đặt h&agrave;ng trực tiếp tr&ecirc;n website TheShop.com.</p>\r\n', 'tai-nghe-bluetooth11.jpg', '2016-09-28 20:12:30', 'Ngô Trung Phát', '2016-09-28 20:12:30', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(145, 92, 'NGẬP TRÀN ƯU ĐÃI KHAI TRƯƠNG SIÊU THỊ', 'ngap-tran-uu-dai-khai-truong-sieu-thi', '', '<p>L&agrave; chuỗi b&aacute;n lẻ thiết bị di động lớn nhất hiện nay, TheShop.com lu&ocirc;n nỗ lực từng ng&agrave;y để mang văn h&oacute;a phục vụ, sự h&agrave;i l&ograve;ng v&agrave; thuận tiện kh&ocirc;ng ngừng vươn xa tới khắp nơi tr&ecirc;n to&agrave;n l&atilde;nh thổ Việt Nam.</p>\r\n\r\n<p>Ng&agrave;y 03/02/2016 Thế giới di động đ&aacute;nh dấu sự ra đời si&ecirc;u thị tại huyện Vĩnh Bảo, th&agrave;nh phố Hải Ph&ograve;ng xin ch&agrave;o đ&oacute;n qu&yacute; kh&aacute;ch h&agrave;ng tới tham quan v&agrave; mua sắm tại địa chỉ sau:</p>\r\n\r\n<p>Số 119 - 121 - 123 khu phố 1/5, TT. Vĩnh Bảo, H. Vĩnh Bảo, Tp. Hải Ph&ograve;ng</p>\r\n\r\n<p>Đặc biệt, đến với ch&uacute;ng t&ocirc;i nh&acirc;n dịp khai trương từ ng&agrave;y 03/02/2016 đến hết ng&agrave;y 07/02/2016 qu&yacute; kh&aacute;ch h&agrave;ng sẽ được những ưu đ&atilde;i cực kỳ hấp dẫn:</p>\r\n\r\n<p>Điện thoại 2 sim chỉ 190.000đ<br />\r\nSmartphone Android gi&aacute; chỉ 890.000đ<br />\r\nPhụ kiện tiện &iacute;ch cho mượn dung thử 30 ng&agrave;y<br />\r\nThẻ c&agrave;o 100.000đ gi&aacute; chỉ 90.000đ (1 thẻ/1 kh&aacute;ch h&agrave;ng)<br />\r\nH&agrave;ng ng&agrave;n ốp lưng điện thoại gi&aacute; chỉ từ 50.000đ<br />\r\nKh&ocirc;ng chỉ thế, hiện nay, tại TheShop.com, bạn sẽ dễ d&agrave;ng mua trả g&oacute;p với thủ tục nhanh ch&oacute;ng đơn giản. C&ugrave;ng với đ&oacute; l&agrave; ch&iacute;nh s&aacute;ch trải nghiệm v&agrave; đổi trả đem lại tiện &iacute;ch tối đa cho kh&aacute;ch h&agrave;ng như:</p>\r\n\r\n<p>100% sản phẩm c&ocirc;ng nghệ được tự do trải nghiệm, vọc thử thoải m&aacute;i; phụ kiện được đổi trả miễn ph&iacute; trong v&ograve;ng 30 ng&agrave;y, nếu kh&ocirc;ng h&agrave;i l&ograve;ng lấy lại tiền, sản phẩm sẽ được được bảo h&agrave;nh 1 đổi 1 trong v&ograve;ng 1 năm.<br />\r\nC&aacute;c mặt h&agrave;ng điện thoại, laptop được 14 ng&agrave;y đổi h&agrave;ng lỗi miễn ph&iacute; v&agrave; chỉ mất 10% ph&iacute; đổi trả nếu sản phẩm kh&ocirc;ng lỗi.<br />\r\nBạn cũng c&oacute; thể mua sắm online thật tiện lợi qua website www.TheShop.com hoặc tổng đ&agrave;i miễn ph&iacute; 18001060.<br />\r\nV&agrave; v&ocirc; số ưu đ&atilde;i kh&aacute;c đang chờ đ&oacute;n tại c&aacute;c si&ecirc;u thị TheShop.com.<br />\r\nNhanh ch&acirc;n đến c&aacute;c si&ecirc;u thị TheShop.com mới để tận hưởng ưu đ&atilde;i ng&agrave;y khai trương.</p>\r\n\r\n<p>Th&ocirc;ng tin địa chỉ v&agrave; bản đồ cụ thể của si&ecirc;u thị TheShop.com mới ngay dưới đ&acirc;y bạn nh&eacute;:</p>\r\n\r\n<p>Số 119 - 121 - 123 khu phố 1/5, TT. Vĩnh Bảo, H. Vĩnh Bảo, Tp. Hải Ph&ograve;ng</p>\r\n', '20160105-homecate-dl-bigbang1.png', '2016-09-28 20:13:29', 'Ngô Trung Phát', '2016-09-28 20:13:29', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(146, 92, 'GIẢM NGAY 10% PIN SẠC DỰ PHÒNG ECO KHI ĐẶT MUA ONLINE', 'giam-ngay-10-pin-sac-du-phong-eco-khi-dat-mua-online', '', '<p>Cả hai loại pin sạc dự ph&ograve;ng 7500 mAh v&agrave; 10.000 mAh khi bạn đặt mua Online sẽ được giảm trực tiếp 10%, c&ograve;n chần chờ g&igrave; nữa m&agrave; kh&ocirc;ng sắm ngay cho m&igrave;nh một m&oacute;n phụ kiện nhiều tiện &iacute;ch cho những chuyến du xu&acirc;n d&agrave;i ng&agrave;y sắp tới.</p>\r\n\r\n<p>Giảm gi&aacute; pin sạc dự ph&ograve;ng ECO khi đặt mua Online</p>\r\n\r\n<p>Tiện &iacute;ch của pin sạc dự ph&ograve;ng ECO:<br />\r\n- Hai mức dung lượng 7500 mAh v&agrave; 10.000 mAh cho bạn lựa chọn.</p>\r\n\r\n<p>- Thiết kế với m&agrave;u sắc đơn giản v&agrave; tươi vui.</p>\r\n\r\n<p>- Trang bị 2 đầu sạc 1A v&agrave; 2.1A để sạc cho điện thoại hay m&aacute;y t&iacute;nh bảng...</p>\r\n\r\n<p>- Sử dụng l&otilde;i pin Iion an to&agrave;n cho bạn sử dụng.</p>\r\n\r\n<p>- T&iacute;ch hợp đ&egrave;n LED b&aacute;o trạng th&aacute;i dung lượng của pin.</p>\r\n\r\n<p>- Sản phẩm được bảo h&agrave;nh 6 th&aacute;ng 1 đổi 1 khi sử dụng bị lỗi</p>\r\n\r\n<p>Ưu đ&atilde;i khi mua Online:<br />\r\n- Từ ng&agrave;y 1/2 đến 29/2, khi bạn đặt mua pin sạc dự ph&ograve;ng qua tổng đ&agrave;i 1800.1060 (miễn ph&iacute;) hoặc qua website TheShop.com, bạn sẽ được giảm 10% trực tiếp v&agrave;o trong gi&aacute; tiền của sản phẩm.</p>\r\n', 'giam-gia-pin-sac-du-phong-eco-khi-dat-mua-online1.jpg', '2016-09-28 20:14:55', 'Ngô Trung Phát', '2016-09-28 20:14:55', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(147, 92, 'MUA HÀNG ONLINE NGÀY TẾT BÍNH THÂN GIẢM GIÁ HẤP DẪN', 'mua-hang-online-ngay-tet-binh-than-giam-gia-hap-dan', '', '<p>TheShop.com k&iacute;nh ch&uacute;c qu&yacute; kh&aacute;ch năm mới An Khang Thịnh Vượng - Vạn Sự Như &Yacute;. Để thuận tiện cho việc mua sắm của qu&yacute; kh&aacute;ch h&agrave;ng, TheShop.com xin gửi đến qu&yacute; kh&aacute;ch lịch phục vụ tết như sau:</p>\r\n\r\n<p>1. Hệ thống tổng đ&agrave;i, website:</p>\r\n\r\n<p>Tổng đ&agrave;i 1800.1060 của TheShop.com sẽ phục vụ đến 22 giờ ng&agrave;y 28 Tết (06/02/2016) v&agrave; hoạt động b&igrave;nh thường từ 7 giờ 30 ph&uacute;t ng&agrave;y m&ugrave;ng 04 Tết (11/02/2016).</p>\r\n\r\n<p>Đặc biệt trong dịp Tết B&iacute;nh Th&acirc;n (từ 29 Tết đến hết m&ugrave;ng 03 Tết), qu&yacute; kh&aacute;ch h&agrave;ng vẫn c&oacute; thể đặt mua h&agrave;ng online tại website www.thegioididong.com với chương tr&igrave;nh giảm gi&aacute; hấp dẫn:</p>\r\n\r\n<p>+ Giảm 10% tất cả Điện thoại, Laptop, M&aacute;y t&iacute;nh bảng.</p>\r\n\r\n<p>+ Giảm 5% cho c&aacute;c sản phẩm của Apple (iPhone, iPad).</p>\r\n\r\n<p>Tất cả c&aacute;c đơn h&agrave;ng sẽ được xử l&yacute; v&agrave; giao h&agrave;ng sau 8 giờ s&aacute;ng ng&agrave;y m&ugrave;ng 04 Tết.</p>\r\n\r\n<p>2. Hệ thống c&aacute;c si&ecirc;u thị:</p>\r\n\r\n<p>Phục vụ đến hết ng&agrave;y 28 Tết (nhằm 06/02/2016) v&agrave; mở cửa phục vụ b&igrave;nh thường từ ng&agrave;y m&ugrave;ng 04 Tết (nhằm ng&agrave;y 11/02/2016).</p>\r\n\r\n<p>Ri&ecirc;ng tại H&agrave; Nội v&agrave; Hải Ph&ograve;ng TheShop.com sẽ phục vụ đến 12 giờ trưa ng&agrave;y 29 Tết (nhằm 07/02/2016) tại 1 số si&ecirc;u thị:</p>\r\n\r\n<p>11A Th&aacute;i H&agrave;, P.Trung Liệt, Q.Đống Đa, H&agrave; Nội</p>\r\n\r\n<p>Số 24 - 26, Phố L&yacute; Quốc Sư, Phường H&agrave;ng Trống, Quận Ho&agrave;n Kiếm, Tp. H&agrave; Nội</p>\r\n\r\n<p>351 Cầu Giấy, P. Dịch Vọng, Q. Cầu Giấy, H&agrave; Nội</p>\r\n\r\n<p>403 Quang Trung, P.Quang Trung, Q.H&agrave; Đ&ocirc;ng, H&agrave; Nội</p>\r\n\r\n<p>Th&ocirc;n Phan X&aacute;, x&atilde; Uy Nỗ, H. Đ&ocirc;ng Anh, Tp. H&agrave; Nội</p>\r\n\r\n<p>221A-221 Lạch Tray, P.Đổng Quốc B&igrave;nh, Q.Ng&ocirc; Quyền, TP.Hải Ph&ograve;ng</p>\r\n\r\n<p>Tr&acirc;n Trọng!</p>\r\n', 'khuyen-mai-tet1.jpg', '2016-09-28 20:15:39', 'Ngô Trung Phát', '2016-09-28 20:15:39', 'Ngô Trung Phát', 1, 1, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `db_customer`
--

CREATE TABLE IF NOT EXISTS `db_customer` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_customer`
--

INSERT INTO `db_customer` (`id`, `fullname`, `username`, `password`, `gender`, `birthday`, `address`, `phone`, `email`, `created`, `trash`, `access`, `status`) VALUES
(1, 'Ngô Trung Phát', 'trungphatit', 'e10adc3949ba59abbe56e057f20f883e', 0, '1996-12-07', '443/15 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức, thành phố Hồ Chí Minh', '01654292452', 'ngotrungphat@gmail.com', '2016-10-07 18:30:00', 1, 1, 1),
(15, 'Trung Phong', 'ngotrungphong', 'e10adc3949ba59abbe56e057f20f883e', 1, '1996-12-07', '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', '01654292454', 'trungphong3913@gmail.com', '0000-00-00 00:00:00', 1, 1, 1),
(16, 'Đinh Văn Nam', 'Dinhvannam', 'e10adc3949ba59abbe56e057f20f883e', 0, '0000-00-00', '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', '01654292454', 'dinhvannam@gmail.com', '0000-00-00 00:00:00', 1, 1, 1),
(17, 'Nguyễn Văn ABC', 'nguyenvanabc', 'e10adc3949ba59abbe56e057f20f883e', 1, '0000-00-00', '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', '01654292453', 'nguyenvanabc@gmail.com', '0000-00-00 00:00:00', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `db_district`
--

CREATE TABLE IF NOT EXISTS `db_district` (
  `districtid` varchar(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `provinceid` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_district`
--

INSERT INTO `db_district` (`districtid`, `name`, `type`, `provinceid`) VALUES
('001', 'Ba Đình', 'Quận', '01'),
('002', 'Hoàn Kiếm', 'Quận', '01'),
('003', 'Tây Hồ', 'Quận', '01'),
('004', 'Long Biên', 'Quận', '01'),
('005', 'Cầu Giấy', 'Quận', '01'),
('006', 'Đống Đa', 'Quận', '01'),
('007', 'Hai Bà Trưng', 'Quận', '01'),
('008', 'Hoàng Mai', 'Quận', '01'),
('009', 'Thanh Xuân', 'Quận', '01'),
('016', 'Sóc Sơn', 'Huyện', '01'),
('017', 'Đông Anh', 'Huyện', '01'),
('018', 'Gia Lâm', 'Huyện', '01'),
('019', 'Từ Liêm', 'Huyện', '01'),
('020', 'Thanh Trì', 'Huyện', '01'),
('024', 'Hà Giang', 'Thị Xã', '02'),
('026', 'Đồng Văn', 'Huyện', '02'),
('027', 'Mèo Vạc', 'Huyện', '02'),
('028', 'Yên Minh', 'Huyện', '02'),
('029', 'Quản Bạ', 'Huyện', '02'),
('030', 'Vị Xuyên', 'Huyện', '02'),
('031', 'Bắc Mê', 'Huyện', '02'),
('032', 'Hoàng Su Phì', 'Huyện', '02'),
('033', 'Xín Mần', 'Huyện', '02'),
('034', 'Bắc Quang', 'Huyện', '02'),
('035', 'Quang Bình', 'Huyện', '02'),
('040', 'Cao Bằng', 'Thị Xã', '04'),
('042', 'Bảo Lâm', 'Huyện', '04'),
('043', 'Bảo Lạc', 'Huyện', '04'),
('044', 'Thông Nông', 'Huyện', '04'),
('045', 'Hà Quảng', 'Huyện', '04'),
('046', 'Trà Lĩnh', 'Huyện', '04'),
('047', 'Trùng Khánh', 'Huyện', '04'),
('048', 'Hạ Lang', 'Huyện', '04'),
('049', 'Quảng Uyên', 'Huyện', '04'),
('050', 'Phục Hoà', 'Huyện', '04'),
('051', 'Hoà An', 'Huyện', '04'),
('052', 'Nguyên Bình', 'Huyện', '04'),
('053', 'Thạch An', 'Huyện', '04'),
('058', 'Bắc Kạn', 'Thị Xã', '06'),
('060', 'Pác Nặm', 'Huyện', '06'),
('061', 'Ba Bể', 'Huyện', '06'),
('062', 'Ngân Sơn', 'Huyện', '06'),
('063', 'Bạch Thông', 'Huyện', '06'),
('064', 'Chợ Đồn', 'Huyện', '06'),
('065', 'Chợ Mới', 'Huyện', '06'),
('066', 'Na Rì', 'Huyện', '06'),
('070', 'Tuyên Quang', 'Thị Xã', '08'),
('072', 'Nà Hang', 'Huyện', '08'),
('073', 'Chiêm Hóa', 'Huyện', '08'),
('074', 'Hàm Yên', 'Huyện', '08'),
('075', 'Yên Sơn', 'Huyện', '08'),
('076', 'Sơn Dương', 'Huyện', '08'),
('080', 'Lào Cai', 'Thành Phố', '10'),
('082', 'Bát Xát', 'Huyện', '10'),
('083', 'Mường Khương', 'Huyện', '10'),
('084', 'Si Ma Cai', 'Huyện', '10'),
('085', 'Bắc Hà', 'Huyện', '10'),
('086', 'Bảo Thắng', 'Huyện', '10'),
('087', 'Bảo Yên', 'Huyện', '10'),
('088', 'Sa Pa', 'Huyện', '10'),
('089', 'Văn Bàn', 'Huyện', '10'),
('094', 'Điện Biên Phủ', 'Thành Phố', '11'),
('095', 'Mường Lay', 'Thị Xã', '11'),
('096', 'Mường Nhé', 'Huyện', '11'),
('097', 'Mường Chà', 'Huyện', '11'),
('098', 'Tủa Chùa', 'Huyện', '11'),
('099', 'Tuần Giáo', 'Huyện', '11'),
('100', 'Điện Biên', 'Huyện', '11'),
('101', 'Điện Biên Đông', 'Huyện', '11'),
('102', 'Mường Ảng', 'Huyện', '11'),
('104', 'Lai Châu', 'Thị Xã', '12'),
('106', 'Tam Đường', 'Huyện', '12'),
('107', 'Mường Tè', 'Huyện', '12'),
('108', 'Sìn Hồ', 'Huyện', '12'),
('109', 'Phong Thổ', 'Huyện', '12'),
('110', 'Than Uyên', 'Huyện', '12'),
('111', 'Tân Uyên', 'Huyện', '12'),
('116', 'Sơn La', 'Thành Phố', '14'),
('118', 'Quỳnh Nhai', 'Huyện', '14'),
('119', 'Thuận Châu', 'Huyện', '14'),
('120', 'Mường La', 'Huyện', '14'),
('121', 'Bắc Yên', 'Huyện', '14'),
('122', 'Phù Yên', 'Huyện', '14'),
('123', 'Mộc Châu', 'Huyện', '14'),
('124', 'Yên Châu', 'Huyện', '14'),
('125', 'Mai Sơn', 'Huyện', '14'),
('126', 'Sông Mã', 'Huyện', '14'),
('127', 'Sốp Cộp', 'Huyện', '14'),
('132', 'Yên Bái', 'Thành Phố', '15'),
('133', 'Nghĩa Lộ', 'Thị Xã', '15'),
('135', 'Lục Yên', 'Huyện', '15'),
('136', 'Văn Yên', 'Huyện', '15'),
('137', 'Mù Cang Chải', 'Huyện', '15'),
('138', 'Trấn Yên', 'Huyện', '15'),
('139', 'Trạm Tấu', 'Huyện', '15'),
('140', 'Văn Chấn', 'Huyện', '15'),
('141', 'Yên Bình', 'Huyện', '15'),
('148', 'Hòa Bình', 'Thành Phố', '17'),
('150', 'Đà Bắc', 'Huyện', '17'),
('151', 'Kỳ Sơn', 'Huyện', '17'),
('152', 'Lương Sơn', 'Huyện', '17'),
('153', 'Kim Bôi', 'Huyện', '17'),
('154', 'Cao Phong', 'Huyện', '17'),
('155', 'Tân Lạc', 'Huyện', '17'),
('156', 'Mai Châu', 'Huyện', '17'),
('157', 'Lạc Sơn', 'Huyện', '17'),
('158', 'Yên Thủy', 'Huyện', '17'),
('159', 'Lạc Thủy', 'Huyện', '17'),
('164', 'Thái Nguyên', 'Thành Phố', '19'),
('165', 'Sông Công', 'Thị Xã', '19'),
('167', 'Định Hóa', 'Huyện', '19'),
('168', 'Phú Lương', 'Huyện', '19'),
('169', 'Đồng Hỷ', 'Huyện', '19'),
('170', 'Võ Nhai', 'Huyện', '19'),
('171', 'Đại Từ', 'Huyện', '19'),
('172', 'Phổ Yên', 'Huyện', '19'),
('173', 'Phú Bình', 'Huyện', '19'),
('178', 'Lạng Sơn', 'Thành Phố', '20'),
('180', 'Tràng Định', 'Huyện', '20'),
('181', 'Bình Gia', 'Huyện', '20'),
('182', 'Văn Lãng', 'Huyện', '20'),
('183', 'Cao Lộc', 'Huyện', '20'),
('184', 'Văn Quan', 'Huyện', '20'),
('185', 'Bắc Sơn', 'Huyện', '20'),
('186', 'Hữu Lũng', 'Huyện', '20'),
('187', 'Chi Lăng', 'Huyện', '20'),
('188', 'Lộc Bình', 'Huyện', '20'),
('189', 'Đình Lập', 'Huyện', '20'),
('193', 'Hạ Long', 'Thành Phố', '22'),
('194', 'Móng Cái', 'Thành Phố', '22'),
('195', 'Cẩm Phả', 'Thị Xã', '22'),
('196', 'Uông Bí', 'Thị Xã', '22'),
('198', 'Bình Liêu', 'Huyện', '22'),
('199', 'Tiên Yên', 'Huyện', '22'),
('200', 'Đầm Hà', 'Huyện', '22'),
('201', 'Hải Hà', 'Huyện', '22'),
('202', 'Ba Chẽ', 'Huyện', '22'),
('203', 'Vân Đồn', 'Huyện', '22'),
('204', 'Hoành Bồ', 'Huyện', '22'),
('205', 'Đông Triều', 'Huyện', '22'),
('206', 'Yên Hưng', 'Huyện', '22'),
('207', 'Cô Tô', 'Huyện', '22'),
('213', 'Bắc Giang', 'Thành Phố', '24'),
('215', 'Yên Thế', 'Huyện', '24'),
('216', 'Tân Yên', 'Huyện', '24'),
('217', 'Lạng Giang', 'Huyện', '24'),
('218', 'Lục Nam', 'Huyện', '24'),
('219', 'Lục Ngạn', 'Huyện', '24'),
('220', 'Sơn Động', 'Huyện', '24'),
('221', 'Yên Dũng', 'Huyện', '24'),
('222', 'Việt Yên', 'Huyện', '24'),
('223', 'Hiệp Hòa', 'Huyện', '24'),
('227', 'Việt Trì', 'Thành Phố', '25'),
('228', 'Phú Thọ', 'Thị Xã', '25'),
('230', 'Đoan Hùng', 'Huyện', '25'),
('231', 'Hạ Hoà', 'Huyện', '25'),
('232', 'Thanh Ba', 'Huyện', '25'),
('233', 'Phù Ninh', 'Huyện', '25'),
('234', 'Yên Lập', 'Huyện', '25'),
('235', 'Cẩm Khê', 'Huyện', '25'),
('236', 'Tam Nông', 'Huyện', '25'),
('237', 'Lâm Thao', 'Huyện', '25'),
('238', 'Thanh Sơn', 'Huyện', '25'),
('239', 'Thanh Thuỷ', 'Huyện', '25'),
('240', 'Tân Sơn', 'Huyện', '25'),
('243', 'Vĩnh Yên', 'Thành Phố', '26'),
('244', 'Phúc Yên', 'Thị Xã', '26'),
('246', 'Lập Thạch', 'Huyện', '26'),
('247', 'Tam Dương', 'Huyện', '26'),
('248', 'Tam Đảo', 'Huyện', '26'),
('249', 'Bình Xuyên', 'Huyện', '26'),
('250', 'Mê Linh', 'Huyện', '01'),
('251', 'Yên Lạc', 'Huyện', '26'),
('252', 'Vĩnh Tường', 'Huyện', '26'),
('253', 'Sông Lô', 'Huyện', '26'),
('256', 'Bắc Ninh', 'Thành Phố', '27'),
('258', 'Yên Phong', 'Huyện', '27'),
('259', 'Quế Võ', 'Huyện', '27'),
('260', 'Tiên Du', 'Huyện', '27'),
('261', 'Từ Sơn', 'Thị Xã', '27'),
('262', 'Thuận Thành', 'Huyện', '27'),
('263', 'Gia Bình', 'Huyện', '27'),
('264', 'Lương Tài', 'Huyện', '27'),
('268', 'Hà Đông', 'Quận', '01'),
('269', 'Sơn Tây', 'Thị Xã', '01'),
('271', 'Ba Vì', 'Huyện', '01'),
('272', 'Phúc Thọ', 'Huyện', '01'),
('273', 'Đan Phượng', 'Huyện', '01'),
('274', 'Hoài Đức', 'Huyện', '01'),
('275', 'Quốc Oai', 'Huyện', '01'),
('276', 'Thạch Thất', 'Huyện', '01'),
('277', 'Chương Mỹ', 'Huyện', '01'),
('278', 'Thanh Oai', 'Huyện', '01'),
('279', 'Thường Tín', 'Huyện', '01'),
('280', 'Phú Xuyên', 'Huyện', '01'),
('281', 'Ứng Hòa', 'Huyện', '01'),
('282', 'Mỹ Đức', 'Huyện', '01'),
('288', 'Hải Dương', 'Thành Phố', '30'),
('290', 'Chí Linh', 'Huyện', '30'),
('291', 'Nam Sách', 'Huyện', '30'),
('292', 'Kinh Môn', 'Huyện', '30'),
('293', 'Kim Thành', 'Huyện', '30'),
('294', 'Thanh Hà', 'Huyện', '30'),
('295', 'Cẩm Giàng', 'Huyện', '30'),
('296', 'Bình Giang', 'Huyện', '30'),
('297', 'Gia Lộc', 'Huyện', '30'),
('298', 'Tứ Kỳ', 'Huyện', '30'),
('299', 'Ninh Giang', 'Huyện', '30'),
('300', 'Thanh Miện', 'Huyện', '30'),
('303', 'Hồng Bàng', 'Quận', '31'),
('304', 'Ngô Quyền', 'Quận', '31'),
('305', 'Lê Chân', 'Quận', '31'),
('306', 'Hải An', 'Quận', '31'),
('307', 'Kiến An', 'Quận', '31'),
('308', 'Đồ Sơn', 'Quận', '31'),
('309', 'Kinh Dương', 'Quận', '31'),
('311', 'Thuỷ Nguyên', 'Huyện', '31'),
('312', 'An Dương', 'Huyện', '31'),
('313', 'An Lão', 'Huyện', '31'),
('314', 'Kiến Thụy', 'Huyện', '31'),
('315', 'Tiên Lãng', 'Huyện', '31'),
('316', 'Vĩnh Bảo', 'Huyện', '31'),
('317', 'Cát Hải', 'Huyện', '31'),
('318', 'Bạch Long Vĩ', 'Huyện', '31'),
('323', 'Hưng Yên', 'Thành Phố', '33'),
('325', 'Văn Lâm', 'Huyện', '33'),
('326', 'Văn Giang', 'Huyện', '33'),
('327', 'Yên Mỹ', 'Huyện', '33'),
('328', 'Mỹ Hào', 'Huyện', '33'),
('329', 'Ân Thi', 'Huyện', '33'),
('330', 'Khoái Châu', 'Huyện', '33'),
('331', 'Kim Động', 'Huyện', '33'),
('332', 'Tiên Lữ', 'Huyện', '33'),
('333', 'Phù Cừ', 'Huyện', '33'),
('336', 'Thái Bình', 'Thành Phố', '34'),
('338', 'Quỳnh Phụ', 'Huyện', '34'),
('339', 'Hưng Hà', 'Huyện', '34'),
('340', 'Đông Hưng', 'Huyện', '34'),
('341', 'Thái Thụy', 'Huyện', '34'),
('342', 'Tiền Hải', 'Huyện', '34'),
('343', 'Kiến Xương', 'Huyện', '34'),
('344', 'Vũ Thư', 'Huyện', '34'),
('347', 'Phủ Lý', 'Thành Phố', '35'),
('349', 'Duy Tiên', 'Huyện', '35'),
('350', 'Kim Bảng', 'Huyện', '35'),
('351', 'Thanh Liêm', 'Huyện', '35'),
('352', 'Bình Lục', 'Huyện', '35'),
('353', 'Lý Nhân', 'Huyện', '35'),
('356', 'Nam Định', 'Thành Phố', '36'),
('358', 'Mỹ Lộc', 'Huyện', '36'),
('359', 'Vụ Bản', 'Huyện', '36'),
('360', 'Ý Yên', 'Huyện', '36'),
('361', 'Nghĩa Hưng', 'Huyện', '36'),
('362', 'Nam Trực', 'Huyện', '36'),
('363', 'Trực Ninh', 'Huyện', '36'),
('364', 'Xuân Trường', 'Huyện', '36'),
('365', 'Giao Thủy', 'Huyện', '36'),
('366', 'Hải Hậu', 'Huyện', '36'),
('369', 'Ninh Bình', 'Thành Phố', '37'),
('370', 'Tam Điệp', 'Thị Xã', '37'),
('372', 'Nho Quan', 'Huyện', '37'),
('373', 'Gia Viễn', 'Huyện', '37'),
('374', 'Hoa Lư', 'Huyện', '37'),
('375', 'Yên Khánh', 'Huyện', '37'),
('376', 'Kim Sơn', 'Huyện', '37'),
('377', 'Yên Mô', 'Huyện', '37'),
('380', 'Thanh Hóa', 'Thành Phố', '38'),
('381', 'Bỉm Sơn', 'Thị Xã', '38'),
('382', 'Sầm Sơn', 'Thị Xã', '38'),
('384', 'Mường Lát', 'Huyện', '38'),
('385', 'Quan Hóa', 'Huyện', '38'),
('386', 'Bá Thước', 'Huyện', '38'),
('387', 'Quan Sơn', 'Huyện', '38'),
('388', 'Lang Chánh', 'Huyện', '38'),
('389', 'Ngọc Lặc', 'Huyện', '38'),
('390', 'Cẩm Thủy', 'Huyện', '38'),
('391', 'Thạch Thành', 'Huyện', '38'),
('392', 'Hà Trung', 'Huyện', '38'),
('393', 'Vĩnh Lộc', 'Huyện', '38'),
('394', 'Yên Định', 'Huyện', '38'),
('395', 'Thọ Xuân', 'Huyện', '38'),
('396', 'Thường Xuân', 'Huyện', '38'),
('397', 'Triệu Sơn', 'Huyện', '38'),
('398', 'Thiệu Hoá', 'Huyện', '38'),
('399', 'Hoằng Hóa', 'Huyện', '38'),
('400', 'Hậu Lộc', 'Huyện', '38'),
('401', 'Nga Sơn', 'Huyện', '38'),
('402', 'Như Xuân', 'Huyện', '38'),
('403', 'Như Thanh', 'Huyện', '38'),
('404', 'Nông Cống', 'Huyện', '38'),
('405', 'Đông Sơn', 'Huyện', '38'),
('406', 'Quảng Xương', 'Huyện', '38'),
('407', 'Tĩnh Gia', 'Huyện', '38'),
('412', 'Vinh', 'Thành Phố', '40'),
('413', 'Cửa Lò', 'Thị Xã', '40'),
('414', 'Thái Hoà', 'Thị Xã', '40'),
('415', 'Quế Phong', 'Huyện', '40'),
('416', 'Quỳ Châu', 'Huyện', '40'),
('417', 'Kỳ Sơn', 'Huyện', '40'),
('418', 'Tương Dương', 'Huyện', '40'),
('419', 'Nghĩa Đàn', 'Huyện', '40'),
('420', 'Quỳ Hợp', 'Huyện', '40'),
('421', 'Quỳnh Lưu', 'Huyện', '40'),
('422', 'Con Cuông', 'Huyện', '40'),
('423', 'Tân Kỳ', 'Huyện', '40'),
('424', 'Anh Sơn', 'Huyện', '40'),
('425', 'Diễn Châu', 'Huyện', '40'),
('426', 'Yên Thành', 'Huyện', '40'),
('427', 'Đô Lương', 'Huyện', '40'),
('428', 'Thanh Chương', 'Huyện', '40'),
('429', 'Nghi Lộc', 'Huyện', '40'),
('430', 'Nam Đàn', 'Huyện', '40'),
('431', 'Hưng Nguyên', 'Huyện', '40'),
('436', 'Hà Tĩnh', 'Thành Phố', '42'),
('437', 'Hồng Lĩnh', 'Thị Xã', '42'),
('439', 'Hương Sơn', 'Huyện', '42'),
('440', 'Đức Thọ', 'Huyện', '42'),
('441', 'Vũ Quang', 'Huyện', '42'),
('442', 'Nghi Xuân', 'Huyện', '42'),
('443', 'Can Lộc', 'Huyện', '42'),
('444', 'Hương Khê', 'Huyện', '42'),
('445', 'Thạch Hà', 'Huyện', '42'),
('446', 'Cẩm Xuyên', 'Huyện', '42'),
('447', 'Kỳ Anh', 'Huyện', '42'),
('448', 'Lộc Hà', 'Huyện', '42'),
('450', 'Đồng Hới', 'Thành Phố', '44'),
('452', 'Minh Hóa', 'Huyện', '44'),
('453', 'Tuyên Hóa', 'Huyện', '44'),
('454', 'Quảng Trạch', 'Huyện', '44'),
('455', 'Bố Trạch', 'Huyện', '44'),
('456', 'Quảng Ninh', 'Huyện', '44'),
('457', 'Lệ Thủy', 'Huyện', '44'),
('461', 'Đông Hà', 'Thành Phố', '45'),
('462', 'Quảng Trị', 'Thị Xã', '45'),
('464', 'Vĩnh Linh', 'Huyện', '45'),
('465', 'Hướng Hóa', 'Huyện', '45'),
('466', 'Gio Linh', 'Huyện', '45'),
('467', 'Đa Krông', 'Huyện', '45'),
('468', 'Cam Lộ', 'Huyện', '45'),
('469', 'Triệu Phong', 'Huyện', '45'),
('470', 'Hải Lăng', 'Huyện', '45'),
('471', 'Cồn Cỏ', 'Huyện', '45'),
('474', 'Huế', 'Thành Phố', '46'),
('476', 'Phong Điền', 'Huyện', '46'),
('477', 'Quảng Điền', 'Huyện', '46'),
('478', 'Phú Vang', 'Huyện', '46'),
('479', 'Hương Thủy', 'Huyện', '46'),
('480', 'Hương Trà', 'Huyện', '46'),
('481', 'A Lưới', 'Huyện', '46'),
('482', 'Phú Lộc', 'Huyện', '46'),
('483', 'Nam Đông', 'Huyện', '46'),
('490', 'Liên Chiểu', 'Quận', '48'),
('491', 'Thanh Khê', 'Quận', '48'),
('492', 'Hải Châu', 'Quận', '48'),
('493', 'Sơn Trà', 'Quận', '48'),
('494', 'Ngũ Hành Sơn', 'Quận', '48'),
('495', 'Cẩm Lệ', 'Quận', '48'),
('497', 'Hoà Vang', 'Huyện', '48'),
('498', 'Hoàng Sa', 'Huyện', '48'),
('502', 'Tam Kỳ', 'Thành Phố', '49'),
('503', 'Hội An', 'Thành Phố', '49'),
('504', 'Tây Giang', 'Huyện', '49'),
('505', 'Đông Giang', 'Huyện', '49'),
('506', 'Đại Lộc', 'Huyện', '49'),
('507', 'Điện Bàn', 'Huyện', '49'),
('508', 'Duy Xuyên', 'Huyện', '49'),
('509', 'Quế Sơn', 'Huyện', '49'),
('510', 'Nam Giang', 'Huyện', '49'),
('511', 'Phước Sơn', 'Huyện', '49'),
('512', 'Hiệp Đức', 'Huyện', '49'),
('513', 'Thăng Bình', 'Huyện', '49'),
('514', 'Tiên Phước', 'Huyện', '49'),
('515', 'Bắc Trà My', 'Huyện', '49'),
('516', 'Nam Trà My', 'Huyện', '49'),
('517', 'Núi Thành', 'Huyện', '49'),
('518', 'Phú Ninh', 'Huyện', '49'),
('519', 'Nông Sơn', 'Huyện', '49'),
('522', 'Quảng Ngãi', 'Thành Phố', '51'),
('524', 'Bình Sơn', 'Huyện', '51'),
('525', 'Trà Bồng', 'Huyện', '51'),
('526', 'Tây Trà', 'Huyện', '51'),
('527', 'Sơn Tịnh', 'Huyện', '51'),
('528', 'Tư Nghĩa', 'Huyện', '51'),
('529', 'Sơn Hà', 'Huyện', '51'),
('530', 'Sơn Tây', 'Huyện', '51'),
('531', 'Minh Long', 'Huyện', '51'),
('532', 'Nghĩa Hành', 'Huyện', '51'),
('533', 'Mộ Đức', 'Huyện', '51'),
('534', 'Đức Phổ', 'Huyện', '51'),
('535', 'Ba Tơ', 'Huyện', '51'),
('536', 'Lý Sơn', 'Huyện', '51'),
('540', 'Qui Nhơn', 'Thành Phố', '52'),
('542', 'An Lão', 'Huyện', '52'),
('543', 'Hoài Nhơn', 'Huyện', '52'),
('544', 'Hoài Ân', 'Huyện', '52'),
('545', 'Phù Mỹ', 'Huyện', '52'),
('546', 'Vĩnh Thạnh', 'Huyện', '52'),
('547', 'Tây Sơn', 'Huyện', '52'),
('548', 'Phù Cát', 'Huyện', '52'),
('549', 'An Nhơn', 'Huyện', '52'),
('550', 'Tuy Phước', 'Huyện', '52'),
('551', 'Vân Canh', 'Huyện', '52'),
('555', 'Tuy Hòa', 'Thành Phố', '54'),
('557', 'Sông Cầu', 'Thị Xã', '54'),
('558', 'Đồng Xuân', 'Huyện', '54'),
('559', 'Tuy An', 'Huyện', '54'),
('560', 'Sơn Hòa', 'Huyện', '54'),
('561', 'Sông Hinh', 'Huyện', '54'),
('562', 'Tây Hoà', 'Huyện', '54'),
('563', 'Phú Hoà', 'Huyện', '54'),
('564', 'Đông Hoà', 'Huyện', '54'),
('568', 'Nha Trang', 'Thành Phố', '56'),
('569', 'Cam Ranh', 'Thị Xã', '56'),
('570', 'Cam Lâm', 'Huyện', '56'),
('571', 'Vạn Ninh', 'Huyện', '56'),
('572', 'Ninh Hòa', 'Huyện', '56'),
('573', 'Khánh Vĩnh', 'Huyện', '56'),
('574', 'Diên Khánh', 'Huyện', '56'),
('575', 'Khánh Sơn', 'Huyện', '56'),
('576', 'Trường Sa', 'Huyện', '56'),
('582', 'Phan Rang-Tháp Chàm', 'Thành Phố', '58'),
('584', 'Bác Ái', 'Huyện', '58'),
('585', 'Ninh Sơn', 'Huyện', '58'),
('586', 'Ninh Hải', 'Huyện', '58'),
('587', 'Ninh Phước', 'Huyện', '58'),
('588', 'Thuận Bắc', 'Huyện', '58'),
('589', 'Thuận Nam', 'Huyện', '58'),
('593', 'Phan Thiết', 'Thành Phố', '60'),
('594', 'La Gi', 'Thị Xã', '60'),
('595', 'Tuy Phong', 'Huyện', '60'),
('596', 'Bắc Bình', 'Huyện', '60'),
('597', 'Hàm Thuận Bắc', 'Huyện', '60'),
('598', 'Hàm Thuận Nam', 'Huyện', '60'),
('599', 'Tánh Linh', 'Huyện', '60'),
('600', 'Đức Linh', 'Huyện', '60'),
('601', 'Hàm Tân', 'Huyện', '60'),
('602', 'Phú Quí', 'Huyện', '60'),
('608', 'Kon Tum', 'Thành Phố', '62'),
('610', 'Đắk Glei', 'Huyện', '62'),
('611', 'Ngọc Hồi', 'Huyện', '62'),
('612', 'Đắk Tô', 'Huyện', '62'),
('613', 'Kon Plông', 'Huyện', '62'),
('614', 'Kon Rẫy', 'Huyện', '62'),
('615', 'Đắk Hà', 'Huyện', '62'),
('616', 'Sa Thầy', 'Huyện', '62'),
('617', 'Tu Mơ Rông', 'Huyện', '62'),
('622', 'Pleiku', 'Thành Phố', '64'),
('623', 'An Khê', 'Thị Xã', '64'),
('624', 'Ayun Pa', 'Thị Xã', '64'),
('625', 'Kbang', 'Huyện', '64'),
('626', 'Đăk Đoa', 'Huyện', '64'),
('627', 'Chư Păh', 'Huyện', '64'),
('628', 'Ia Grai', 'Huyện', '64'),
('629', 'Mang Yang', 'Huyện', '64'),
('630', 'Kông Chro', 'Huyện', '64'),
('631', 'Đức Cơ', 'Huyện', '64'),
('632', 'Chư Prông', 'Huyện', '64'),
('633', 'Chư Sê', 'Huyện', '64'),
('634', 'Đăk Pơ', 'Huyện', '64'),
('635', 'Ia Pa', 'Huyện', '64'),
('637', 'Krông Pa', 'Huyện', '64'),
('638', 'Phú Thiện', 'Huyện', '64'),
('639', 'Chư Pưh', 'Huyện', '64'),
('643', 'Buôn Ma Thuột', 'Thành Phố', '66'),
('644', 'Buôn Hồ', 'Thị Xã', '66'),
('645', 'Ea H''leo', 'Huyện', '66'),
('646', 'Ea Súp', 'Huyện', '66'),
('647', 'Buôn Đôn', 'Huyện', '66'),
('648', 'Cư M''gar', 'Huyện', '66'),
('649', 'Krông Búk', 'Huyện', '66'),
('650', 'Krông Năng', 'Huyện', '66'),
('651', 'Ea Kar', 'Huyện', '66'),
('652', 'M''đrắk', 'Huyện', '66'),
('653', 'Krông Bông', 'Huyện', '66'),
('654', 'Krông Pắc', 'Huyện', '66'),
('655', 'Krông A Na', 'Huyện', '66'),
('656', 'Lắk', 'Huyện', '66'),
('657', 'Cư Kuin', 'Huyện', '66'),
('660', 'Gia Nghĩa', 'Thị Xã', '67'),
('661', 'Đắk Glong', 'Huyện', '67'),
('662', 'Cư Jút', 'Huyện', '67'),
('663', 'Đắk Mil', 'Huyện', '67'),
('664', 'Krông Nô', 'Huyện', '67'),
('665', 'Đắk Song', 'Huyện', '67'),
('666', 'Đắk R''lấp', 'Huyện', '67'),
('667', 'Tuy Đức', 'Huyện', '67'),
('672', 'Đà Lạt', 'Thành Phố', '68'),
('673', 'Bảo Lộc', 'Thị Xã', '68'),
('674', 'Đam Rông', 'Huyện', '68'),
('675', 'Lạc Dương', 'Huyện', '68'),
('676', 'Lâm Hà', 'Huyện', '68'),
('677', 'Đơn Dương', 'Huyện', '68'),
('678', 'Đức Trọng', 'Huyện', '68'),
('679', 'Di Linh', 'Huyện', '68'),
('680', 'Bảo Lâm', 'Huyện', '68'),
('681', 'Đạ Huoai', 'Huyện', '68'),
('682', 'Đạ Tẻh', 'Huyện', '68'),
('683', 'Cát Tiên', 'Huyện', '68'),
('688', 'Phước Long', 'Thị Xã', '70'),
('689', 'Đồng Xoài', 'Thị Xã', '70'),
('690', 'Bình Long', 'Thị Xã', '70'),
('691', 'Bù Gia Mập', 'Huyện', '70'),
('692', 'Lộc Ninh', 'Huyện', '70'),
('693', 'Bù Đốp', 'Huyện', '70'),
('694', 'Hớn Quản', 'Huyện', '70'),
('695', 'Đồng Phù', 'Huyện', '70'),
('696', 'Bù Đăng', 'Huyện', '70'),
('697', 'Chơn Thành', 'Huyện', '70'),
('703', 'Tây Ninh', 'Thị Xã', '72'),
('705', 'Tân Biên', 'Huyện', '72'),
('706', 'Tân Châu', 'Huyện', '72'),
('707', 'Dương Minh Châu', 'Huyện', '72'),
('708', 'Châu Thành', 'Huyện', '72'),
('709', 'Hòa Thành', 'Huyện', '72'),
('710', 'Gò Dầu', 'Huyện', '72'),
('711', 'Bến Cầu', 'Huyện', '72'),
('712', 'Trảng Bàng', 'Huyện', '72'),
('718', 'Thủ Dầu Một', 'Thị Xã', '74'),
('720', 'Dầu Tiếng', 'Huyện', '74'),
('721', 'Bến Cát', 'Huyện', '74'),
('722', 'Phú Giáo', 'Huyện', '74'),
('723', 'Tân Uyên', 'Huyện', '74'),
('724', 'Dĩ An', 'Huyện', '74'),
('725', 'Thuận An', 'Huyện', '74'),
('731', 'Biên Hòa', 'Thành Phố', '75'),
('732', 'Long Khánh', 'Thị Xã', '75'),
('734', 'Tân Phú', 'Huyện', '75'),
('735', 'Vĩnh Cửu', 'Huyện', '75'),
('736', 'Định Quán', 'Huyện', '75'),
('737', 'Trảng Bom', 'Huyện', '75'),
('738', 'Thống Nhất', 'Huyện', '75'),
('739', 'Cẩm Mỹ', 'Huyện', '75'),
('740', 'Long Thành', 'Huyện', '75'),
('741', 'Xuân Lộc', 'Huyện', '75'),
('742', 'Nhơn Trạch', 'Huyện', '75'),
('747', 'Vũng Tầu', 'Thành Phố', '77'),
('748', 'Bà Rịa', 'Thị Xã', '77'),
('750', 'Châu Đức', 'Huyện', '77'),
('751', 'Xuyên Mộc', 'Huyện', '77'),
('752', 'Long Điền', 'Huyện', '77'),
('753', 'Đất Đỏ', 'Huyện', '77'),
('754', 'Tân Thành', 'Huyện', '77'),
('755', 'Côn Đảo', 'Huyện', '77'),
('760', '1', 'Quận', '79'),
('761', '12', 'Quận', '79'),
('762', 'Thủ Đức', 'Quận', '79'),
('763', '9', 'Quận', '79'),
('764', 'Gò Vấp', 'Quận', '79'),
('765', 'Bình Thạnh', 'Quận', '79'),
('766', 'Tân Bình', 'Quận', '79'),
('767', 'Tân Phú', 'Quận', '79'),
('768', 'Phú Nhuận', 'Quận', '79'),
('769', '2', 'Quận', '79'),
('770', '3', 'Quận', '79'),
('771', '10', 'Quận', '79'),
('772', '11', 'Quận', '79'),
('773', '4', 'Quận', '79'),
('774', '5', 'Quận', '79'),
('775', '6', 'Quận', '79'),
('776', '8', 'Quận', '79'),
('777', 'Bình Tân', 'Quận', '79'),
('778', '7', 'Quận', '79'),
('783', 'Củ Chi', 'Huyện', '79'),
('784', 'Hóc Môn', 'Huyện', '79'),
('785', 'Bình Chánh', 'Huyện', '79'),
('786', 'Nhà Bè', 'Huyện', '79'),
('787', 'Cần Giờ', 'Huyện', '79'),
('794', 'Tân An', 'Thành Phố', '80'),
('796', 'Tân Hưng', 'Huyện', '80'),
('797', 'Vĩnh Hưng', 'Huyện', '80'),
('798', 'Mộc Hóa', 'Huyện', '80'),
('799', 'Tân Thạnh', 'Huyện', '80'),
('800', 'Thạnh Hóa', 'Huyện', '80'),
('801', 'Đức Huệ', 'Huyện', '80'),
('802', 'Đức Hòa', 'Huyện', '80'),
('803', 'Bến Lức', 'Huyện', '80'),
('804', 'Thủ Thừa', 'Huyện', '80'),
('805', 'Tân Trụ', 'Huyện', '80'),
('806', 'Cần Đước', 'Huyện', '80'),
('807', 'Cần Giuộc', 'Huyện', '80'),
('808', 'Châu Thành', 'Huyện', '80'),
('815', 'Mỹ Tho', 'Thành Phố', '82'),
('816', 'Gò Công', 'Thị Xã', '82'),
('818', 'Tân Phước', 'Huyện', '82'),
('819', 'Cái Bè', 'Huyện', '82'),
('820', 'Cai Lậy', 'Huyện', '82'),
('821', 'Châu Thành', 'Huyện', '82'),
('822', 'Chợ Gạo', 'Huyện', '82'),
('823', 'Gò Công Tây', 'Huyện', '82'),
('824', 'Gò Công Đông', 'Huyện', '82'),
('825', 'Tân Phú Đông', 'Huyện', '82'),
('829', 'Bến Tre', 'Thành Phố', '83'),
('831', 'Châu Thành', 'Huyện', '83'),
('832', 'Chợ Lách', 'Huyện', '83'),
('833', 'Mỏ Cày Nam', 'Huyện', '83'),
('834', 'Giồng Trôm', 'Huyện', '83'),
('835', 'Bình Đại', 'Huyện', '83'),
('836', 'Ba Tri', 'Huyện', '83'),
('837', 'Thạnh Phú', 'Huyện', '83'),
('838', 'Mỏ Cày Bắc', 'Huyện', '83'),
('842', 'Trà Vinh', 'Thị Xã', '84'),
('844', 'Càng Long', 'Huyện', '84'),
('845', 'Cầu Kè', 'Huyện', '84'),
('846', 'Tiểu Cần', 'Huyện', '84'),
('847', 'Châu Thành', 'Huyện', '84'),
('848', 'Cầu Ngang', 'Huyện', '84'),
('849', 'Trà Cú', 'Huyện', '84'),
('850', 'Duyên Hải', 'Huyện', '84'),
('855', 'Vĩnh Long', 'Thành Phố', '86'),
('857', 'Long Hồ', 'Huyện', '86'),
('858', 'Mang Thít', 'Huyện', '86'),
('859', 'Vũng Liêm', 'Huyện', '86'),
('860', 'Tam Bình', 'Huyện', '86'),
('861', 'Bình Minh', 'Huyện', '86'),
('862', 'Trà Ôn', 'Huyện', '86'),
('863', 'Bình Tân', 'Huyện', '86'),
('866', 'Cao Lãnh', 'Thành Phố', '87'),
('867', 'Sa Đéc', 'Thị Xã', '87'),
('868', 'Hồng Ngự', 'Thị Xã', '87'),
('869', 'Tân Hồng', 'Huyện', '87'),
('870', 'Hồng Ngự', 'Huyện', '87'),
('871', 'Tam Nông', 'Huyện', '87'),
('872', 'Tháp Mười', 'Huyện', '87'),
('873', 'Cao Lãnh', 'Huyện', '87'),
('874', 'Thanh Bình', 'Huyện', '87'),
('875', 'Lấp Vò', 'Huyện', '87'),
('876', 'Lai Vung', 'Huyện', '87'),
('877', 'Châu Thành', 'Huyện', '87'),
('883', 'Long Xuyên', 'Thành Phố', '89'),
('884', 'Châu Đốc', 'Thị Xã', '89'),
('886', 'An Phú', 'Huyện', '89'),
('887', 'Tân Châu', 'Thị Xã', '89'),
('888', 'Phú Tân', 'Huyện', '89'),
('889', 'Châu Phú', 'Huyện', '89'),
('890', 'Tịnh Biên', 'Huyện', '89'),
('891', 'Tri Tôn', 'Huyện', '89'),
('892', 'Châu Thành', 'Huyện', '89'),
('893', 'Chợ Mới', 'Huyện', '89'),
('894', 'Thoại Sơn', 'Huyện', '89'),
('899', 'Rạch Giá', 'Thành Phố', '91'),
('900', 'Hà Tiên', 'Thị Xã', '91'),
('902', 'Kiên Lương', 'Huyện', '91'),
('903', 'Hòn Đất', 'Huyện', '91'),
('904', 'Tân Hiệp', 'Huyện', '91'),
('905', 'Châu Thành', 'Huyện', '91'),
('906', 'Giồng Giềng', 'Huyện', '91'),
('907', 'Gò Quao', 'Huyện', '91'),
('908', 'An Biên', 'Huyện', '91'),
('909', 'An Minh', 'Huyện', '91'),
('910', 'Vĩnh Thuận', 'Huyện', '91'),
('911', 'Phú Quốc', 'Huyện', '91'),
('912', 'Kiên Hải', 'Huyện', '91'),
('913', 'U Minh Thượng', 'Huyện', '91'),
('914', 'Giang Thành', 'Huyện', '91'),
('916', 'Ninh Kiều', 'Quận', '92'),
('917', 'Ô Môn', 'Quận', '92'),
('918', 'Bình Thuỷ', 'Quận', '92'),
('919', 'Cái Răng', 'Quận', '92'),
('923', 'Thốt Nốt', 'Quận', '92'),
('924', 'Vĩnh Thạnh', 'Huyện', '92'),
('925', 'Cờ Đỏ', 'Huyện', '92'),
('926', 'Phong Điền', 'Huyện', '92'),
('927', 'Thới Lai', 'Huyện', '92'),
('930', 'Vị Thanh', 'Thị Xã', '93'),
('931', 'Ngã Bảy', 'Thị Xã', '93'),
('932', 'Châu Thành A', 'Huyện', '93'),
('933', 'Châu Thành', 'Huyện', '93'),
('934', 'Phụng Hiệp', 'Huyện', '93'),
('935', 'Vị Thuỷ', 'Huyện', '93'),
('936', 'Long Mỹ', 'Huyện', '93'),
('941', 'Sóc Trăng', 'Thành Phố', '94'),
('942', 'Châu Thành', 'Huyện', '94'),
('943', 'Kế Sách', 'Huyện', '94'),
('944', 'Mỹ Tú', 'Huyện', '94'),
('945', 'Cù Lao Dung', 'Huyện', '94'),
('946', 'Long Phú', 'Huyện', '94'),
('947', 'Mỹ Xuyên', 'Huyện', '94'),
('948', 'Ngã Năm', 'Huyện', '94'),
('949', 'Thạnh Trị', 'Huyện', '94'),
('950', 'Vĩnh Châu', 'Huyện', '94'),
('951', 'Trần Đề', 'Huyện', '94'),
('954', 'Bạc Liêu', 'Thị Xã', '95'),
('956', 'Hồng Dân', 'Huyện', '95'),
('957', 'Phước Long', 'Huyện', '95'),
('958', 'Vĩnh Lợi', 'Huyện', '95'),
('959', 'Giá Rai', 'Huyện', '95'),
('960', 'Đông Hải', 'Huyện', '95'),
('961', 'Hoà Bình', 'Huyện', '95'),
('964', 'Cà Mau', 'Thành Phố', '96'),
('966', 'U Minh', 'Huyện', '96'),
('967', 'Thới Bình', 'Huyện', '96'),
('968', 'Trần Văn Thời', 'Huyện', '96'),
('969', 'Cái Nước', 'Huyện', '96'),
('970', 'Đầm Dơi', 'Huyện', '96'),
('971', 'Năm Căn', 'Huyện', '96'),
('972', 'Phú Tân', 'Huyện', '96'),
('973', 'Ngọc Hiển', 'Huyện', '96');

-- --------------------------------------------------------

--
-- Table structure for table `db_help`
--

CREATE TABLE IF NOT EXISTS `db_help` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `fulltext` text NOT NULL,
  `img` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `modified` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `orders` int(11) NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `db_images_product`
--

CREATE TABLE IF NOT EXISTS `db_images_product` (
  `id` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `images` int(11) NOT NULL,
  `trash` int(1) NOT NULL DEFAULT '1',
  `access` int(1) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `db_menu`
--

CREATE TABLE IF NOT EXISTS `db_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  `parentid` int(11) NOT NULL,
  `orders` int(11) NOT NULL,
  `position` varchar(50) DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'Supper admin',
  `modified` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT 'Supper admin',
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `nameimg` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_menu`
--

INSERT INTO `db_menu` (`id`, `name`, `link`, `level`, `parentid`, `orders`, `position`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`, `nameimg`) VALUES
(1, 'Trang chủ', 'trang-chu', 1, 0, 1, 'copyright', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(2, 'Giới thiệu', 'gioi-thieu', 1, 0, 2, 'copyright', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(3, 'Sản phẩm', 'san-pham', 1, 0, 3, 'copyright', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(4, 'Tin tức', 'tin-tuc', 1, 0, 4, 'copyright', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(6, 'Quà tặng', 'qua-tang', 1, 0, 6, 'copyright', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(7, 'Điện tử', 'dien-tu', 2, 3, 1, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_1.png'),
(8, 'Điện lạnh', 'dien-lanh', 2, 3, 2, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_2.png'),
(9, 'Nhà bếp', 'nha-bep', 2, 3, 3, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_3.png'),
(10, 'Gia dụng', 'gia-dung', 2, 3, 4, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_4.png'),
(11, 'Viễn thông', 'vien-thong', 2, 3, 5, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_5.png'),
(12, 'Tin học', 'tin-hoc', 2, 3, 6, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_6.png'),
(13, 'Kỹ thuật số', 'ky-thuat-so', 2, 3, 7, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_7.png'),
(14, 'Thiết bị văn phòng', 'thiet-bi-van-phong', 2, 3, 8, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_8.png'),
(15, 'Điện cơ', 'dien-co', 2, 3, 9, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_9.png'),
(16, 'Phụ kiện', 'phu-kien', 2, 3, 10, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_10.png'),
(17, 'Sức khỏe làm đẹp', 'suc-khoe-lam-dep', 2, 3, 11, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_11.png'),
(18, 'Bách hóa', 'bach-hoa', 2, 3, 12, 'left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'leftmenu_icon_12.png'),
(19, 'Tivi', 'tivi', 3, 7, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(20, 'Dàn máy nghe nhạc', 'dan-may-nghe-nhac', 3, 7, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(21, 'Đầu đĩa DVD Bluray - Karaoke', 'dau-dia-dvd-bluray-karaoke', 3, 7, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(22, 'Amply & Loa', 'amply-loa', 3, 7, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(23, 'Máy Radio Cassette', 'may-radio-cassette', 3, 7, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(24, 'Phụ kiện điện tử', 'phu-kien-dien-tu', 3, 7, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(25, 'Máy giặt', 'may-giat', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(26, 'Máy lạnh', 'may-lanh', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(27, 'Tủ lạnh', 'tu-lanh', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(28, 'Máy nước nóng', 'may-nuoc-nong', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(29, 'Máy sấy quần áo', 'may-say-quan-ao', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(30, 'Máy nước nóng lạnh', 'may-nuoc-nong-lanh', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(31, 'Tủ đông', 'tu-dong', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(32, 'Tủ mát - Tủ giữ lạnh', 'tu-mat-tu-giu-lanh', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(33, 'Máy lọc nước', 'may-loc-nuoc', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(34, 'Máy lọc không khí', 'may-loc-khong-khi', 3, 8, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(35, 'Nồi cơm điện', 'noi-com-dien', 3, 9, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(36, 'Bếp gas', 'bep-gas', 3, 9, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(37, 'Nồi đa năng', 'noi-da-nang', 3, 9, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(39, 'Lò vi sóng', 'lo-vi-song', 3, 9, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(40, 'Bộ nồi nấu ăn', 'bo-noi-nau-an', 3, 9, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(41, 'Nồi áp suất', 'noi-ap-suat', 3, 9, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(42, 'Chảo chống dính', 'chao-chong-dinh', 3, 9, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(43, 'Quạt điện - Quạt máy', 'quat-dien-quat-may', 3, 10, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(44, 'Máy hút bụi', 'may-hut-bui', 3, 10, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(45, 'Bàn ủi - Bàn là', 'ban-ui-ban-la', 3, 10, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(46, 'Máy hút ẩm', 'may-hut-am', 3, 10, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(47, 'Quạt phun sương - Quạt hơi nước', 'quat-phun-suong', 3, 10, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(48, 'Bình lọc nước', 'binh-loc-nuoc', 3, 10, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(49, 'Máy pha cà phê', 'may-pha-ca-phe', 3, 10, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(50, 'Điện thoại di động', 'dien-thoai-di-dong', 3, 11, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(51, 'Máy tính bảng', 'may-tinh-bang', 3, 11, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(52, 'Máy nghe nhạc MP3/MP4', 'may-nghe-nhac-mp3-mp4', 3, 11, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(53, 'Sim - Thẻ cào', 'sim-the-cao', 3, 11, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(54, 'Phụ kiện điện thoại', 'phu-kien-dien-thoai', 3, 11, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(55, 'Máy tính xách tay', 'may-tinh-xach-tay', 3, 12, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(56, 'Máy tính để bàn', 'may-tinh-de-ban', 3, 12, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(57, 'Màn hình vi tính LCD', 'man-hinh-vi-tinh-lcd', 3, 12, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(58, 'Loa vi tính', 'loa-vi-tinh', 3, 12, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(59, 'Phụ kiện tin học', 'phu-kien-tin-hoc', 3, 12, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(60, 'Máy ảnh ống kính rời', 'may-anh-ong-kinh-roi', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(61, 'Máy ảnh du lịch', 'may-anh-du-lich', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(62, 'Máy quay phim', 'may-quay-phim', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(63, 'Camera quan sát', 'camera-quan-sat', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(64, 'Máy ghi âm', 'may-ghi-am', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(65, 'Máy chụp ảnh lấy liên', 'may-chup-anh-lay-lien', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(66, 'Máy nghe nhạc MP3/MP4', 'may-nghe-nhac-mp3-mp4', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(67, 'Thiết bị giải trí', 'thiet-bi-giai-tri', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(68, 'Phụ kiện kỹ thuật số', 'phu-kien-ky-thuat-so', 3, 13, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(69, 'Máy in', 'may-in', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(70, 'Máy scan', 'may-scan', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(71, 'Máy chiếu', 'may-chieu', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(72, 'Máy fax', 'may-fax', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(73, 'Máy hủy giấy', 'may-huy-giay', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(74, 'Máy đếm tiền', 'may-dem-tien', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(75, 'Máy tính bỏ túi', 'may-tinh-bo-tui', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(76, 'Máy in Bill', 'may-in-bill', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(77, 'Máy quét Barcode', 'may-quet-barcode', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(78, 'Phụ kiện văn phòng', 'phu-kien-van-phong', 3, 14, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(79, 'Gối', 'goi', 3, 18, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(80, 'Vali', 'vali', 3, 18, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(81, 'Ba lô', 'ba-lo', 3, 18, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(82, 'Mền', 'men', 3, 18, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(83, 'Hóa mỹ phẩm', 'hoa-my-pham', 3, 18, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(84, 'Bộ Drap trải giường và áo gối', 'drap-trai-giuong-va-ao-goi', 3, 18, 1, 'subleft', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(85, 'Khuyến mãi', 'khuyen-mai', 1, 0, 1, 'top', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(86, 'Dịch vụ', 'dich-vu', 1, 0, 2, 'top', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(88, 'Tuyển dụng', 'tuyen-dung', 1, 0, 4, 'top', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(89, 'Tài khoản', 'tai-khoan', 1, 0, 5, 'top', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(90, 'Đăng nhập', 'dang-nhap', 1, 0, 6, 'top', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(91, 'Đăng kí', 'dang-ki', 1, 0, 7, 'top', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(92, 'Máy lạnh', 'may-lanh', 1, 8, 1, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_1.png'),
(93, 'Tủ lạnh', 'tu-lanh', 1, 8, 2, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_2.png'),
(94, 'Máy giặt', 'may-giat', 1, 8, 3, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_3.png'),
(95, 'Quạt máy', 'quat-dien-quat-may', 1, 10, 4, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_4.png'),
(96, 'Bếp gas', 'bep-gas', 1, 9, 5, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_5.png'),
(97, 'Tivi', 'tivi', 1, 7, 6, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_6.png'),
(98, 'Điện thoại', 'dien-thoai-di-dong', 1, 11, 7, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_7.png'),
(99, 'Máy ảnh', 'may-anh-ong-kinh-roi', 1, 13, 8, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_8.png'),
(100, 'Laptop', 'may-tinh-xach-tay', 1, 12, 9, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_9.png'),
(101, 'Tablet', 'may-tinh-bang', 1, 11, 10, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_11.png'),
(102, 'Máy in', 'may-in', 1, 14, 11, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_12.png'),
(103, 'Audio', 'may-nghe-nhac-mp3-mp4', 1, 13, 12, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_13.png'),
(104, 'Chính sách vận chuyển', 'chinh-sach-van-chuyen', 1, 0, 1, 'footer', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(105, 'Chính sách bảo mật', 'chinh-sach-bao-mat', 1, 0, 2, 'footer', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(106, 'Đổi trả hàng', 'doi-tra-hang', 1, 0, 3, 'footer', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(107, 'Vận chuyển', 'van-chuyen', 1, 0, 4, 'footer', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(108, 'Phí vận chuyển', 'phi-van-chuyen', 1, 0, 5, 'footer', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(109, 'Phương thức thanh toán', 'phuong-thuc-thanh-toan', 1, 0, 6, 'footer', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, NULL),
(110, 'Iphone', 'dien-thoai-di-dong', 1, 11, 10, 'main', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 1, 1, 'main_menu_icon_10.png'),
(113, 'Chạy thử menu', 'chay-thu-menu', 3, 7, 0, 'left', '2016-09-30 20:17:32', 'Ngô Trung Phát', '2016-09-30 20:17:32', 'Ngô Trung Phát', 1, 1, 1, NULL),
(114, 'Test', 'test', 4, 19, 0, 'subleft', '2016-09-30 20:18:37', 'Ngô Trung Phát', '2016-09-30 20:18:37', 'Ngô Trung Phát', 1, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `db_order`
--

CREATE TABLE IF NOT EXISTS `db_order` (
  `id` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `orderdate` datetime NOT NULL,
  `requireddate` datetime NOT NULL,
  `shipperdate` datetime NOT NULL,
  `total` double NOT NULL,
  `address` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `district` varchar(100) NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_order`
--

INSERT INTO `db_order` (`id`, `customerid`, `orderdate`, `requireddate`, `shipperdate`, `total`, `address`, `province`, `district`, `trash`, `access`, `status`) VALUES
(1, 16, '2016-11-16 14:14:13', '2016-11-16 14:14:13', '2016-11-16 14:14:13', 66930000, '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', 'Bạc Liêu', 'Bạc Liêu', 1, 1, 1),
(2, 15, '2016-07-16 14:14:13', '2016-11-26 14:14:13', '2016-11-25 14:14:13', 66930000, '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', 'Bạc Liêu', 'Bạc Liêu', 1, 1, 1),
(3, 1, '2016-11-26 14:14:13', '2016-02-26 14:14:13', '2016-02-25 14:14:13', 66930000, '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', 'Bạc Liêu', 'Bạc Liêu', 1, 1, 1),
(4, 15, '2016-11-18 00:49:35', '2016-11-18 00:49:35', '2016-11-18 00:49:35', 43320000, '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', 'Cao Bằng', 'Thông Nông', 1, 1, 1),
(5, 17, '2016-11-21 13:35:10', '2016-11-21 13:35:10', '2016-11-21 13:35:10', 20940000, '445/13 Song Hành Hà Nội, phường Bình Thọ, quận Thủ Đức', 'Bình Dương', 'Thủ Dầu Một', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `db_orderdetail`
--

CREATE TABLE IF NOT EXISTS `db_orderdetail` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_orderdetail`
--

INSERT INTO `db_orderdetail` (`id`, `orderid`, `productid`, `price`, `amount`, `discount`, `trash`, `access`, `status`) VALUES
(141, 20, 36, 3100000, 1, 7, 1, 1, 1),
(142, 20, 23, 1700000, 1, 8, 1, 1, 1),
(143, 20, 26, 650000, 3, 8, 1, 1, 1),
(144, 20, 20, 450000, 1, 10, 1, 1, 1),
(145, 1, 175, 45990000, 1, 0, 1, 1, 1),
(146, 1, 174, 20900000, 1, 0, 1, 1, 1),
(147, 2, 175, 45990000, 1, 0, 1, 1, 1),
(148, 2, 174, 20900000, 1, 0, 1, 1, 1),
(149, 4, 170, 12400000, 3, 0, 1, 1, 1),
(150, 4, 2, 4490000, 1, 8, 1, 1, 1),
(151, 4, 21, 1590000, 1, 9, 1, 1, 1),
(152, 5, 174, 20900000, 1, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `db_product`
--

CREATE TABLE IF NOT EXISTS `db_product` (
  `id` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `introtext` text NOT NULL,
  `tag` varchar(255) NOT NULL,
  `number` int(11) NOT NULL,
  `number_buy` int(11) NOT NULL,
  `sale` int(3) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `price_sale` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL DEFAULT 'HDL',
  `modified` datetime NOT NULL,
  `modified_by` varchar(100) NOT NULL DEFAULT 'HDL',
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `metakey` varchar(155) NOT NULL,
  `metadesc` varchar(155) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_product`
--

INSERT INTO `db_product` (`id`, `catid`, `name`, `alias`, `img`, `detail`, `introtext`, `tag`, `number`, `number_buy`, `sale`, `price`, `price_sale`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`, `metakey`, `metadesc`) VALUES
(0, 4, 'Máy Hút Ẩm Delonghi Des16', 'may-hut-am-delonghi-des16', 'may-hut-am-delonghi-des-16ew.jpg', '', '', '', 10, 10, 10, 9000000, 8500000, '0000-00-00 00:00:00', 'HDL', '2016-11-23 14:53:23', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(1, 21, 'Tủ Lạnh Panasonic Nr (152l)', 'tu-lanh-panasonic-nr-152l', 'tu-lanh-panasonic-nr-bm179ssvn-.jpg', '', '', '', 10, 2, 8, 5000000, 4590000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(2, 21, 'Tủ Lạnh Aqua Aqr - S185an', 'tu-lanh-aqua-aqr-s185an', 'tu-lanh-aqua-aqr-s185an-sn.jpg', '', '', '', 10, 10, 8, 4900000, 4490000, '0000-00-00 00:00:00', 'HDL', '2016-11-22 17:59:07', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(3, 21, 'Tủ Lạnh Panasonic Nr (167l)', 'tu-lanh-panasonic-nr-167l', 'tu-lanh-panasonic-nr-bj186ssvn.jpg', '', '', '', 10, 2, 7, 4820000, 4490000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(4, 21, 'Tủ Lạnh Aqua Aqr-145an (Ss)', 'tu-lanh-aqua-aqr-145an-ss', 'tu-lanh-aqua-aqr-145an-ss.jpg', '', '', '', 10, 2, 8, 4820000, 4100000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(5, 21, 'Tủ Lạnh Sanyo Sr-145pn(Vs)', 'tu-lanh-sanyo-sr-145pn-vs', 'tu-lanh-sanyo-sr-145pn-vs-.jpg', '', '', '', 10, 2, 8, 4800000, 4100000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(6, 21, 'Tủ Lạnh Panasonic Nr (135l)', 'tu-lanh-panasonic-nr-135l', 'tu-lanh-panasonic-nr-bbj151ssv1.jpg', '', '', '', 10, 2, 10, 3990000, 4610000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(7, 21, 'Tủ Lạnh Aqua Aqr-125an (Ss)', 'tu-lanh-aqua-aqr-125an-ss', 'tu-lanh-aqua-aqr-125an-ss.jpg', '', '', '', 10, 2, 8, 4610000, 3790000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(8, 21, 'Tủ Lạnh Toshiba Gr-V906vn(I)', 'tu-lanh-toshiba-gr-v906vn-i', 'tu-lanh-toshiba-gr-v906vn.jpg', '', '', '', 10, 2, 9, 4500000, 2590000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(9, 21, 'Tủ Lạnh Electrolux 210 Lít', 'tu-lanh-electrolux-210', 'may-lanh-electrolux-etb2102pe.jpg', '', '', '', 10, 2, 4, 6990000, 4690000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(11, 19, 'Máy Giặt Aqua Aqw-S70kt (H)', 'may-giat-aqua-aqw-s70kt-h', 'may-giat-aqua-s70kt-h-06.jpg', '', '', '', 10, 2, 9, 11900000, 6900000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(13, 13, 'Tivi Led Samsung Ua48j6200', 'tivi-led-samsung-ua48j6200', 'tivi-led-samsung-ua48j6200akxxv-2.jpg', '', '', '', 10, 2, 7, 14100000, 13800000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(14, 13, 'Tivi Led Samsung Ua40j6300', 'tivi-led-samsung-ua40j6300', '10021724-samsung-ua32j6300akxxv-1.jpg', '', '', '', 10, 2, 8, 14400000, 13900000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(15, 13, 'Tivi Led Samsung Ua43j5500', 'tivi-led-samsung-ua43j5500', '10021258-samsung-ua43j5500akxxv-1.jpg', '', '', '', 10, 2, 5, 13900000, 10890000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(16, 13, 'Tivi Led Hd Panasonic 32c400v', 'tivi-led-hd-panasonic-32c400v', '10021837-sony-kdl-48r550c-vn3-1.jpg', '', '', '', 10, 2, 7, 13900000, 12900000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(17, 13, 'Tivi LCD Sony Kdl-32r300c Vn3', 'tivi-lcd-sony-kdl-32r300c-vn3', 'tivi-led-sony-32r300c-1.jpg', '', '', '', 10, 2, 7, 7000000, 5890000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(19, 30, 'Vỉ Nướng Electrolux Ebg100', 'vi-nuong-electrolux-ebg100', '0065131-electrolux-ebg100-1.jpg', '', '', '', 10, 10, 8, 7000000, 7000000, '0000-00-00 00:00:00', 'HDL', '2016-11-23 14:50:45', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(20, 42, 'Máy Pha Cà Phê Ecm3200r', 'may-pha-ca-phe-ecm3200r', 'may-pha-ca-phe-electrolux-ecm3200r.jpg', '', '', '', 10, 2, 10, 650000, 450000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(21, 37, 'Hút Bụi Panasonic Mc-Cg331rn46', 'hut-bui-panasonic-mc-cg331rn46', 'may-hut-bui-panasonic-mc-cg331rn46-hinh-1.jpg', '', '', '', 10, 2, 9, 1840000, 1590000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(22, 38, 'Bàn Ủi Philips Gc504', 'ban-ui-philips-gc504', '10013987-ban-ui-philips-gc504.jpg', '', '', '', 10, 2, 5, 2090000, 1690000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(23, 40, 'Quạt Đứng Mitsubishi Lv16-Rt', 'quat-dung-mitsubishi-lv16-rt', 'quat-dung-mitsubishi-lv16-rt-cy-gy-xam-dam.jpg', '', '', '', 10, 10, 8, 2000000, 1700000, '0000-00-00 00:00:00', 'HDL', '2016-11-23 14:50:09', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(24, 31, 'Xay Đa Năng Panasonic Mjzi', 'xay-da-nang-panasonic-mjzi', '1209050-panasonic-mj-m176pwra.png', '', '', '', 10, 10, 4, 2580000, 2450000, '0000-00-00 00:00:00', 'HDL', '2016-11-22 17:59:46', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(25, 31, 'Máy Xay Sinh Tố Steba Mx2 Plus', 'may-xay-sinh-to-steba-mx2-plus', 'may-xay-sinh-to-steba-mx2-plus.jpg', '', '', '', 10, 2, 8, 2580000, 2390000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(26, 31, 'Sinh Tố Panasonic Mx-Gm1011gra', 'sinh-to-panasonic-mx-gm1011gra', 'may-xay-sinh-to-panasonic-mx-gm1011-1.jpg', '', '', '', 10, 2, 8, 700000, 650000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(27, 31, 'Lò Vi Sóng Sanyo Em-G3564vfrg', 'lo-vi-song-sanyo-em-g3564vfrg', 'lo-vi-song-electrolux-emm2318x.jpg', '', '', '', 10, 2, 6, 3000000, 2890000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(28, 31, 'Lò Vi Sóng Electrolux E2318', 'lo-vi-song-electrolux-e2318', 'lo-vi-song-sanyo-em-s2182w-fbfa1538-1f68-4bee-817e-bcc3f8f8a593.jpg', '', '', '', 10, 10, 7, 3000000, 2890000, '0000-00-00 00:00:00', 'HDL', '2016-11-23 14:51:21', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(30, 54, 'Máy Ảnh Nikon Di.Camera D3300', 'may-anh-nikon-di-camera-d3300', '1-may-anh-chuyen-nghiep-nikon-d3300-kit-18-55-01.jpg', '', '', '', 10, 2, 7, 12500000, 11790000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(31, 55, 'Máy Ảnh Canon Eos 700d 18-55', 'may-anh-canon-eos-700d-18-55', 'may-anh-chuyen-nghiep-canon-700d-01.jpg', '', '', '', 10, 2, 8, 12000000, 10790000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(32, 54, 'Máy ảnh Canon 1200d-01', 'may-anh-canon-1200d-01', 'may-anh-chuyen-nghiep-canon-1200d-01.jpg', '', '', '', 10, 2, 5, 12000000, 10900000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(33, 54, 'Máy ảnh Nikon D3300', 'may-anh-nikon-d3300', 'may-anh-chuyen-nghiep-nikon-d3300-kit-18-55-01.jpg', '', '', '', 10, 2, 6, 22580000, 22450000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(34, 45, 'Máy nghe nhạc MP3 Sony WH505', 'may-nghe-nhac-mp3-sony-wh505', 'may-nghe-nhac-mp3-sony-nwz-wh505-bme.jpg', '', '', '', 10, 2, 8, 12580000, 12450000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 0, '', ''),
(35, 45, 'Máy nghe nhạc MP3 Sony M504', 'may-nghe-nhac-mp3-sony-m504', 'may-nghe-nhac-mp3-sony-nwz-m504-den-01.jpg', '', '', '', 10, 2, 8, 2580000, 2450000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(36, 43, 'Samsung Galaxy S6 Edge 32gb', 'samsung-galaxy-s6-edge-32gb', 'samsung-galaxy-a5-vang.jpg', '', '', '', 10, 2, 7, 4100000, 3100000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(38, 43, 'Samsung Sm-A510fd Vàng', 'samsung-sm-a510fd-vang', 'samsung-galaxy-a5-vang.jpg', '', '', '', 10, 2, 5, 12000000, 12000000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(39, 43, 'Samsung Sm-N910c Đen', 'samsung-sm-n910c-den', 'ss-n4-d.jpg', '', '', '', 10, 2, 5, 12990000, 12990000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(45, 78, 'Bộ Chăn Drap Gối Hometex', 'bo-chan-drap-goi-hometex', 'bo-chan-drap-goi-hometex-trung-cap-boc-160x200cm-cotton-hoa.jpg', '', '', '', 10, 2, 47, 1290000, 690000, '2016-09-21 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(48, 4, 'Máy Hút Ẩm Delonghi Des16', 'may-hut-am-delonghi-des16', 'may-hut-am-delonghi-des-16ew.jpg', '', '', '', 10, 10, 10, 9000000, 8500000, '0000-00-00 00:00:00', 'HDL', '2016-11-23 14:52:09', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(52, 21, 'Tủ Lạnh Aqua Aqr - S185an (Sn)', 'tu-lanh-aqua-aqr-s185an-sn', 'tu-lanh-aqua-aqr-s185an-sn.jpg', '', '', '', 10, 2, 8, 4900000, 4490000, '0000-00-00 00:00:00', 'HDL', '0000-00-00 00:00:00', 'HDL', 1, 1, 1, '', ''),
(140, 13, 'Tivi Sony 32 Inch Dvb-T2', 'tivi-sony-32-inch-dvb-t2', 'dvb-121.png', '<p>TIVI SONY 32 INCH DVB-T2</p>\r\n', '', '', 10, 10, 12, 6490000, 5690000, '2016-10-13 20:30:50', 'Ngô Trung Phát', '2016-10-13 20:59:23', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(143, 13, 'Tivi Sony 40 Inch Full Hd', 'tivi-sony-40-inch-full-hd', 'sn31.png', '<p>Tivi Sony 40 Inch Full Hd</p>\r\n', '', '', 10, 10, 8, 8490000, 7790000, '2016-10-13 21:21:51', 'Ngô Trung Phát', '2016-10-13 21:21:51', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(144, 13, 'Tivi Sony 40W650D Internet 40 Inch', 'tivi-sony-40w650d-internet-40-inch', 'sn41.png', '<p><a href="http://eco-mart.com.vn/products/tivi-sony-40w650d-internet-40-inches">Tivi Sony 40W650D Internet 40 inch</a></p>\r\n', '', '', 10, 10, 0, 9690000, 0, '2016-10-13 21:25:37', 'Ngô Trung Phát', '2016-10-13 21:25:37', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(145, 13, 'Tivi Sony 43 Inch 4k Uhd Smart Tv', 'tivi-sony-43-inch-4k-uhd-smart-tv', 'sn61.png', '<p>Tivi Sony 43 Inch 4k Uhd Smart Tv</p>\r\n', '', '', 10, 10, 9, 16990000, 15490000, '2016-10-13 23:04:34', 'Ngô Trung Phát', '2016-10-13 23:04:34', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(146, 13, 'Tivi Sony 43 Inch Smart Tv', 'tivi-sony-43-inch-smart-tv', 'sn71.png', '<p>Tivi Sony 43 Inch Smart Tv</p>\r\n', '', '', 10, 10, 32, 15490000, 10590000, '2016-10-13 23:06:56', 'Ngô Trung Phát', '2016-10-13 23:06:56', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(147, 13, 'Tivi Sony 43 inch Smart TV 3D Full HD', 'tivi-sony-43-inch-smart-tv-3d-full-hd', 'sn141.png', '<p>Tivi Sony 43 inch Smart TV 3D Full HD</p>\r\n', '', '', 10, 10, 8, 12890000, 11890000, '2016-10-13 23:09:21', 'Ngô Trung Phát', '2016-10-13 23:09:21', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(148, 13, 'Tivi Sony 4k Smart TV', 'tivi-sony-4k-smart-tv', 'sn91.png', '<p>Tivi Sony 43x8000ds 4k Smart</p>\r\n', '', '', 10, 10, 9, 16990000, 15490000, '2016-10-13 23:10:58', 'Ngô Trung Phát', '2016-11-23 14:58:29', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(149, 13, 'Tivi Sony 48 Inch Internet 200hz', 'tivi-sony-48-inch-internet-200hz', 'sn101.png', '<p>Tivi Sony 48 Inch Internet 200hz</p>\r\n', '', '', 10, 10, 6, 14190000, 13290000, '2016-10-13 23:13:13', 'Ngô Trung Phát', '2016-10-13 23:13:13', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(150, 13, 'Tivi Sony 49 Inch Smart Tv', 'tivi-sony-49-inch-smart-tv', 'sn111.png', '<p><a href="http://eco-mart.com.vn/products/tivi-sony-49w750d-49-inch-smart-tv">Tivi Sony 49 Inch Smart Tv</a></p>\r\n', '', '', 10, 10, 5, 15490000, 14690000, '2016-10-13 23:15:33', 'Ngô Trung Phát', '2016-10-13 23:15:33', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(151, 13, 'Tivi Sony 49x8300c 49 Inch 4k', 'tivi-sony-49x8300c-49-inch-4k', 'sn121.png', '<p>Tivi Sony 49x8300c 49 Inch 4k</p>\r\n', '', '', 10, 10, 0, 22890000, 0, '2016-10-13 23:17:11', 'Ngô Trung Phát', '2016-10-13 23:17:11', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(152, 13, 'Tivi Sony 50 inch màn cong 4K', 'tivi-sony-50-inch-man-cong-4k', 'sn201.png', '<p><a href="http://eco-mart.com.vn/products/tivi-sony-50s8000d-man-cong">Tivi Sony 50 inch m&agrave;n cong 4K</a></p>\r\n', '', '', 10, 10, 0, 23990000, 0, '2016-10-13 23:19:11', 'Ngô Trung Phát', '2016-10-13 23:19:11', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(153, 13, 'Tivi Sony 50 inch Smart TV 3D Full HD', 'tivi-sony-50-inch-smart-tv-3d-full-hd', 'sn42.png', '<p><a href="http://eco-mart.com.vn/products/tivi-sony-50w800c-internet-wifi-3d-full-hd">Tivi Sony 50 inch Smart TV 3D Full HD</a></p>\r\n', '', '', 10, 10, 0, 17690000, 0, '2016-10-13 23:21:01', 'Ngô Trung Phát', '2016-10-13 23:21:01', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(154, 13, 'TIVI SONY 55 INCH 4K SMART TV 2 Màu', 'tivi-sony-55-inch-4k-smart-tv-2-mau', 'sn151.png', '<p>TIVI SONY 55 INCH 4K SMART TV 2 M&agrave;u</p>\r\n', '', '', 10, 10, 16, 38990000, 32890000, '2016-10-13 23:23:19', 'Ngô Trung Phát', '2016-10-13 23:23:19', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(155, 13, 'Tivi Sony 55 inch Smart TV 3D Full HD', 'tivi-sony-55-inch-smart-tv-3d-full-hd', 'sn72.png', '<p><a href="http://eco-mart.com.vn/products/tivi-sony-55w800c-internet-wifi-3d-full-hd">Tivi Sony 55 inch Smart TV 3D Full HD</a></p>\r\n', '', '', 10, 10, 5, 15490000, 14690000, '2016-10-13 23:24:50', 'Ngô Trung Phát', '2016-10-13 23:24:50', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(156, 14, ' Tivi Samsung 55 Inch 4k Tizen Os', 'tivi-samsung-55-inch-4k-tizen-os', 'ss-s (7).jpg#ss-s (3).jpg#ss-s (4).jpg#ss-s (5).jpg#ss-s (6).jpg#ss-s (1).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-55-inch-smart-tv-4k-suhd-hdr-tizen-os-man-cong-1">&nbsp;Tivi Samsung 55 Inch 4k Tizen Os</a></p>\r\n', '', 'SAMSUNG|Smart TV|55 Inch', 10, 10, 0, 69900000, 0, '2016-10-13 23:47:32', 'Ngô Trung Phát', '2016-10-13 23:47:32', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(157, 14, 'Tv Samsung 78 Inch 4k Tizen Os', 'tv-samsung-78-inch-4k-tizen-os', 'ss-r (2).jpg#ss-r (1).jpg#ss-r (3).jpg#ss-r (4).jpg#ss-r (5).jpg#ss-r (6).jpg#ss-r (7).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-78-inch-smart-tv-4k-suhd-tizen-os-man-hinh-cong">Tv Samsung 78 Inch 4k Tizen Os</a></p>\r\n', '', 'SAMSUNG|Smart TV|78 Inch', 10, 10, 0, 18900000, 0, '2016-10-13 23:48:58', 'Ngô Trung Phát', '2016-10-13 23:48:58', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(158, 14, 'Tivi Led Samsung 32 Inch 32j4003', 'tivi-led-samsung-32-inch-32j4003', 'ss-q (2).jpg#ss-q (1).jpg#ss-q (3).jpg#ss-q (4).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-led-samsung-32-inch-32j4003">Tivi Led Samsung 32 Inch 32j4003</a></p>\r\n', '', 'SAMSUNG|Smart TV|32 Inch', 10, 10, 0, 4990000, 0, '2016-10-13 23:49:52', 'Ngô Trung Phát', '2016-10-13 23:49:52', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(159, 14, 'Tivi Samsung 28 inch 28J4100', 'tivi-samsung-28-inch-28j4100', 'ss-p.jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-28-inch-28j4100">Tivi Samsung 28 inch 28J4100</a></p>\r\n', '', 'SAMSUNG|Smart TV|28 Inch', 10, 10, 0, 4790000, 0, '2016-10-13 23:50:34', 'Ngô Trung Phát', '2016-10-13 23:50:34', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(160, 14, 'Tivi Samsung 32 Inch', 'tivi-samsung-32-inch', 'ss-o (1).png#ss-o (1).jpg#ss-o (2).jpg#ss-o (3).jpg#ss-o (4).jpg#ss-o (5).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-32k4100-32-inch">Tivi Samsung 32 Inch</a></p>\r\n', '', 'SAMSUNG|Smart TV|32 Inch', 10, 10, 0, 5690000, 0, '2016-10-13 23:52:06', 'Ngô Trung Phát', '2016-10-13 23:52:14', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(161, 14, 'Tivi SS 32in Smart Tv Tizen Os', 'tivi-ss-32in-smart-tv-tizen-os', 'ss-n (2).jpg#ss-n (1).jpg#ss-n (3).jpg#ss-n (4).jpg#ss-n (5).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-32-inch-smart-tv-tizen-os">Tivi Samsung 32 Inch Smart Tv Tizen Os</a></p>\r\n', '', 'SAMSUNG|Smart TV|32 Inch', 1, 1, 0, 8400000, 0, '2016-10-13 23:52:53', 'Ngô Trung Phát', '2016-11-22 17:50:30', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(162, 14, 'Tivi SAMSUNG 32J4303 SMART', 'tivi-samsung-32j4303-smart', 'ss-m.jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-32j4303-smart">Tivi SAMSUNG 32J4303 SMART</a></p>\r\n', '', 'SAMSUNG|Smart TV', 1, 1, 0, 6390000, 0, '2016-10-13 23:53:51', 'Ngô Trung Phát', '2016-10-13 23:53:51', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(163, 14, 'Tivi SS 40 Inch 4k Uhd Tizen Os', 'tivi-ss-40-inch-4k-uhd-tizen-os', 'ss-l (0).jpg#ss-l (1).jpg#ss-l (3).jpg#ss-l (4).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-40-inch-4k-uhd-tizen-os">Tivi Samsung 40 Inch 4k Uhd Tizen Os</a></p>\r\n', '', 'SAMSUNG|Smart TV|40 Inch', 10, 10, 0, 12900000, 0, '2016-10-13 23:54:35', 'Ngô Trung Phát', '2016-11-22 17:51:09', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(164, 14, 'Tivi SS 40in Màn Cong Tizen Os', 'tivi-ss-40in-man-cong-tizen-os', 'ss-k (7).jpg#ss-k (1).jpg#ss-k (3).jpg#ss-k (4).jpg#ss-k (5).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-40-inch-man-hinh-cong-tizen-os">Tivi Samsung 40 Inch M&agrave;n Cong Tizen Os</a></p>\r\n', '', 'SAMSUNG|Smart TV|40 Inch', 1, 1, 0, 14900000, 0, '2016-10-13 23:55:12', 'Ngô Trung Phát', '2016-11-22 17:50:15', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(165, 14, 'Tivi Samsung 40k5500 40 Inch Smart', 'tivi-samsung-40k5500-40-inch-smart', 'ss-i (1).jpg#ss-i (2).jpg#ss-i (3).jpg#ss-i (4).jpg#ss-i (5).jpg#ss-i (6).jpg#ss-i (7).jpg#ss-i (8).jpg#ss-i (9).jpg#ss-i (10).jpg#ss-i (11).jpg#ss-i (12).jpg#ss-i (13).jpg#ss-i (14).jpg#ss-i (15).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-40k5500-40-inch">Tivi Samsung 40k5500 40 Inch Smart</a></p>\r\n', '', 'SAMSUNG|Smart TV|40 Inch', 10, 10, 0, 10900000, 0, '2016-10-13 23:56:04', 'Ngô Trung Phát', '2016-10-13 23:56:04', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(166, 14, 'Tivi Samsung 43 Inch Màn Cong', 'tivi-samsung-43-inch-man-cong', 'ss-h (2).jpg#ss-h (1).jpg#ss-h (3).jpg#ss-h (4).jpg#ss-h (5).jpg#ss-h (6).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-43-inch-4k-uhd-smart-tv-man-cong">Tivi Samsung 43 Inch 4k Smart M&agrave;n Cong</a></p>\r\n', '', 'SAMSUNG|Smart TV|43 Inch', 6, 6, 0, 18400000, 0, '2016-10-13 23:57:02', 'Ngô Trung Phát', '2016-11-22 17:48:15', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(167, 14, 'Tivi Samsung 43in 4k Uhd Smart Tv', 'tivi-samsung-43in-4k-uhd-smart-tv', 'ss-g (1).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-43-inch-4k-uhd-smart-tv">Tivi Samsung 43 Inch 4k Uhd Smart Tv</a></p>\r\n', '', 'SAMSUNG|Smart TV|43 Inch', 8, 8, 0, 16400000, 0, '2016-10-13 23:57:55', 'Ngô Trung Phát', '2016-11-22 17:49:13', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(168, 14, 'Tivi Samsung 43in 4k Uhd Tizen Os', 'tivi-samsung-43in-4k-uhd-tizen-os', 'ss-f (2).jpg#ss-f (3).jpg#ss-f (4).jpg#ss-f (5).jpg#ss-f (6).jpg#ss-f (7).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-43-inch-4k-uhd-tizen-os">Tivi Samsung 43 Inch 4k Uhd Tizen Os</a></p>\r\n', '', 'SAMSUNG|Smart TV|43 Inch', 5, 5, 0, 13400000, 0, '2016-10-13 23:59:07', 'Ngô Trung Phát', '2016-11-22 17:49:48', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(169, 14, 'Tivi Samsung 43 Inch Full Hd Dvb-T2', 'tivi-samsung-43-inch-full-hd-dvb-t2', 'k1_master.jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-43-inch-full-hd-dvb-t2">Tivi Samsung 43 Inch Full Hd Dvb-T2</a></p>\r\n', '', 'SAMSUNG|Smart TV|43 Inch', 3, 3, 0, 9400000, 0, '2016-10-13 23:59:49', 'Ngô Trung Phát', '2016-10-13 23:59:49', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(170, 14, 'Tivi Samsung 43 Inch Smart Tv', 'tivi-samsung-43-inch-smart-tv', 'ss-e (2).jpg#ss-e (3).jpg#ss-e (4).jpg#ss-e (1).jpg', '<p>Tivi Samsung 43 Inch Smart Tv Tizen Os</p>\r\n', '', 'SAMSUNG|Smart TV|43 Inch', 15, 15, 0, 12400000, 0, '2016-10-14 00:03:33', 'Ngô Trung Phát', '2016-11-22 17:47:57', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(171, 14, 'Tivi Samsung 49 Inch 4k Tizen Os', 'tivi-samsung-49-inch-4k-tizen-os', 'ss-d (1).jpg#ss-d (2).jpg#ss-d (3).jpg#ss-d (4).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-49-inch-4k-uhd-tizen-os-man-cong">Tivi Samsung 49 Inch 4k Tizen Os</a></p>\r\n', '', 'SAMSUNG|Smart TV|49 Inch', 13, 13, 0, 24900000, 0, '2016-10-14 00:04:54', 'Ngô Trung Phát', '2016-10-14 00:04:54', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(172, 14, 'Tivi SS 49 Inch 4k Uhd Tizen Os', 'tivi-ss-49-inch-4k-uhd-tizen-os', 'ss-c (2).jpg#ss-c (3).jpg#ss-c (4).jpg#ss-c (5).jpg#ss-c (1).jpg', '<p>Tivi Samsung 49 Inch 4k Uhd Tizen Os</p>\r\n', '', 'SAMSUNG|Smart TV|49 Inch', 15, 15, 0, 22900000, 0, '2016-10-14 00:05:45', 'Ngô Trung Phát', '2016-11-22 17:50:54', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(173, 14, 'Tivi SS 49 in Smart 4k Tizen Os', 'tivi-ss-49-in-smart-4k-tizen-os', 'ss-b6.jpg#ss-b (2).jpg#ss-b (1).jpg#ss-b (3).jpg#ss-b (5).jpg', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-49-inch-smart-tv-4k-suhd-tizen-os-man-cong">Tivi Samsung 49 Inch Smart 4k Tizen Os</a></p>\r\n', '<p><a href="http://eco-mart.com.vn/products/tivi-samsung-49-inch-smart-tv-4k-suhd-tizen-os-man-cong">Tivi Samsung 49 Inch Smart 4k Tizen Os</a></p>\r\n', 'SAMSUNG|Smart TV|49 Inch', 6, 6, 0, 33900000, 33900000, '2016-10-14 00:07:46', 'Ngô Trung Phát', '2016-11-22 17:58:24', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(174, 14, 'Tivi Samsung 49 Inch Smart Tv', 'tivi-samsung-49-inch-smart-tv', 'ss-a (2).jpg#ss-a (1).jpg#ss-a (3).jpg#ss-a (4).jpg#ss-a (5).jpg', '<p>Thiết kế hiện đại Tivi LED TCL L32D2700 được thiết kế với d&aacute;ng vẻ hiện đại, cứng c&aacute;p, đem lại cho căn ph&ograve;ng bạn th&ecirc;m phần nổi bật. Ch&acirc;n đế chắc chắn với kiểu d&aacute;ng h&ograve;a hợp với th&acirc;n, tạo c&aacute;i nh&igrave;n th&acirc;n thiện.</p>\r\n\r\n<p><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-03-f403796e-c276-4bc5-a950-b7c3d65b39bd.jpg?v=1454322011580" /></p>\r\n\r\n<p><em>Tivi LED TCL L32D2700 với thiết kế hiện đại</em></p>\r\n\r\n<p>M&agrave;n h&igrave;nh 32 inches độ ph&acirc;n giải HD Tivi LED TCL L32D2700 trang bị m&agrave;n h&igrave;nh với k&iacute;ch 32 inches c&ugrave;ng độ ph&acirc;n giải HD, đem đến khả năng hiển thị h&igrave;nh ảnh tốt, đ&aacute;p ứng nhu cầu giải tr&iacute; trong ph&ograve;ng kh&aacute;ch cỡ nhỏ hay ph&ograve;ng ngủ.</p>\r\n\r\n<p><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-02.jpg?v=1453801783267" /></p>\r\n\r\n<p>Tivi LED TCL L32D2700 trang bị m&agrave;n h&igrave;nh 32 inches</p>\r\n\r\n<p>&Aacute;nh s&aacute;ng tự nhi&ecirc;n Tivi LED TCL L32D2700 c&oacute; t&iacute;nh năng chuyển sang chế độ &aacute;nh s&aacute;ng tự nhi&ecirc;n tiện lợi, sẽ gi&uacute;p bạn tiết kiệm điện v&agrave; kh&ocirc;ng g&acirc;y tổn hại cho mắt, để bạn c&oacute; thể an t&acirc;m giải tr&iacute;. Tivi LED TCL L32D2700 với &aacute;nh s&aacute;ng tự nhi&ecirc;n kh&ocirc;ng g&acirc;y hại mắt</p>\r\n\r\n<p><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-04.jpg?v=1454322114487" /></p>\r\n\r\n<p>Tivi LED TCL L32D2700 sử dụng &aacute;nh s&aacute;ng tự nhi&ecirc;n</p>\r\n\r\n<p>&Acirc;m thanh v&ograve;m sống động Tivi LED TCL L32D2700 sử dụng hệ thống 2 loa tổng c&ocirc;ng suất 10W c&ugrave;ng với khả năng giả lập &acirc;m thanh v&ograve;m, sẽ đem đến sự trọn vẹn khi bạn thưởng thức &acirc;m nhạc hay xem phim.</p>\r\n\r\n<p><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-05.jpg?v=1454322158124" /></p>\r\n\r\n<p>Tivi LED TCL L32D2700 cho &acirc;m thanh sống động</p>\r\n\r\n<p><strong>L&yacute; do mua h&agrave;ng</strong>:</p>\r\n\r\n<ul>\r\n	<li>Sản phẩm c&oacute; mức gi&aacute; rất l&agrave; cạnh tranh, dễ d&agrave;ng đi v&agrave;o l&ograve;ng người ti&ecirc;u d&ugrave;ng Việt Nam.</li>\r\n	<li>Tivi m&agrave;n h&igrave;nh 32 inches độ ph&acirc;n giải HD cho khả năng hiển thị h&igrave;nh ảnh sắc n&eacute;t.</li>\r\n	<li>Giả lập &acirc;m thanh v&ograve;m sống động.</li>\r\n	<li>C&ocirc;ng nghệ &aacute;nh s&aacute;ng tự nhi&ecirc;n độc đ&aacute;o.<br />\r\n	&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>Ưu đ&atilde;i mua h&agrave;ng:</strong></p>\r\n\r\n<ul>\r\n	<li>Để đặt mua Tivi LED TCL L32D2700, bạn c&oacute; thể gọi ngay đến tổng đ&agrave;i 1900 1267 bấm ph&iacute;m số 1.</li>\r\n	<li>Bạn cũng c&oacute; thể y&ecirc;u cầu nh&acirc;n vi&ecirc;n&nbsp;gọi lại để tiết kiệm chi ph&iacute; điện thoại.</li>\r\n	<li>Tham khảo những chương tr&igrave;nh khuyến m&atilde;i hiện c&oacute; tại đ&acirc;y.</li>\r\n	<li>Lu&ocirc;n c&oacute; ch&iacute;nh s&aacute;ch hậu m&atilde;i kh&aacute;ch h&agrave;ng chu đ&aacute;o.</li>\r\n</ul>\r\n', '<p>Thiết kế hiện đại Tivi LED TCL L32D2700 được thiết kế với d&aacute;ng vẻ hiện đại, cứng c&aacute;p, đem lại cho căn ph&ograve;ng bạn th&ecirc;m phần nổi bật. Ch&acirc;n đế chắc chắn với kiểu d&aacute;ng h&ograve;a hợp với th&acirc;n, tạo c&aacute;i nh&igrave;n th&acirc;n thiện.</p>\r\n\r\n<p class = "text-center"><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-03-f403796e-c276-4bc5-a950-b7c3d65b39bd.jpg?v=1454322011580" /></p>\r\n\r\n<p class = "text-center"><em>Tivi LED TCL L32D2700 với thiết kế hiện đại</em></p>\r\n\r\n<p>M&agrave;n h&igrave;nh 32 inches độ ph&acirc;n giải HD Tivi LED TCL L32D2700 trang bị m&agrave;n h&igrave;nh với k&iacute;ch 32 inches c&ugrave;ng độ ph&acirc;n giải HD, đem đến khả năng hiển thị h&igrave;nh ảnh tốt, đ&aacute;p ứng nhu cầu giải tr&iacute; trong ph&ograve;ng kh&aacute;ch cỡ nhỏ hay ph&ograve;ng ngủ.</p>\r\n\r\n<p class = "text-center"><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-02.jpg?v=1453801783267" /></p>\r\n\r\n<p class = "text-center">Tivi LED TCL L32D2700 trang bị m&agrave;n h&igrave;nh 32 inches</p>\r\n\r\n<p>&Aacute;nh s&aacute;ng tự nhi&ecirc;n Tivi LED TCL L32D2700 c&oacute; t&iacute;nh năng chuyển sang chế độ &aacute;nh s&aacute;ng tự nhi&ecirc;n tiện lợi, sẽ gi&uacute;p bạn tiết kiệm điện v&agrave; kh&ocirc;ng g&acirc;y tổn hại cho mắt, để bạn c&oacute; thể an t&acirc;m giải tr&iacute;. Tivi LED TCL L32D2700 với &aacute;nh s&aacute;ng tự nhi&ecirc;n kh&ocirc;ng g&acirc;y hại mắt</p>\r\n\r\n<p class = "text-center"><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-04.jpg?v=1454322114487" /></p>\r\n\r\n<p class = "text-center">Tivi LED TCL L32D2700 sử dụng &aacute;nh s&aacute;ng tự nhi&ecirc;n</p>\r\n\r\n<p>&Acirc;m thanh v&ograve;m sống động Tivi LED TCL L32D2700 sử dụng hệ thống 2 loa tổng c&ocirc;ng suất 10W c&ugrave;ng với khả năng giả lập &acirc;m thanh v&ograve;m, sẽ đem đến sự trọn vẹn khi bạn thưởng thức &acirc;m nhạc hay xem phim.</p>\r\n\r\n<p class = "text-center"><img src="https://bizweb.dktcdn.net/100/052/691/files/000000000010012467-tivi-tcl-d2700-05.jpg?v=1454322158124" /></p>\r\n\r\n<p>Tivi LED TCL L32D2700 cho &acirc;m thanh sống động</p>\r\n\r\n<p class = "text-center"><strong>L&yacute; do mua h&agrave;ng</strong>:</p>\r\n\r\n<ul>\r\n	<li>Sản phẩm c&oacute; mức gi&aacute; rất l&agrave; cạnh tranh, dễ d&agrave;ng đi v&agrave;o l&ograve;ng người ti&ecirc;u d&ugrave;ng Việt Nam.</li>\r\n	<li>Tivi m&agrave;n h&igrave;nh 32 inches độ ph&acirc;n giải HD cho khả năng hiển thị h&igrave;nh ảnh sắc n&eacute;t.</li>\r\n	<li>Giả lập &acirc;m thanh v&ograve;m sống động.</li>\r\n	<li>C&ocirc;ng nghệ &aacute;nh s&aacute;ng tự nhi&ecirc;n độc đ&aacute;o.<br />\r\n	&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>Ưu đ&atilde;i mua h&agrave;ng:</strong></p>\r\n\r\n<ul>\r\n	<li>Để đặt mua Tivi LED TCL L32D2700, bạn c&oacute; thể gọi ngay đến tổng đ&agrave;i 1900 1267 bấm ph&iacute;m số 1.</li>\r\n	<li>Bạn cũng c&oacute; thể y&ecirc;u cầu nh&acirc;n vi&ecirc;n&nbsp;gọi lại để tiết kiệm chi ph&iacute; điện thoại.</li>\r\n	<li>Tham khảo những chương tr&igrave;nh khuyến m&atilde;i hiện c&oacute; tại đ&acirc;y.</li>\r\n	<li>Lu&ocirc;n c&oacute; ch&iacute;nh s&aacute;ch hậu m&atilde;i kh&aacute;ch h&agrave;ng chu đ&aacute;o.</li>\r\n</ul>\r\n', 'SAMSUNG|Smart TV|49 Inch', 100, 100, 0, 20900000, 0, '2016-10-14 00:10:03', 'Ngô Trung Phát', '2016-11-14 15:03:06', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(175, 21, 'Tủ lạnh SS RF56K90 564 lít', 'tu-lanh-ss-rf56k90-564-lit', 'tl-1.jpg#tl-2.jpg', '<table style="height:293px; width:601px">\r\n	<tbody>\r\n		<tr>\r\n			<td>Samsung</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Xuất Xứ</th>\r\n			<td>Ch&iacute;nh h&atilde;ng</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Bảo h&agrave;nh</th>\r\n			<td>02 Năm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Loại Tủ</th>\r\n			<td>Tủ bốn c&aacute;nh</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&agrave;u Sắc</th>\r\n			<td>Black Caviar</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Khoảng dung t&iacute;ch</th>\r\n			<td>600 - 700 l&iacute;t</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dung t&iacute;ch tổng</th>\r\n			<td>633 l&iacute;t</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Chất liệu khay</th>\r\n			<td>Khay k&iacute;nh chịu lực</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Bảng điều khiển v&agrave; hiển thị</th>\r\n			<td>LED Display</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Trọng lượng</th>\r\n			<td>153</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&aacute;c t&iacute;nh năng kh&aacute;c</th>\r\n			<td>\r\n			<ul>\r\n				<li>3 d&agrave;n lạnh độc lập</li>\r\n				<li>Ngăn chứa thực phẩm lớn hơn</li>\r\n				<li>Thiết kế vượt thời gian</li>\r\n				<li>4 Chế Độ Chuyển Đổi Nhiệt Độ Cool Select Zone</li>\r\n				<li>Bảo Quản Thực Phẩm Tươi Ngon FreshZone</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '<table style="height:293px; width:601px">\r\n	<tbody>\r\n		<tr>\r\n			<td>Samsung</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Xuất Xứ</strong></th>\r\n			<td>Ch&iacute;nh h&atilde;ng</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Bảo h&agrave;nh</strong></th>\r\n			<td>02 Năm</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Loại Tủ</strong></th>\r\n			<td>Tủ bốn c&aacute;nh</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>M&agrave;u Sắc</strong></th>\r\n			<td>Black Caviar</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Khoảng dung t&iacute;ch</strong></th>\r\n			<td>600 - 700 l&iacute;t</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Dung t&iacute;ch tổng</strong></th>\r\n			<td>633 l&iacute;t</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Chất liệu khay</strong></th>\r\n			<td>Khay k&iacute;nh chịu lực</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Bảng điều khiển v&agrave; hiển thị</strong></th>\r\n			<td>LED Display</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>Trọng lượng</strong></th>\r\n			<td>153</td>\r\n		</tr>\r\n		<tr>\r\n			<th><strong>C&aacute;c t&iacute;nh năng kh&aacute;c</strong></th>\r\n			<td>\r\n			<ul>\r\n				<li>3 d&agrave;n lạnh độc lập</li>\r\n				<li>Ngăn chứa thực phẩm lớn hơn</li>\r\n				<li>Thiết kế vượt thời gian</li>\r\n				<li>4 Chế Độ Chuyển Đổi Nhiệt Độ Cool Select Zone</li>\r\n				<li>Bảo Quản Thực Phẩm Tươi Ngon FreshZone</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', 9, 9, 0, 45990000, 45990000, '2016-11-14 16:14:42', 'Ngô Trung Phát', '2016-11-22 17:58:47', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(176, 28, 'Điều hòa 1 chiều Midea 9000 BTU', 'dieu-hoa-1-chieu-midea-9000-btu', 'q1.jpg#q2.jpg', '<p>- H&atilde;ng sản xuất: Midea<br />\r\n- Xuất xứ: Li&ecirc;n doanh<br />\r\n- Kiểu: 2 khối - 1 chiều lạnh<br />\r\n- C&ocirc;ng suất lạnh: 9.000BTU/h<br />\r\n- Chế độ Turbo. &ETH;&egrave;n LED hiển thị. Chức năng tự khởi động lại.<br />\r\n- Bộ lọc sinh học. Chế độ tạo lonizer.<br />\r\n- Chức năng l&agrave;m gi&agrave;u Oxy, vận h&agrave;nh khi ngủ.<br />\r\n- C&ocirc;ng nghệ hai hướng gi&oacute; trực tiếp. C&acirc;n bằng nhiệt độ t&ugrave;y chọn<br />\r\n- Bảo h&agrave;nh: 12 th&aacute;ng</p>\r\n', '', '', 20, 20, 0, 5490000, 5490000, '2016-12-03 16:49:52', 'Ngô Trung Phát', '2016-12-03 16:49:52', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(177, 28, 'Điều hòa 2 chiều Midea MS11D1-12HR 12000 btu', 'dieu-hoa-2-chieu-midea-ms11d1-12hr-12000-btu', 'w1.jpg', '<p>- H&atilde;ng sản xuất: Midea<br />\r\n- Xuất xứ: Li&ecirc;n doanh<br />\r\n- Kiểu: 2 khối - 2 chiều n&oacute;ng/ lạnh<br />\r\n- C&ocirc;ng suất lạnh: 12.000BTU/h<br />\r\n- Chế độ Turbo. &ETH;&egrave;n LED hiển thị. Chức năng tự khởi động lại.<br />\r\n- Bộ lọc sinh học. Chế độ tạo lonizer.<br />\r\n- Chức năng l&agrave;m gi&agrave;u Oxy, vận h&agrave;nh khi ngủ.<br />\r\n- C&ocirc;ng nghệ hai hướng gi&oacute; trực tiếp. C&acirc;n bằng nhiệt độ t&ugrave;y chọn<br />\r\n- Bảo h&agrave;nh: 12 th&aacute;ng</p>\r\n', '', '', 15, 15, 0, 7490000, 7490000, '2016-12-03 17:13:06', 'Ngô Trung Phát', '2016-12-03 17:13:06', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(178, 28, 'Điều hòa Daikin FTKC35QVMV 12000 BTU 1 chiều', 'dieu-hoa-daikin-ftkc35qvmv-12000-btu-1-chieu', 'e1.jpg#e2.jpg#e3.jpg#e4.jpg#e5.jpg#e6.jpg', '<ul>\r\n	<li>C&ocirc;ng suất l&agrave;m lạnh :&nbsp;12000 BTU</li>\r\n	<li>Loại Điều h&ograve;a&nbsp;&nbsp;: Điều h&ograve;a 1 chiều lạnh !</li>\r\n	<li>Diện t&iacute;ch&nbsp;l&agrave;m lạnh hiệu quả:Dưới 15 - 20 m2 (từ 30 đến 45 m3)</li>\r\n	<li>C&ocirc;ng nghệ Inverter:M&aacute;y lạnh Inverter</li>\r\n	<li>Loại m&aacute;y:Điều ho&agrave; 1 chiều (chỉ l&agrave;m lạnh)</li>\r\n	<li>Ti&ecirc;u thụ điện:Khoảng 0.95&nbsp;kW/h</li>\r\n	<li>T&iacute;nh năng</li>\r\n	<li>Chế độ tiết kiệm điện:Econo tiết kiệm điện</li>\r\n</ul>\r\n', '', '', 15, 15, 0, 13090000, 13090000, '2016-12-03 17:14:55', 'Ngô Trung Phát', '2016-12-03 17:14:55', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(179, 19, 'Máy giặt Samsung WA72H4000SG/SV 7.2 kg', 'may-giat-samsung-wa72h4000sg-sv-7-2-kg', 'r1.jpg', '<p>Th&ocirc;ng số kỹ thuật</p>\r\n\r\n<ul>\r\n	<li>Th&ocirc;ng tin chung</li>\r\n	<li>Loại m&aacute;y giặt:Cửa tr&ecirc;n</li>\r\n	<li>Lồng giặt:Lồng đứng</li>\r\n	<li>Khối lượng giặt:7.2 Kg</li>\r\n	<li>Tốc độ quay vắt:700 v&ograve;ng/ph&uacute;t</li>\r\n	<li>Lượng nước ti&ecirc;u thụ chuẩn:Khoảng 155 l&iacute;t một lần giặt thường</li>\r\n	<li>Hiệu suất sử dụng điện:12 Wh/kg</li>\r\n	<li>Kiểu động cơ:Truyền động d&acirc;y Curoa</li>\r\n	<li>Inverter:Kh&ocirc;ng</li>\r\n	<li>C&ocirc;ng nghệ giặt</li>\r\n	<li>Chương tr&igrave;nh hoạt động:6 chương tr&igrave;nh (c&oacute; giặt tiết kiệm, giặt chăn mền, giặt đồ mỏng...)</li>\r\n	<li>C&ocirc;ng nghệ giặt:Bộ lọc xơ vải Magic Filter, Lồng giặt thiết kế kim cương, M&acirc;m giặt Wobble tạo luồng nước đa chiều</li>\r\n	<li>Tiện &iacute;ch:Kh&oacute;a trẻ em, Vệ sinh lồng giặt, Vắt cực kh&ocirc;</li>\r\n	<li>Tổng quan</li>\r\n	<li>Chất liệu lồng giặt:Th&eacute;p kh&ocirc;ng gỉ</li>\r\n	<li>Chất liệu vỏ m&aacute;y:Kim loại sơn tĩnh điện</li>\r\n	<li>Chất liệu nắp m&aacute;y:K&iacute;nh chịu lực</li>\r\n	<li>Bảng điều khiển:Tiếng Việt n&uacute;t nhấn c&oacute; m&agrave;n h&igrave;nh hiển thị</li>\r\n	<li>Số người sử dụng:Dưới 4 người (Dưới 7.5 kg)</li>\r\n	<li>K&iacute;ch thước - Khối lượng:Cao 85 cm - Ngang 54 cm - S&acirc;u 56 cm - Nặng 31 kg</li>\r\n	<li>Nơi sản xuất:Th&aacute;i Lan</li>\r\n	<li>Năm sản xuất:2014</li>\r\n</ul>\r\n', '', '', 15, 15, 0, 3890000, 3890000, '2016-12-03 17:17:02', 'Ngô Trung Phát', '2016-12-03 17:17:02', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(180, 19, 'Máy giặt Aqua AQW-QW80ZT 8.0 kg', 'may-giat-aqua-aqw-qw80zt-8-0-kg', 't1.jpg', '<ul>\r\n	<li>Th&ocirc;ng tin chung</li>\r\n	<li>Loại m&aacute;y giặt:Cửa tr&ecirc;n</li>\r\n	<li>Lồng giặt:Lồng đứng</li>\r\n	<li>Khối lượng giặt:8 Kg</li>\r\n	<li>Kiểu động cơ:Truyền động d&acirc;y Curoa</li>\r\n	<li>Inverter:Kh&ocirc;ng</li>\r\n	<li>C&ocirc;ng nghệ giặt</li>\r\n	<li>Chương tr&igrave;nh hoạt động:10 Chương tr&igrave;nh</li>\r\n	<li>Tổng quan</li>\r\n	<li>Chất liệu lồng giặt:Th&eacute;p kh&ocirc;ng gỉ</li>\r\n	<li>Chất liệu vỏ m&aacute;y:Kim loại sơn tĩnh điện</li>\r\n	<li>Chất liệu nắp m&aacute;y:Nhựa</li>\r\n	<li>Số người sử dụng:4 - 5 người (7.5 - 8.5 kg)</li>\r\n	<li>K&iacute;ch thước - Khối lượng:100 x 60 x 57 cm</li>\r\n	<li>Nơi sản xuất:Việt Nam</li>\r\n	<li>Năm sản xuất:2015</li>\r\n</ul>\r\n', '', '', 15, 15, 0, 5290000, 5290000, '2016-12-03 17:17:46', 'Ngô Trung Phát', '2016-12-03 17:17:46', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(182, 19, 'Máy giặt Samsung WD10K6410OS 10.5Kg sấy 6.0Kg', 'may-giat-samsung-wd10k6410os-10-5kg-say-6-0kg', 'u.png', '<table style="width:590px">\r\n	<tbody>\r\n		<tr>\r\n			<td>Loại m&aacute;y giặt</td>\r\n			<td>M&aacute;y giặt cửa cửa trước</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Lồng giặt</td>\r\n			<td>Lồng ngang</td>\r\n		</tr>\r\n		<tr>\r\n			<td>H&atilde;ng sản xuất</td>\r\n			<td>Samsung</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Xuất xứ</td>\r\n			<td>Việt Nam</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bảo h&agrave;nh</td>\r\n			<td>2 năm ( Động cơ 11 năm)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Điện năng ti&ecirc;u thụ</td>\r\n			<td>500 - 2400 W/h</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Khối lượng giặt</td>\r\n			<td>10.5 kg giặt / 6kg sấy</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Kh&aacute;ng khuẩn, khử m&ugrave;i</td>\r\n			<td>Air Wash</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '', 15, 15, 0, 22990000, 22990000, '2016-12-03 17:20:12', 'Ngô Trung Phát', '2016-12-03 17:20:12', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(184, 19, 'Máy giặt Toshiba AW-A800SV 7kg', 'may-giat-toshiba-aw-a800sv-7kg', 'o1.jpg#o2.png#o3.png#o4.jpg', '<table style="height:444px">\r\n	<tbody>\r\n		<tr>\r\n			<th>H&atilde;ng</th>\r\n			<td>Toshiba</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Xuất Xứ</th>\r\n			<td>Th&aacute;i Lan</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Bảo h&agrave;nh</th>\r\n			<td>12 Th&aacute;ng</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&agrave;u Sắc</th>\r\n			<td>Xanh dương (WB)/ Hồng (WL)/ X&aacute;m (WG)/ T&iacute;m (WV)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Thời gian giặt</th>\r\n			<td>55 ph&uacute;t</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Khoảng khối lượng giặt</th>\r\n			<td>7 - 9 Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Khối lượng giặt</th>\r\n			<td>7kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Động cơ chuyển động</th>\r\n			<td>D&acirc;y curoa</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Kiểu m&aacute;y</th>\r\n			<td>M&aacute;y giặt lồng đứng</td>\r\n		</tr>\r\n		<tr>\r\n			<th>&Aacute;p lực nước</th>\r\n			<td>1.5m</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tốc độ vắt</th>\r\n			<td>700 (v&ograve;ng/ph&uacute;t)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Chương tr&igrave;nh giặt</th>\r\n			<td>4 chương tr&igrave;nh giặt tự động</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&ocirc;ng suất ti&ecirc;u thụ nước</th>\r\n			<td>135l&iacute;t</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Điện năng ti&ecirc;u thụ</th>\r\n			<td>355/395w</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Trọng lượng</th>\r\n			<td>27kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>K&iacute;ch thước (RxSxC)</th>\r\n			<td>905 x 555 x 590mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&aacute;c t&iacute;nh năng kh&aacute;c</th>\r\n			<td>\r\n			<ul>\r\n				<li>Điều chỉnh luồng nước</li>\r\n				<li>Hiệu ứng th&aacute;c nước đ&ocirc;i</li>\r\n				<li>M&acirc;m giặt xoay chiều W</li>\r\n				<li>Giặt c&ocirc; đặc bằng bọt kh&iacute;</li>\r\n				<li>Vắt cực kh&ocirc; với hệ thống h&uacute;t kh&iacute; v&ograve;ng cung</li>\r\n				<li>M&acirc;m giặt kh&aacute;ng khuẩn: Powerful Hybrid﻿</li>\r\n				<li>Kỹ thuật Fuzzy logic</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<th>D&ograve;ng sản phẩm</th>\r\n			<td>Th&ocirc;ng dụng</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '', 15, 15, 0, 3890000, 3890000, '2016-12-03 17:22:47', 'Ngô Trung Phát', '2016-12-03 17:22:47', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(185, 19, 'Máy giặt Sanyo AWD-D750T 7.5kg', 'may-giat-sanyo-awd-d750t-7-5kg', 'p1.jpg', '<ul>\r\n	<li>Kiểu m&aacute;y giặt&nbsp;Cửa trước</li>\r\n	<li>Khối lượng giặt - sấy&nbsp;7.5kg</li>\r\n	<li>Số lượng chương tr&igrave;nh&nbsp;9 chương tr&igrave;nh giặt</li>\r\n	<li>Hẹn giờ&nbsp;C&oacute;</li>\r\n	<li>C&ocirc;ng nghệ Inverter&nbsp;C&oacute;</li>\r\n	<li>Chất liệu lồng giặt&nbsp;Th&eacute;p kh&ocirc;ng gỉ&nbsp;</li>\r\n	<li>Th&ocirc;ng tin chung về M&aacute;y giặt</li>\r\n	<li>M&agrave;u sắc&nbsp;Trắng</li>\r\n	<li>Xuất xứ thương hiệu&nbsp;Nhật Bản</li>\r\n	<li>Sản xuất tại&nbsp;Việt Nam</li>\r\n	<li>Bảo h&agrave;nh&nbsp;24 th&aacute;ng</li>\r\n</ul>\r\n', '', '', 20, 20, 0, 7690000, 7690000, '2016-12-03 17:23:49', 'Ngô Trung Phát', '2016-12-03 17:23:49', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(186, 19, 'Máy Giặt Samsung 17kg giặt 9kg sấy', 'may-giat-samsung-17kg-giat-9kg-say', 'a1.jpg#a2.jpg', '<table border="0" cellpadding="0" cellspacing="0" style="height:201px">\r\n	<tbody>\r\n		<tr>\r\n			<td>Khối lượng giặt</td>\r\n			<td>17.0KG</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Khối lượng sấy</td>\r\n			<td>9.0KG</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Tốc độ quay tối đa (RPM)</td>\r\n			<td>1100</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Động cơ</td>\r\n			<td>Direct Drive</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Giặt bổ sung - Add Wash</td>\r\n			<td>C&oacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>C&ocirc;ng nghệ giặt bong b&oacute;ng - Eco Bubble</td>\r\n			<td>C&oacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td rowspan="5">Thiết kế</td>\r\n			<td>Thiết kế cửa</td>\r\n			<td>X&aacute;m Inox</td>\r\n		</tr>\r\n		<tr>\r\n			<td>M&agrave;u th&acirc;n m&aacute;y</td>\r\n			<td>Pha l&ecirc; xanh (AW)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Lồng giặt</td>\r\n			<td>Lồng giặt kim cương</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Hai đường nước v&agrave;o</td>\r\n			<td>N&oacute;ng/Lạnh</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Hiển thị điều khiển</td>\r\n			<td>LED</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '', 14, 14, 0, 32990000, 32990000, '2016-12-03 17:25:01', 'Ngô Trung Phát', '2016-12-03 17:25:01', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(187, 19, 'MÁY GIẶT SANYO AQUA DQ900HT CỬA TRÊN 9KG', 'may-giat-sanyo-aqua-dq900ht-cua-tren-9kg', 's1.jpg#s2.jpg#s3.jpg#s4.jpg#s5.jpg#s51.jpg#s6.jpg#s7.jpg#s8.jpg#s9.jpg#s10.jpg#s11.jpg#s12.jpg#s13.jpg#s14.jpg#s15.jpg', '<ul>\r\n	<li>Th&ocirc;ng tin chung</li>\r\n	<li>Loại m&aacute;y giặt:Cửa tr&ecirc;n</li>\r\n	<li>Lồng giặt:Lồng nghi&ecirc;ng</li>\r\n	<li>Khối lượng giặt:9 Kg</li>\r\n	<li>Tốc độ quay vắt:750 v&ograve;ng/ph&uacute;t</li>\r\n	<li>Lượng nước ti&ecirc;u thụ chuẩn:Khoảng 130 l&iacute;t một lần giặt thường</li>\r\n	<li>Hiệu suất sử dụng điện:10.1 Wh/kg</li>\r\n	<li>Kiểu động cơ:Truyền động trực tiếp bền &amp; &ecirc;m</li>\r\n	<li>Inverter:C&oacute;</li>\r\n	<li>C&ocirc;ng nghệ giặt</li>\r\n	<li>Chương tr&igrave;nh hoạt động:9 Chương tr&igrave;nh (c&oacute; giặt nhanh, giặt mạnh, giặt chăn mền...)</li>\r\n	<li>C&ocirc;ng nghệ giặt:Giặt nhiều luồng nước phun MultiJet, Giặt s&oacute;ng si&ecirc;u &acirc;m, Diệt khuẩn khử m&ugrave;i Nano Ag+</li>\r\n	<li>Tiện &iacute;ch:Tự khởi động lại khi c&oacute; điện, Kh&oacute;a trẻ em, Vắt cực kh&ocirc;</li>\r\n	<li>Tổng quan</li>\r\n	<li>Chất liệu lồng giặt:Th&eacute;p kh&ocirc;ng gỉ</li>\r\n	<li>Chất liệu vỏ m&aacute;y:Kim loại sơn tĩnh điện</li>\r\n	<li>Chất liệu nắp m&aacute;y:Nhựa</li>\r\n	<li>Bảng điều khiển:Tiếng Việt n&uacute;t nhấn</li>\r\n	<li>Số người sử dụng:Từ tr&ecirc;n 6 người (Tr&ecirc;n 8.5 kg)</li>\r\n	<li>K&iacute;ch thước - Khối lượng:Cao 99 cm - Ngang 58.9 cm - S&acirc;u 67 cm - Nặng 40 kg</li>\r\n	<li>Nơi sản xuất:Việt Nam</li>\r\n</ul>\r\n', '', '', 10, 10, 17, 8990000, 7490000, '2016-12-03 17:30:45', 'Ngô Trung Phát', '2016-12-03 17:30:45', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(190, 43, 'Điện thoại OPPO F1s', 'dien-thoai-oppo-f1s', 'a1.png#a21.jpg#a3.jpg#a4.jpg#a5.jpg#a6.jpg#a7.jpg#a8.jpg#a9.jpg#a10.jpg#a11.jpg#a12.jpg#a13.jpg#a14.jpg#a15.jpg#a16.jpg#a17.jpg', '<p><img alt="" src="/minimart/public/uploadimages/a.jpg" style="height:500px; width:590px" /></p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a20.PNG" style="height:701px; width:590px" /></p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a21.PNG" style="height:469px; width:590px" /></p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a22.PNG" style="height:739px; width:590px" /></p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a24.PNG" style="height:648px; width:590px" /></p>\r\n\r\n<h2>OPPO F1s sẽ l&agrave; chiếc điện thoại tiếp theo được&nbsp;<a href="https://www.thegioididong.com/dtdd-oppo" target="_blank">OPPO&nbsp;</a>giới thiệu tại Việt Nam nhằm đ&aacute;nh v&agrave;o sở th&iacute;ch selfie của giới trẻ với điểm nhấn l&agrave; camera trước c&oacute; độ ph&acirc;n giải l&ecirc;n tới 16 MP.</h2>\r\n\r\n<h3><strong>Thiết kế cao cấp</strong></h3>\r\n\r\n<p>Nh&igrave;n tổng quan th&igrave;&nbsp;OPPO F1s sẽ mang trong m&igrave;nh thiết kế tương tự như người anh em&nbsp;<a href="https://www.thegioididong.com/dtdd/oppo-f1-plus" target="_blank">OPPO F1 Plus</a>với chất liệu kim loại cao cấp. F1s cũng sẽ sở hữu cho m&igrave;nh phần viền m&agrave;n h&igrave;nh 2 b&ecirc;n si&ecirc;u mỏng với độ mỏng chỉ 1.55 mm.</p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a25.jpg" style="height:321px; width:640px" /></p>\r\n\r\n<p>C&aacute;c g&oacute;c cạnh của F1s cũng được bo cong mềm mại đem lại cho người d&ugrave;ng cảm gi&aacute;c cầm nắm kh&aacute; thoải m&aacute;i c&ugrave;ng phần viền m&agrave;n h&igrave;nh được ho&agrave;n thiện cong 2.5D mang lại trải nghiệm sử dụng tốt hơn.</p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a26.jpg" style="height:345px; width:640px" /></p>\r\n\r\n<p>M&aacute;y c&oacute; m&agrave;n h&igrave;nh 5.5 inch, độ ph&acirc;n giải HD. Theo th&ocirc;ng tin tiết lộ th&igrave; m&aacute;y sẽ c&oacute; 2 m&agrave;u rất thời trang l&agrave; m&agrave;u v&agrave;ng v&agrave; m&agrave;u hồng thời thượng.</p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/s27.jpg" style="height:318px; width:640px" /></p>\r\n\r\n<h3><strong>Camera si&ecirc;u n&eacute;t</strong></h3>\r\n\r\n<p>Với camera trước c&oacute; độ ph&acirc;n giải lớn l&ecirc;n tới 16 MP th&igrave; OPPO kh&ocirc;ng giấu tham vọng đ&aacute;nh mạnh v&agrave;o giới trẻ với nhu cầu tự sướng cao. Với chất lượng ảnh selfie từ c&aacute;c m&aacute;y OPPO trước đ&acirc;y lu&ocirc;n được người d&ugrave;ng đ&aacute;nh gi&aacute; cao th&igrave; hi vọng camera tự sướng của chiếc F1s sẽ một lần nữa khiến thị trường nhộn nhịp trở lại.</p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a28.jpg" style="height:318px; width:640px" /></p>\r\n\r\n<p>Ngo&agrave;i ra m&aacute;y cũng được trang bị camera ch&iacute;nh độ ph&acirc;n giải 13 MP&nbsp;với t&iacute;nh năng tự động lấy n&eacute;t theo giai đoạn (PDAF) v&agrave; chống rung th&ocirc;ng minh cho ph&eacute;p chụp ảnh hay lấy n&eacute;t cực nhanh v&agrave; ch&iacute;nh x&aacute;c, để chụp n&ecirc;n những bức ảnh sắc n&eacute;t chỉ trong t&iacute;ch tắc.</p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a29.jpg" style="height:407px; width:640px" /></p>\r\n\r\n<h3><strong>Hiệu năng mượt m&agrave;</strong></h3>\r\n\r\n<p>OPPO F1s được cung cấp sức mạnh từ con chip&nbsp;MediaTek MT6750 8 nh&acirc;n 1,5 GHz, 3 GB RAM, bộ nhớ trong 32 GB.&nbsp;Tuy nhi&ecirc;n, chỉ tiếc l&agrave; hệ điều h&agrave;nh ColorOS 3.0, dựa tr&ecirc;n nền Android 5.1 Lollipop c&oacute; vẻ hơi cũ khi m&agrave; Android 7 chuẩn bị ra mắt.</p>\r\n\r\n<p>Hi vọng OPPO sẽ nhanh ch&oacute;ng đưa ra bản cập nhật cho F1s.</p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a30.jpg" style="height:316px; width:640px" /></p>\r\n\r\n<h3><strong>V&acirc;n tay si&ecirc;u nhạy</strong></h3>\r\n\r\n<p>Theo những g&igrave; OPPO giới thiệu th&igrave; F1s sẽ chỉ mất&nbsp;0.22 gi&acirc;y để c&oacute; thể nhận ra v&acirc;n tay của bạn khi mở kh&oacute;a thiết bị, một con số kh&aacute; ấn tượng với một chiếc m&aacute;y tầm trung. Với việc t&iacute;ch hợp cảm biến v&acirc;n tay th&igrave; OPPO F1s sẽ gi&uacute;p bạn bảo mật dữ liệu an to&agrave;n hơn, mở kh&oacute;a thiết bị nhanh ch&oacute;ng.</p>\r\n\r\n<p><img alt="" src="/minimart/public/uploadimages/a31.jpg" style="height:318px; width:640px" /></p>\r\n\r\n<p>Với những điểm mạnh kh&aacute; ấn tượng th&igrave; ch&uacute;ng ta c&ugrave;ng chờ đợi sự ra mắt của&nbsp;<strong>OPPO F1s</strong>&nbsp;v&agrave; ng&agrave;y m&aacute;y sẽ ch&iacute;nh thức được l&ecirc;n kệ để c&oacute; thể sở hữu ngay cho m&igrave;nh một chiếc.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '', 100, 100, 0, 5900000, 5900000, '2016-12-03 17:54:46', 'Ngô Trung Phát', '2016-12-03 17:54:46', 'Ngô Trung Phát', 1, 1, 1, '', '');
INSERT INTO `db_product` (`id`, `catid`, `name`, `alias`, `img`, `detail`, `introtext`, `tag`, `number`, `number_buy`, `sale`, `price`, `price_sale`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`, `metakey`, `metadesc`) VALUES
(191, 43, 'iPhone 7 Plus 32GB', 'iphone-7-plus-32gb', 'b1.jpg#b2.jpg#b3.jpg#b4.jpg#b5.jpg#b6.jpg#b7.jpg#b8.jpg#b9.jpg#b10.jpg#b11.jpg#b12.jpg#b13.jpg#b14.jpg#b15.jpg#b16.jpg#b17.jpg#b18.jpg#b19.jpg#b20.jpg', '<h3>Th&ocirc;ng số kỹ thuật</h3>\r\n\r\n<ul>\r\n	<li>Th&ocirc;ng số cơ bản</li>\r\n	<li>M&agrave;n h&igrave;nh :5.5 inch (1920 x 1080 pixels)</li>\r\n	<li>Camera :Ch&iacute;nh: Dual Camera 12.0MP; Phụ: 7.0MP</li>\r\n	<li>Bộ nhớ trong :32 GB</li>\r\n	<li>Hệ điều h&agrave;nh :iOS 10</li>\r\n	<li>Chipset :Apple A10</li>\r\n	<li>GPU :M10</li>\r\n	<li>K&iacute;ch thước :158.2 x 77.9 x 7.3 mm</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>M&agrave;n h&igrave;nh</li>\r\n	<li>Loại m&agrave;n h&igrave;nh :Retina</li>\r\n	<li>M&agrave;u m&agrave;n h&igrave;nh :16 Triệu m&agrave;u</li>\r\n	<li>Chuẩn m&agrave;n h&igrave;nh :Full HD</li>\r\n	<li>Độ ph&acirc;n giải m&agrave;n h&igrave;nh :1920 x 1080 pixels</li>\r\n	<li>K&iacute;ch thước m&agrave;n h&igrave;nh :5.5 inch</li>\r\n	<li>C&ocirc;ng nghệ cảm ứng :3D Touch</li>\r\n	<li>Chụp h&igrave;nh &amp; Quay phim</li>\r\n	<li>Camera sau :Dual Camera: 12.0 MP</li>\r\n	<li>Camera trước :7.0 MP</li>\r\n	<li>Đ&egrave;n Flash :C&oacute;</li>\r\n	<li>T&iacute;nh năng camera :Ổn định quang học, Panorama, tự động lấy n&eacute;t, live photos, HDR</li>\r\n	<li>Quay Phim :4K 2160p@30fps</li>\r\n	<li>Video Call :C&oacute;</li>\r\n	<li>Cấu h&igrave;nh phần cứng</li>\r\n	<li>Số nh&acirc;n :4</li>\r\n	<li>Chipset :A10</li>\r\n	<li>RAM :3 GB</li>\r\n	<li>Chip đồ họa (GPU) :M10</li>\r\n	<li>Bộ nhớ &amp; Lưu trữ</li>\r\n	<li>Danh bạ lưu trữ :Kh&ocirc;ng giới hạn</li>\r\n	<li>Bộ nhớ trong :32 GB</li>\r\n	<li>Thẻ nhớ ngo&agrave;i :Kh&ocirc;ng</li>\r\n	<li>Hỗ trợ thẻ nhớ tối đa :Kh&ocirc;ng</li>\r\n	<li>Thiết kế &amp; Trọng lượng</li>\r\n	<li>Kiểu d&aacute;ng :Thanh (thẳng) + Cảm ứng</li>\r\n	<li>K&iacute;ch thước :158.2 x 77.9 x 7.3 mm</li>\r\n	<li>Trọng lượng :188 g</li>\r\n	<li>Th&ocirc;ng tin pin</li>\r\n	<li>Dung lượng pin :11.1 Wh (2900 mAh)</li>\r\n	<li>Pin c&oacute; thể th&aacute;o rời :Kh&ocirc;ng</li>\r\n	<li>Kết nối &amp; Cổng giao tiếp</li>\r\n	<li>Băng tần 2G :GSM / EDGE (850, 900, 1800, 1900 MHz)</li>\r\n	<li>Băng tần 3G :UMTS / HSPA + / DC-HSDPA (850, 900, 1700/2100, 1900, 2100 MHz)</li>\r\n	<li>Băng tần 4G :C&oacute;</li>\r\n	<li>Hỗ trợ SIM :Nano SIM</li>\r\n	<li>Khe cắm sim :1 Sim</li>\r\n	<li>Wifi :Wi-Fi 802.11 a/b/g/n/ac</li>\r\n	<li>GPS :A-GPS v&agrave; GLONASS</li>\r\n	<li>Bluetooth :v4.2</li>\r\n	<li>Kết nối USB :Lightning</li>\r\n	<li>Cổng kết nối kh&aacute;c :Kh&ocirc;ng</li>\r\n	<li>Cổng sạc :Lightning</li>\r\n	<li>Jack (Input &amp; Output) :Lightning</li>\r\n	<li>Giải tr&iacute; &amp; Ứng dụng</li>\r\n	<li>Xem phim :C&oacute;</li>\r\n	<li>Nghe nhạc :C&oacute;</li>\r\n	<li>Ghi &acirc;m :C&oacute;</li>\r\n	<li>Chức năng kh&aacute;c :kh&ocirc;ng</li>\r\n	<li>Bảo h&agrave;nh</li>\r\n	<li>Thời gian bảo h&agrave;nh :12 th&aacute;ng</li>\r\n</ul>\r\n\r\n<h2><strong>iPhone 7 Plus 32 GB với bộ đ&ocirc;i camera k&eacute;p ấn tượng c&ugrave;ng ti&ecirc;u chuẩn chống nước lần đầu ti&ecirc;n xuất hiện tr&ecirc;n c&aacute;c thế hệ iPhone sẽ l&agrave; smartphone đ&aacute;ng mua nhất dịp cuối năm.</strong></h2>\r\n\r\n<h3><strong>Thiết kế quen thuộc</strong></h3>\r\n\r\n<p>Thiết kế của chiếc&nbsp;<strong>iPhone 7 Plus 32 GB</strong>&nbsp;kh&ocirc;ng c&oacute; nhiều điểm kh&aacute;c biệt so với người anh em&nbsp;<a href="https://www.thegioididong.com/dtdd/iphone-6s-plus" target="_blank">iPhone 6s Plus</a>&nbsp;năm ngo&aacute;i. Mặt lưng ch&iacute;nh l&agrave; điểm nổi bật nhất của&nbsp;<strong>iPhone 7 Plus 32 GB</strong>&nbsp;với bộ đ&ocirc;i camera k&eacute;p lồi hẳn l&ecirc;n.</p>\r\n\r\n<p><a href="https://www.thegioididong.com/images/42/78124/apple-iphone-7-plus8.jpg" onclick="return false;"><img alt="Thiết kế vẫn tượng tự iPhone 6s Plus" src="https://cdn.tgdd.vn/Products/Images/42/78124/apple-iphone-7-plus8.jpg" /></a></p>\r\n\r\n<p><em>Thiết kế vẫn tượng tự iPhone 6s Plus</em></p>\r\n\r\n<p>Ngo&agrave;i ra th&igrave; Ph&iacute;m Home vật l&yacute; truyền thống tr&ecirc;n c&aacute;c d&ograve;ng iPhone cũng đ&atilde; được Apple loại bỏ v&agrave; thay v&agrave;o đ&oacute; l&agrave; ph&iacute;m Home với với c&ocirc;ng nghệ cảm ứng lực gi&uacute;p bạn sử dụng thiết bị thoải m&aacute;i hơn m&agrave; kh&ocirc;ng sợ bị liệt hay hỏng ph&iacute;m Home.</p>\r\n\r\n<p><a href="https://www.thegioididong.com/images/42/78124/apple-iphone-7-plus4.jpg" onclick="return false;"><img alt="Phím Home với cảm ứng lực" src="https://cdn2.tgdd.vn/Products/Images/42/78124/apple-iphone-7-plus4.jpg" /></a></p>\r\n\r\n<p><em>Ph&iacute;m Home với cảm ứng lực</em></p>\r\n\r\n<p>Một sự thay đổi cũng rất được người d&ugrave;ng ch&uacute; &yacute; ch&iacute;nh l&agrave; jack cắm&nbsp;<a href="https://www.thegioididong.com/tai-nghe" target="_blank">tai nghe</a>&nbsp;3.5 mm đ&atilde; bị loại bỏ v&agrave; thay v&agrave;o đ&oacute; l&agrave; dải loa&nbsp;stereo mang lại chất lượng &acirc;m thanh tốt hơn.</p>\r\n\r\n<p><a href="https://www.thegioididong.com/images/42/78124/apple-iphone-7-plus3.jpg" onclick="return false;"><img alt="Nhiều màu hấp dẫn và thời trang hơn" src="https://cdn4.tgdd.vn/Products/Images/42/78124/apple-iphone-7-plus3.jpg" /></a></p>\r\n\r\n<p><em>Nhiều m&agrave;u hấp dẫn v&agrave; thời trang hơn</em></p>\r\n\r\n<p>M&agrave;n h&igrave;nh của m&aacute;y vẫn giữ nguy&ecirc;n k&iacute;ch thước 5.5 inch độ ph&acirc;n giải&nbsp;1080 x 1920 pixels nhưng được cải tiến về độ s&aacute;ng cũng như độ tương phản mang lại trải nghiệm tốt hơn.</p>\r\n\r\n<h3><strong>Hiệu năng h&agrave;ng đầu</strong></h3>\r\n\r\n<p>Năm ngo&aacute;i khi Apple giới thiệu con chip Apple A9 tr&ecirc;n bộ đ&ocirc;i iPhone 6s v&agrave; 6s Plus th&igrave; người d&ugrave;ng đ&atilde; kh&ocirc;ng khỏi bất ngờ về hiệu năng m&agrave; n&oacute; mang lại.</p>\r\n\r\n<p><a href="https://www.thegioididong.com/images/42/78124/apple-iphone-7-plus2-1.jpg" onclick="return false;"><img alt="iPhone 7 Plus vẫn sẽ là một trong những smartphone mạnh mẽ nhất trên thị trường" src="https://cdn1.tgdd.vn/Products/Images/42/78124/apple-iphone-7-plus2-1.jpg" /></a></p>\r\n\r\n<p><em><strong>iPhone 7 Plus 32 GB</strong><em>vẫn sẽ</em>l&agrave; một trong những smartphone mạnh mẽ nhất tr&ecirc;n thị trường</em></p>\r\n\r\n<p>Năm nay với con chip Apple thế hệ mới m&agrave; theo Apple c&ocirc;ng bố l&agrave; sẽ cho tốc độ CPU nhanh hơn 40% v&agrave; tốc độ GPU nhanh hơn 50% so với thế hệ trước th&igrave; chắc chắn&nbsp;<strong>iPhone 7 Plus 32 GB</strong>&nbsp;sẽ kh&ocirc;ng l&agrave;m người d&ugrave;ng thất vọng.</p>\r\n\r\n<h3><strong>Khả năng chống nước IP67</strong></h3>\r\n\r\n<p>Lần đầu ti&ecirc;n tr&ecirc;n một chiếc iPhone th&igrave; Apple đ&atilde; t&iacute;ch hợp khả năng chống nước v&agrave; chống bụi ti&ecirc;u chuẩn IP67 gi&uacute;p người d&ugrave;ng c&oacute; thể thoải m&aacute;i mang theo chiếc iPhone của m&igrave;nh xuống hồ bơi v&agrave; chụp những bức h&igrave;nh vui vẻ.</p>\r\n\r\n<p><a href="https://www.thegioididong.com/images/42/78124/apple-iphone-7-plus1-1.jpg" onclick="return false;"><img alt="iPhone 7 Plus cũng sẽ hỗ trợ chống nước và chống bụi" src="https://cdn3.tgdd.vn/Products/Images/42/78124/apple-iphone-7-plus1-1.jpg" /></a></p>\r\n\r\n<p><em><strong>iPhone 7 Plus 32 GB</strong>cũng sẽ hỗ trợ chống nước v&agrave; chống bụi</em></p>\r\n\r\n<h3><strong>Bộ đ&ocirc;i camera ấn tượng</strong></h3>\r\n\r\n<p>Với bộ đ&ocirc;i camera k&eacute;p c&oacute; c&ugrave;ng độ ph&acirc;n giải 12 MP&nbsp;v&agrave; một trong số đ&oacute; l&agrave; ống kinh g&oacute;c rộng v&agrave; m&aacute;y ảnh c&ograve;n lại c&oacute; ti&ecirc;u cự 56 mm.&nbsp;Theo Apple hệ thống camera k&eacute;p n&agrave;y cho ph&eacute;p zoom quang 2X m&agrave; kh&ocirc;ng l&agrave;m suy giảm chất lượng ảnh.</p>\r\n\r\n<p>Khi chụp người d&ugrave;ng c&oacute; thể zoom từ 1-10x nhưng từ 2x trở l&ecirc;n l&agrave; zoom số, khi zoom số thuật to&aacute;n xử l&yacute; ảnh sẽ c&ugrave;ng kết hợp h&igrave;nh ảnh từ cả 2 camera để xử l&yacute;, qua đ&oacute; sẽ cho chất lượng cao gấp 4 lần hệ thống thường.</p>\r\n\r\n<p><a href="https://www.thegioididong.com/images/42/78124/apple-iphone-7-plus7.jpg" onclick="return false;"><img alt="Camera kép trên iPhone 7 Plus sẽ tạo nên sự đột phá" src="https://cdn.tgdd.vn/Products/Images/42/78124/apple-iphone-7-plus7.jpg" /></a></p>\r\n\r\n<p><em>Camera k&eacute;p tr&ecirc;n&nbsp;<strong>iPhone 7 Plus 32 GB</strong>&nbsp;sẽ tạo n&ecirc;n sự đột ph&aacute;</em></p>\r\n\r\n<p>Ngo&agrave;i ra th&igrave; Apple cũng sẽ t&iacute;ch hợp khả năng sử dụng đồng thời cả 2 camera để giả lập độ s&acirc;u trường ảnh mang lại cho người d&ugrave;ng những bức ảnh x&oacute;a ph&ocirc;ng ấn tượng.</p>\r\n\r\n<h3><strong>Hệ điều h&agrave;nh mới</strong></h3>\r\n\r\n<p>Đồng thời với việc ra mắt iPhone mới th&igrave;&nbsp;<a href="https://www.thegioididong.com/dtdd-apple-iphone" target="_blank">Apple</a>&nbsp;cũng giới thiệu tới người d&ugrave;ng hệ điều h&agrave;nh iOS 10 ho&agrave;n to&agrave;n mới với nhiều cải tiến mạnh mẽ&nbsp;về giao diện m&agrave;n h&igrave;nh kh&oacute;a, thanh th&ocirc;ng b&aacute;o, Apple Maps, Apple Music, Messages v&agrave; HomeKit.</p>\r\n\r\n<p><a href="https://www.thegioididong.com/images/42/78124/apple-iphone-7-plus5.jpg" onclick="return false;"><img alt="Hệ điều hành iOS 10 hoàn toàn mới" src="https://cdn2.tgdd.vn/Products/Images/42/78124/apple-iphone-7-plus5.jpg" /></a></p>\r\n\r\n<p><em>Hệ điều h&agrave;nh iOS 10 ho&agrave;n to&agrave;n mới</em></p>\r\n\r\n<p>Với những trang bị h&agrave;ng đầu hiện nay th&igrave;&nbsp;<strong>iPhone 7 Plus 32 GB</strong>&nbsp;thực sự l&agrave; 1 flahship đ&aacute;ng sở hữu trong dịp mua sắm cuối năm.</p>\r\n', '', '', 100, 100, 0, 22290000, 22290000, '2016-12-03 18:06:18', 'Ngô Trung Phát', '2016-12-03 18:06:18', 'Ngô Trung Phát', 1, 1, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `db_province`
--

CREATE TABLE IF NOT EXISTS `db_province` (
  `provinceid` varchar(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_province`
--

INSERT INTO `db_province` (`provinceid`, `name`, `type`) VALUES
('01', 'Hà Nội', 'Thành Phố'),
('02', 'Hà Giang', 'Tỉnh'),
('04', 'Cao Bằng', 'Tỉnh'),
('06', 'Bắc Kạn', 'Tỉnh'),
('08', 'Tuyên Quang', 'Tỉnh'),
('10', 'Lào Cai', 'Tỉnh'),
('11', 'Điện Biên', 'Tỉnh'),
('12', 'Lai Châu', 'Tỉnh'),
('14', 'Sơn La', 'Tỉnh'),
('15', 'Yên Bái', 'Tỉnh'),
('17', 'Hòa Bình', 'Tỉnh'),
('19', 'Thái Nguyên', 'Tỉnh'),
('20', 'Lạng Sơn', 'Tỉnh'),
('22', 'Quảng Ninh', 'Tỉnh'),
('24', 'Bắc Giang', 'Tỉnh'),
('25', 'Phú Thọ', 'Tỉnh'),
('26', 'Vĩnh Phúc', 'Tỉnh'),
('27', 'Bắc Ninh', 'Tỉnh'),
('30', 'Hải Dương', 'Tỉnh'),
('31', 'Hải Phòng', 'Thành Phố'),
('33', 'Hưng Yên', 'Tỉnh'),
('34', 'Thái Bình', 'Tỉnh'),
('35', 'Hà Nam', 'Tỉnh'),
('36', 'Nam Định', 'Tỉnh'),
('37', 'Ninh Bình', 'Tỉnh'),
('38', 'Thanh Hóa', 'Tỉnh'),
('40', 'Nghệ An', 'Tỉnh'),
('42', 'Hà Tĩnh', 'Tỉnh'),
('44', 'Quảng Bình', 'Tỉnh'),
('45', 'Quảng Trị', 'Tỉnh'),
('46', 'Thừa Thiên Huế', 'Tỉnh'),
('48', 'Đà Nẵng', 'Thành Phố'),
('49', 'Quảng Nam', 'Tỉnh'),
('51', 'Quảng Ngãi', 'Tỉnh'),
('52', 'Bình Định', 'Tỉnh'),
('54', 'Phú Yên', 'Tỉnh'),
('56', 'Khánh Hòa', 'Tỉnh'),
('58', 'Ninh Thuận', 'Tỉnh'),
('60', 'Bình Thuận', 'Tỉnh'),
('62', 'Kon Tum', 'Tỉnh'),
('64', 'Gia Lai', 'Tỉnh'),
('66', 'Đắk Lắk', 'Tỉnh'),
('67', 'Đắk Nông', 'Tỉnh'),
('68', 'Lâm Đồng', 'Tỉnh'),
('70', 'Bình Phước', 'Tỉnh'),
('72', 'Tây Ninh', 'Tỉnh'),
('74', 'Bình Dương', 'Tỉnh'),
('75', 'Đồng Nai', 'Tỉnh'),
('77', 'Bà Rịa - Vũng Tàu', 'Tỉnh'),
('79', 'Hồ Chí Minh', 'Thành Phố'),
('80', 'Long An', 'Tỉnh'),
('82', 'Tiền Giang', 'Tỉnh'),
('83', 'Bến Tre', 'Tỉnh'),
('84', 'Trà Vinh', 'Tỉnh'),
('86', 'Vĩnh Long', 'Tỉnh'),
('87', 'Đồng Tháp', 'Tỉnh'),
('89', 'An Giang', 'Tỉnh'),
('91', 'Kiên Giang', 'Tỉnh'),
('92', 'Cần Thơ', 'Thành Phố'),
('93', 'Hậu Giang', 'Tỉnh'),
('94', 'Sóc Trăng', 'Tỉnh'),
('95', 'Bạc Liêu', 'Tỉnh'),
('96', 'Cà Mau', 'Tỉnh');

-- --------------------------------------------------------

--
-- Table structure for table `db_slider`
--

CREATE TABLE IF NOT EXISTS `db_slider` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `position` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `modified` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `options` varchar(2) DEFAULT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_slider`
--

INSERT INTO `db_slider` (`id`, `name`, `link`, `position`, `img`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`, `options`, `active`) VALUES
(1, 'Điện tử ', 'http://[::1]/minimart/san-pham/dien-tu', 'slideshow', 'slideshow_2.jpg', '2015-07-09 00:00:00', 'Supper Admin', '2016-10-14 17:58:29', 'Ngô Trung Phát', 1, 1, 1, '0', 1),
(3, 'Điện thoại di động 1', 'http://[::1]/minimart/san-pham/dien-thoai-di-dong', 'slideshow', 'slideshow_3.jpg', '2015-07-09 00:00:00', 'Supper Admin', '2016-10-04 14:55:16', 'Ngô Trung Phát', 1, 1, 1, '2', 0),
(4, 'Điện thoại di động 2', 'http://[::1]/minimart/san-pham/dien-thoai-di-dong', 'slideshow', 'slideshow_4.jpg', '2015-07-09 00:00:00', 'Supper Admin', '2016-10-04 14:55:30', 'Ngô Trung Phát', 1, 1, 1, '3', 0),
(5, 'PC 1', 'http://[::1]/minimart/san-pham/may-tinh-xach-tay', 'slideshow', 'slideshow_21.jpg', '2016-11-15 18:25:26', 'Ngô Trung Phát', '2016-11-15 18:25:26', 'Ngô Trung Phát', 1, 1, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `db_template`
--

CREATE TABLE IF NOT EXISTS `db_template` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `img` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `modified` int(11) NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_template`
--

INSERT INTO `db_template` (`id`, `name`, `author`, `email`, `img`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`) VALUES
(1, 'default', 'Hồ Diên Lợi', 'hodienloi@gmail.com', 'default.jpg', '0000-00-00 00:00:00', 'Supper Admin', 0, 'Supper Admin', 1, 1, 1),
(2, 'default1', 'Hồ Diên Lợi', 'hodienloi@gmail.com', 'default.jpg', '0000-00-00 00:00:00', 'Supper Admin', 0, 'Supper Admin', 1, 1, 0),
(3, 'default2', 'Hồ Diên Lợi', 'hodienloi@gmail.com', 'default.jpg', '0000-00-00 00:00:00', 'Supper Admin', 0, 'Supper Admin', 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `db_topic`
--

CREATE TABLE IF NOT EXISTS `db_topic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  `parentid` int(11) NOT NULL,
  `orders` varchar(5) DEFAULT NULL,
  `position` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'Supper admin',
  `modified` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT 'Supper admin',
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `metakey` varchar(155) NOT NULL,
  `metadesc` varchar(155) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_topic`
--

INSERT INTO `db_topic` (`id`, `name`, `link`, `level`, `parentid`, `orders`, `position`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`, `metakey`, `metadesc`) VALUES
(2, 'Giới thiệu', 'gioi-thieu', 1, 0, '1', 'footer-left', '2015-09-23 15:27:10', 'Hồ Diên Lợi', '2015-09-23 15:27:10', 'Hồ Diên Lợi', 1, 0, 1, '', ''),
(4, 'Tin tức', 'tin-tuc', 1, 0, '2', 'footer-left', '2015-07-24 00:00:00', 'Supper admin', '2015-07-24 00:00:00', 'Supper admin', 1, 0, 1, '', ''),
(6, 'Quà tặng', 'qua-tang', 1, 0, '4', 'footer-left', '2015-10-02 12:09:30', 'Hồ Diên Lợi', '2015-10-02 12:09:30', 'Hồ Diên Lợi', 1, 0, 1, '', ''),
(86, 'Chính sách vận chuyển', 'chinh-sach-van-chuyen', 1, 0, '', 'footer-right', '2016-10-03 16:18:03', 'Ngô Trung Phát', '2016-10-03 16:18:03', 'Ngô Trung Phát', 1, 1, 1, '', ''),
(87, 'Chính sách bảo mật', 'chinh-sach-bao-mat', 1, 0, '9', 'footer-right', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', ''),
(88, 'Đổi trả hàng', 'doi-tra-hang', 1, 0, '10', 'footer-right', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', ''),
(89, 'Vận chuyển\r\n', 'van-chuyen', 1, 0, '11', 'footer-right', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', ''),
(90, 'Phí vận chuyển', 'phi-van-chuyen', 1, 0, '12', 'footer-right', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', ''),
(91, 'Phương thức thanh toán', 'phuong-thuc-thanh-toan', 1, 0, '13', 'footer-right', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', ''),
(92, 'Khuyến mãi', 'khuyen-mai', 1, 0, '5', 'top', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', ''),
(93, 'Dịch vụ', 'dich-vu', 1, 0, '6', 'top', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', ''),
(94, 'Tuyển dụng', 'tuyen-dung', 1, 0, '7', 'top', '2016-09-25 12:09:30', 'Ngô Trung Phát', '2016-09-25 12:09:30', 'Ngô Trung Phát', 1, 0, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `db_user`
--

CREATE TABLE IF NOT EXISTS `db_user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `img` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_user`
--

INSERT INTO `db_user` (`id`, `fullname`, `username`, `password`, `email`, `gender`, `phone`, `img`, `created`, `trash`, `access`, `status`) VALUES
(2, 'Ngô Trung Phát', 'madmin', '64f020910aee24f1f4c8986946031818c4b00647', 'ngotrungphat@gmail.com', 1, '01654292452', 'avarta.jpg', '2016-10-03 07:30:00', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `db_usergroup`
--

CREATE TABLE IF NOT EXISTS `db_usergroup` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `modified` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT 'Supper Admin',
  `trash` tinyint(1) NOT NULL DEFAULT '1',
  `access` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_usergroup`
--

INSERT INTO `db_usergroup` (`id`, `name`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `access`, `status`) VALUES
(1, 'Quản trị', '2015-07-09 00:00:00', 'Hồ Diên Lợi', '2015-07-09 00:00:00', 'Hồ Diên Lợi', 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `db_category`
--
ALTER TABLE `db_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_config`
--
ALTER TABLE `db_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_contact`
--
ALTER TABLE `db_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_content`
--
ALTER TABLE `db_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_customer`
--
ALTER TABLE `db_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_district`
--
ALTER TABLE `db_district`
  ADD PRIMARY KEY (`districtid`),
  ADD KEY `provinceid` (`provinceid`);

--
-- Indexes for table `db_help`
--
ALTER TABLE `db_help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_images_product`
--
ALTER TABLE `db_images_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_menu`
--
ALTER TABLE `db_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_order`
--
ALTER TABLE `db_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_orderdetail`
--
ALTER TABLE `db_orderdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_product`
--
ALTER TABLE `db_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_province`
--
ALTER TABLE `db_province`
  ADD PRIMARY KEY (`provinceid`);

--
-- Indexes for table `db_slider`
--
ALTER TABLE `db_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_template`
--
ALTER TABLE `db_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_topic`
--
ALTER TABLE `db_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_user`
--
ALTER TABLE `db_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_usergroup`
--
ALTER TABLE `db_usergroup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `db_category`
--
ALTER TABLE `db_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `db_config`
--
ALTER TABLE `db_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `db_contact`
--
ALTER TABLE `db_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `db_content`
--
ALTER TABLE `db_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=148;
--
-- AUTO_INCREMENT for table `db_customer`
--
ALTER TABLE `db_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `db_help`
--
ALTER TABLE `db_help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_images_product`
--
ALTER TABLE `db_images_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_menu`
--
ALTER TABLE `db_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `db_order`
--
ALTER TABLE `db_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `db_orderdetail`
--
ALTER TABLE `db_orderdetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=153;
--
-- AUTO_INCREMENT for table `db_product`
--
ALTER TABLE `db_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=192;
--
-- AUTO_INCREMENT for table `db_slider`
--
ALTER TABLE `db_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `db_template`
--
ALTER TABLE `db_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `db_topic`
--
ALTER TABLE `db_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `db_user`
--
ALTER TABLE `db_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `db_usergroup`
--
ALTER TABLE `db_usergroup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
